<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title> Firstinsure</title>
    <!-- FAVICON LINK -->
    <link rel="shortcut icon" type="image/x-icon" href="https://i.ibb.co/4TTrTYt/logo.png">
    <!-- BOOTSTRAP -->
    <link rel="stylesheet" type="text/css" href="css/vendor/bootstrap.min.css">

    <!-- FONT AWESOME LINK -->
    <link rel="stylesheet" type="text/css" href="css/vendor/font-awesome/css/font-awesome.min.css">

    <!-- IMAGE OVERLAY STYLE LINK  -->
    <link rel="stylesheet" type="text/css" href="css/vendor/overlay/style.css">
    <!-- LIGHTBOX STYLE LINK -->
    <link rel="stylesheet" type="text/css" href="css/vendor/lightbox/ekko-style.css">

    <!-- CAROUSEL STYLE LINK  -->
    <link rel="stylesheet" type="text/css" href="css/vendor/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/vendor/owl-carousel/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="css/vendor/owl-carousel/carousel.css">

    <!-- STYLE SWITCHER LINK -->
    <link rel="stylesheet" type="text/css" href="css/vendor/style_switcher/styleswitcher.css">

    <!-- CUSTOM CSS -->
    <link rel="stylesheet" type="text/css" href="css/custom/style_blue.css">
  
</head>

<body data-spy="scroll" data-target=".navbar-fixed-top" data-offset="75">
 
    <!--================================= NAVIGATION START =============================================-->
    <nav class="navbar navbar-default navbar-fixed-top menu-bg" id="top-nav">
        <div class="container">
            <div class="navigation-tb">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div>
                         <a href="#"> <img src="https://i.ibb.co/MRtD4R8/final-first-insure.png" alt="logo" style='width:290px' />
                        </a>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right no-margin" id="menu-list">
                        <li class="menu-fs menu-underline"><a href="#home" class="pagescroll">Home</a>
                        </li>
                        <li class="menu-fs menu-underline"><a href="#services" class="pagescroll">Services</a>
                        </li>
                        <li class="menu-fs menu-underline"><a href="#gallery" class="pagescroll">Gallery </a>
                        </li>
                        <li class="menu-fs menu-underline"><a href="#price" class="pagescroll"> Product</a>
                        </li>
                        <li class="menu-fs menu-underline"><a href="#contact" class="pagescroll"> Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!--================================= NAVIGATION END =============================================-->

    <!--================================= HEADER-FORM-1 START =============================================-->
    <div class="container-fluid header-form-bgimage-1 bgimage-property parallax  header-section-space-form-1" id="home">
        <div class="container">
            <div class="col-md-12 column-center no-padding white-text">
                <div class="row">
                    <div class="col-md-7">
                        <div class="header-form-heading-top res-header-heading-bottom">
                              <p class="left header-head1 header-head1-bottom">Welcome to FirstInsure</p>
  <p class="left header-text-bottom ls">Advice WIth Trusted Finace Advisor</p>
                            <p class="left header-text-bottom ls">We assist you with arriving at right and right Information. 
Online Comparison will show you just rewarding and appealing choices – which could conceivably suit your necessities Therefore, 
It is Advisable to fix a gathering with specialists who can comprehend your requirements and assist you with getting the best monetary exhortation</p>
                            <div class="left">
                                <a href="#">
                                    <div class="header-btn">Call Us</div>
                                </a>
                            </div>
                        </div>
                    </div>

                  <!--  <div class="col-md-5">
                        <div class="contact-div header-contact-form header-contact-bg">
                           <form class="contact-form1" action="thankyou.php" method="POST">
                                <div class="clearfix">
                                    <div class="header-form-heading-bg">
                                        <h4 class="center">register today to get upto 60% off</h4>
                                    </div>
                                    <div class="header-contact-bg-pad">
                                       
                                      
                                        <div class="form-div form-bottom-1">
                                            <i class="fa fa-user"></i>
                                            <input class="form-control form-text" type="text" name="name" value="" placeholder="Please Enter Name" required />
                                        </div>

                                     
                                        <div class="form-div form-bottom-1">
                                            <i class="fa fa-envelope"></i>
                                            <input class="form-control form-text" type="email" name="email" value="" placeholder="Please Enter Email" required />
                                        </div>

                                    
                                        <div class="form-div phoneno-bottom error-div">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <input class="form-control form-text phoneno" name="phoneno" placeholder="Phone Number" autocomplete="off" type="text" onkeypress="return isNumber(event)" maxlength="10">
                                        </div>
                                       
                                        
                                          <div class="form-div form-bottom-1">
                                            <i class="fa fa-map-marker"></i>
                                            <input class="form-control form-text" type="text" name="city" value="" placeholder="Please Enter City Name" required />
                                        </div>

                                      <div class="col-sm-6">
                                            <div class="form-div form-bottom-1">
                                            <i class="fa fa-calendar"></i>
                                            <input class="form-control form-text" type="date" name="dob" value="" placeholder="DOB" required />
                                        </div>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-div form-bottom-1">
                                             <select class="form-control" name="gender">
                                                            <option value="" style="color: black;">Select Gender</option>
                                                            <option value="Male" style="color: black;">Male</option>
                                                            <option value="Female" style="color: black;">Female</option>
                               
                                            </select>
                                        </div>
                                        </div>

                                     
                                        <div class="left form-error-top"> <span class="form-success sucessMessage"> </span> <span class="form-failure failMessage"> </span>
                                        </div>
                                    </div>

                               
                                    <div>
                                        <input type="submit" class="submit-btn" name="inquiry" value="SUBMIT NOW" style="text-align: center; cursor: pointer;">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>-->

                </div>
            </div>
        </div>
    </div>
    <!--================================= HEADER-FORM-1 END =============================================-->

    <!--================================= OUR SERVICES START =============================================-->
    <section class="container-fluid section-space section-bg-1" id="services">
        <div class="container">
            <h1 class="center">Get Advice To Provide Trusted Finance Advisor</h1>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6 common-full">
                    <div class="services-icon">
                        <img src="images/64x64x1.png" alt="icon" />
                    </div>
                    <div class="services-pad services-bottom">
                        <h3 class="left h3-bottom"><a href="#">Understand Your Needs</a></h3>
                       
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-6 common-full">
                    <div class="services-icon">
                        <img src="images/64x64x2.png" alt="icon" />
                    </div>
                    <div class="services-pad services-bottom">
                        <h3 class="left h3-bottom"><a href="#"> Know Your T&Cs </a></h3>
                       
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-6 common-full">
                    <div class="services-icon">
                        <img src="images/64x64x3.png" alt="icon" />
                    </div>
                    <div class="services-pad services-bottom">
                        <h3 class="left h3-bottom"><a href="#">Determine Your Coverage Amount</a></h3>
                      
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-6 common-full">
                    <div class="services-icon">
                        <img src="images/64x64x4.png" alt="icon" />
                    </div>
                    <div class="services-pad res-services-bottom services-res-bottom">
                        <h3 class="left h3-bottom"><a href="#">Soon You Start The Better</a></h3>
                       
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-6 common-full">
                    <div class="services-icon">
                        <img src="images/64x64x5.png" alt="icon" />
                    </div>
                    <div class="services-pad services-res-bottom">
                        <h3 class="left h3-bottom"><a href="#">Be Transparent</a></h3>
                        
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-6 common-full">
                    <div class="services-icon">
                        <img src="images/64x64x6.png" alt="icon" />
                    </div>
                    <div class="services-pad">
                        <h3 class="left h3-bottom"><a href="#">Get Advice</a></h3>
                      
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================================= OUR SERVICES END =============================================-->

    <!--================================= OUR ENVIRONMENT START =============================================-->
    <section class="container-fluid section-space section-bg-2">
        <div class="container">
            <h1 class="center">Money Back Plan</h1>
            <div class="row">
                <div class="col-md-4 col-sm-4 common-res-bottom-1">
                    <div class="left image-bottom">
                        <img src="images/new/firstinsure (11).jpg" alt="image" class="img-responsive" />
                    </div>
                    <h3 class="left h3-bottom"><a href="#">How does Plan work?</a></h3>
                    <p class="left"> Money back plans give both of reserve funds and life cover. In this plans a specific measure of the total guaranteed is being given to the person at standard spans during the arrangement time frame. The equilibrium sum is being paid during the development of the arrangement. In the event that the approach holder bites the dust during the strategy time frame the chosen one would get the whole guaranteed just as the reward sum assuming any. Besides the money payout got at ordinary span is excluded from charge. The measure of payout, terms of the approach, relies upon the strategy that one has picked. </p>
                </div>

                <div class="col-md-4 col-sm-4 common-res-bottom-1">
                    <div class="left image-bottom">
                        <img src="images/750x500x2.jpg" alt="image" class="img-responsive" />
                    </div>
                    <h3 class="left h3-bottom"><a href="#">Break Bonus </a></h3>
                    <p class="left">Rewards are pronounced toward the finish of the monetary year. In any case, imagine a scenario in which a strategy develops or demise happens before the finish of that period. Guarantors announce break reward to deal with such circumstances and to try not to put such policyholders off guard. The measure of reward, notwithstanding, is added to the arrangement on favorable to rate reason for that particular year. </p>
                </div>

                <div class="col-md-4 col-sm-4">
                    <div class="left image-bottom">
                        <img src="images/new/firstinsure (10).jpg" alt="image" class="img-responsive" />
                    </div>
                    <h3 class="left h3-bottom"><a href="#">Terminal Bonus</a></h3>
                    <p class="left">Terminal Bonus is otherwise called perseverance reward which is paid once, for example at the hour of development of the strategy. It is such a steadfastness reward given to a policyholder for keeping up the approach till development. Its worth isn't ensured and will be revealed uniquely at the hour of strategy development.</p>
                </div>

            </div>
        </div>
    </section>
    <!--================================= OUR ENVIRONMENT END =============================================-->

    <!--================================= COUNTER START =============================================-->
    <div class="container-fluid counter-bgimage bgimage-property parallax  counter-section-space">
        <div class="container">
            <div class="row white-text">
               <!-- <div class="col-md-3 col-sm-3 col-xs-6 counter-res-bottom common-full-1">
                    <div class="center image-bottom">
                        <img src="images/64x64x7.png" alt="icon" />
                    </div>
                    <p class="center counter-num count ls">3100</p>
                    <p class="center counter-sub ls">PROJECTS </p>
                </div>-->

                <div class="col-md-6 col-sm-6 col-xs-6 counter-res-bottom common-full-1">
                    <div class="center image-bottom">
                        <img src="images/64x64x8.png" alt="icon" />
                    </div>
                    <p class="center counter-num count ls">4578</p>
                    <p class="center counter-sub ls">CLIENTS</p>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-6 common-full-1 counter-res-bottom-1">
                    <div class="center image-bottom">
                        <img src="images/64x64x9.png" alt="icon" />
                    </div>
                    <p class="center counter-num count ls">7548 </p>
                    <p class="center counter-sub ls">LIKES</p>
                </div>

              <!--  <div class="col-md-3 col-sm-3 col-xs-6 common-full-1">
                    <div class="center image-bottom">
                        <img src="images/64x64x10.png" alt="icon" />
                    </div>
                    <p class="center counter-num count ls">507</p>
                    <p class="center counter-sub ls">AWARDS </p>
                </div>-->
            </div>
        </div>
    </div>
    <!--================================= COUNTER END =============================================-->

    <!--================================= CREATIVE TEMPLATE START =============================================-->
    <section class="container-fluid section-space section-bg-2">
        <div class="container">
            <div class="row res-text-center-1">
                <div class="col-md-6 col-sm-12 common-res-bottom">
                    <div class="center res-image-bottom-1">
                        <img src="https://i.ibb.co/ZXQRYPB/covid-19.jpg" alt="image" class="img-responsive image-center image-grow" />
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 common-res-bottom common-res-bottom-1">
                    <h2 class="left h2-bottom"><a href="#"> COVID-19</a></h2>
                    <p class="left p-bottom">Amplify Your Protection Against Corona Virus With A COVID-19 </p>
                </div>

            </div>
        </div>
    </section>
    <!--================================= CREATIVE TEMPLATE END =============================================-->

   

    <!--================================= WHAT WE DO START =============================================-->
    <section class="container-fluid section-space section-bg-1">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="left res-image-bottom res-image-bottom-1">
                        <img src="images/new/firstinsure (1).jpg" alt="image" class="img-responsive" />
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div>
                        <h2 class="left h2-bottom"><a href="#">What Trusted Finance Advisor Do</a></h2>
                        <p class="left p-bottom"> We don't give online correlations. We assist you with arriving at right and right data. Online Comparison will show you just worthwhile and appealing choices – which might possibly suit your necessities Therefore, It is Advisable to fix a gathering with specialists who can comprehend your requirements and assist you with getting the best monetary counsel.</p>
                        <div class="distab list-bottom list-top">
                            <div class="left distab-cell-middle">
                                <img src="images/24x24x1.png" alt="icon" />
                            </div>
                            <div class="left distab-cell-middle what-left-pad">
                                <p class="left what-list ls">Experienced Adviser Assistance</p>
                            </div>
                        </div>

                        <div class="distab list-bottom">
                            <div class="left distab-cell-middle">
                                <img src="images/24x24x2.png" alt="icon" />
                            </div>
                            <div class="left distab-cell-middle what-left-pad">
                                <p class="left what-list ls">Marketing Analyst</p>
                            </div>
                        </div>

                        <div class="distab list-bottom">
                            <div class="left distab-cell-middle">
                                <img src="images/24x24x3.png" alt="icon" />
                            </div>
                            <div class="left distab-cell-middle what-left-pad">
                                <p class="left what-list ls">Best Support Services</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--================================= WHAT WE DO END =============================================-->

    <!--================================= GALLERY START =============================================-->
    <section class="container-fluid section-space section-bg-2" id="gallery">
        <div class="container">
            <h1 class="center">gallery</h1>
            <div id="grid" class="row effects">
                <figure class="col-sm-4 col-xs-6 gallery-image-lr gallery-row-bottom light-box">
                    <div class="img">
                        <img alt="portfolio-2" src="images/750x500x4.jpg" />
                        <div class="overlay">
                            <div class="outer">
                                <div class="middle">
                                    <div class="inner">
                                        <ul class="center no-padding no-margin">
                                            <li>
                                                <a href="images/750x500x4.jpg" class="expand extend-icon" data-title="Portfolio One" data-toggle="lightbox" data-gallery="navigateTo"><i class="fa fa-search gallery-fa"></i></a>
                                                <a href="#" class="expand extend-icon-two"><i class="fa fa-chain-broken gallery-fa gallery-fa-pad-left"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </figure>

                <figure class="col-sm-4 col-xs-6 gallery-image-lr gallery-row-bottom light-box">
                    <div class="img">
                        <img alt="portfolio-2" src="images/750x500x5.jpg" />
                        <div class="overlay">
                            <div class="outer">
                                <div class="middle">
                                    <div class="inner">
                                        <ul class="center no-padding no-margin">
                                            <li>
                                                <a href="images/750x500x5.jpg" class="expand extend-icon" data-title="Portfolio Two" data-toggle="lightbox" data-gallery="navigateTo"><i class="fa fa-search gallery-fa"></i></a>
                                                <a href="#" class="expand extend-icon-two"><i class="fa fa-chain-broken gallery-fa gallery-fa-pad-left"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </figure>

                <figure class="col-sm-4 col-xs-6 gallery-image-lr gallery-row-bottom light-box">
                    <div class="img">
                        <img alt="portfolio-2" src="images/750x500x6.jpg" />
                        <div class="overlay">
                            <div class="outer">
                                <div class="middle">
                                    <div class="inner">
                                        <ul class="center no-padding no-margin">
                                            <li>
                                                <a href="images/750x500x6.jpg" class="expand extend-icon" data-title="Portfolio Three" data-toggle="lightbox" data-gallery="navigateTo"><i class="fa fa-search gallery-fa"></i></a>
                                                <a href="#" class="expand extend-icon-two"><i class="fa fa-chain-broken gallery-fa gallery-fa-pad-left"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </figure>

                <figure class="col-sm-4 col-xs-6 gallery-image-lr res-gallery-row-bottom light-box">
                    <div class="img">
                        <img alt="portfolio-2" src="images/750x500x7.jpg" />
                        <div class="overlay">
                            <div class="outer">
                                <div class="middle">
                                    <div class="inner">
                                        <ul class="center no-padding no-margin">
                                            <li>
                                                <a href="images/750x500x7.jpg" class="expand extend-icon" data-title="Portfolio Four" data-toggle="lightbox" data-gallery="navigateTo"><i class="fa fa-search gallery-fa"></i></a>
                                                <a href="#" class="expand extend-icon-two"><i class="fa fa-chain-broken gallery-fa gallery-fa-pad-left"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </figure>

                <figure class="col-sm-4 col-xs-6 gallery-image-lr light-box res-gallery-bottom">
                    <div class="img">
                        <img alt="portfolio-2" src="images/750x500x8.jpg" />
                        <div class="overlay">
                            <div class="outer">
                                <div class="middle">
                                    <div class="inner">
                                        <ul class="center no-padding no-margin">
                                            <li>
                                                <a href="images/750x500x8.jpg" class="expand extend-icon" data-title="Portfolio Five" data-toggle="lightbox" data-gallery="navigateTo"><i class="fa fa-search gallery-fa"></i></a>
                                                <a href="#" class="expand extend-icon-two"><i class="fa fa-chain-broken gallery-fa gallery-fa-pad-left"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </figure>

                <figure class="col-sm-4 col-xs-6 gallery-image-lr light-box">
                    <div class="img">
                        <img alt="portfolio-2" src="images/750x500x9.jpg" />
                        <div class="overlay">
                            <div class="outer">
                                <div class="middle">
                                    <div class="inner">
                                        <ul class="center no-padding no-margin">
                                            <li>
                                                <a href="images/750x500x9.jpg" class="expand extend-icon" data-title="Portfolio Six" data-toggle="lightbox" data-gallery="navigateTo"><i class="fa fa-search gallery-fa"></i></a>
                                                <a href="#" class="expand extend-icon-two"><i class="fa fa-chain-broken gallery-fa gallery-fa-pad-left"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </figure>

            </div>
        </div>
    </section>
    <!--================================= GALLERY END =============================================-->

    <!--================================= MARKET ANALYSIS START =============================================-->
    <section class="container-fluid section-space section-bg-1">
        <div class="container analysis-row-space">
            <h1 class="center">Insurance Plan With Tax Benefits</h1>
            <!--=============== ROW-1 ==================-->
            <div class="row">
                <div class="center col-md-10 column-center no-padding res-width-full">
                    <div class="col-md-6 res-image-bottom res-image-bottom-1">
                        <div class="center">
                            <img src="images/new/firstinsure (18).jpeg" alt="image" class="img-responsive image-center"/>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <h3 class="left h3-bottom">Pension Plan</h3>
                        <p class="left p-bottom"> Plan where amassing stage is utilized for making corpus prior to beginning month to month annuity at vesting date.
As the name recommends the purpose of the arrangement is to turn out ordinary revenue post retirement as the majority of us work in private association consequently.</p>
                        <p class="left">It isn't hard to comprehend that the greater the superior paid today, the greater will be the corpus made at retirement and greater will be the annuity sum. 

<br/>A limit of 33% Tax Free and leave the leftover to be changed over into likened regularly scheduled payouts, like annuity.</p>
                        
                    </div>
                </div>
            </div>

            <!--=============== ROW-2 ==================-->
            <div class="row">
                <div class="center col-md-10 column-center no-padding res-width-full">
                    <div class="col-md-6 res-image-bottom res-image-bottom-1">
                        <div class="center">
                            <img src="images/750x410x2.jpg" alt="image" class="img-responsive image-center" />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <h3 class="left h3-bottom">Investment Plan</h3>
                        <p class="left p-bottom">Venture arranging is a huge piece of monetary arranging. At the point when we talk about speculation arranging or the correct venture plans to invest,three words com to mind-scary, overpowering and alarming. For a "standard joe" this inquiry appears to be never-ending</p>
                        <p class="left">It gives made sure about method of objective based abundance creation like kid's marriage, kid's schooling or retirement arranging and so forth.</p>
                        
                    </div>
                </div>
            </div>

            <!--=============== ROW-3 ==================-->
            <div class="row">
                <div class="center col-md-10 column-center no-padding res-width-full">
                    <div class="col-md-6 res-image-bottom res-image-bottom-1">
                        <div class="center">
                            <img src="images/750x410x3.jpg" alt="image" class="img-responsive image-center" />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <h3 class="left h3-bottom">Life Insurance Plan</h3>
                        <p class="left p-bottom"> Disaster protection is an agreement between a safeguarded and a Life Insurance Company,where the Insurance organization pays a single amount add up to the candidate/guaranteed in return for the charge after a specific period or upon the passing of the safeguarded</p>
                        <p class="left">The purchaser pays a predefined premium sum in singular amount or at customary stretches. Here, your life cover charge relies on different factors, for example, age, sex and ailment</p>
                      
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================================= MARKET ANALYSIS END =============================================-->

    <!--================================= LATEST NEWS START =============================================-->
    <section class="container-fluid section-space section-bg-2">
        <div class="container">
            <h1 class="center">Our Plans</h1>
            <div class="row">
                <div class="col-md-3 col-sm-6 common-res-bottom common-res-bottom-1">
                    <div class="left image-bottom">
                        <img src="images/new/firstinsure (11).jpg" alt="image" class="img-responsive" />
                    </div>
                    <h3 class="left h3-bottom"><a href="#">ULIP Plan</a></h3>
                    <p class="left">Unit-linked Insurance Plans are market connected plans and gives double advantage of protection and speculation </p>
                </div>

                <div class="col-md-3 col-sm-6 common-res-bottom-1">
                    <div class="left image-bottom">
                        <img src="images/new/firstinsure (2).jpg" alt="image" class="img-responsive" />
                    </div>
                    <h3 class="left h3-bottom"><a href="#">Home Insurance</a></h3>
                    <p class="left">Home has consistently been an exceptional spot for each person and each client in India realizes that esteem.</p>
                </div>
                  <div class="col-md-3 col-sm-6 common-res-bottom common-res-bottom-1">
                    <div class="left image-bottom">
                        <img src="images/new/firstinsure (15).jpg" alt="image" class="img-responsive" />
                    </div>
                    <h3 class="left h3-bottom"><a href="#">Term Insurance</a></h3>
                    <p class="left">Term Insurance Planis Pure Protection insurance plan </p>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="left image-bottom">
                        <img src="images/new/firstinsure (17).jpg" alt="image" class="img-responsive" />
                    </div>
                    <h3 class="left h3-bottom"><a href="#">Travel Insurance</a></h3>
                    <p class="left">Typically, except if made necessary, go protection is viewed as extra cost in the general outing surge.</p>
                </div>

            </div>
        </div>
    </section>
    <!--================================= LATEST NEWS END =============================================-->

  <!--================================= EMAIL SUBSCRIPTION START =============================================-->
    <div class="container-fluid subscription-bgimage bgimage-property parallax  subscription-section-space white-text">
        <div class="container">
            <div class="col-md-10 column-center no-padding">
                <p class="center bgimage-head bgimage-head-bottom ls">Secure your child's future</p>
                <div class="col-md-8 column-center no-padding res-width-full">
                    <p class="center email-text-bottom ls"> Our Savings and Investment plans are life insurance plans that offer you</p>
                </div>
               
            </div>
        </div>
    </div>
    <!--================================= EMAIL SUBSCRIPTION END =============================================-->



    <!--================================= FREQUENTLY ASKED QUESTIONS START =============================================-->
    <section class="container-fluid section-space section-bg-1">
        <div class="container">
            <h1 class="center">frequently asked questions</h1>
            <div class="row faq-row faq-white-text">
                <div class="col-md-6 col-sm-6 faq-res-bottom">
                    <div class="faq-col">
                        <div class="faq-row-bottom">
                            <div class="faq-title faq-bg">
                                <div class="distab faq-bg-width">
                                    <h3 class="faq-heading distab-cell-middle">Benefits of Buying Life Insurance?
									</h3>
                                    <p class="down-arrow down-arrow-left distab-cell-middle"></p>
                                </div>
                            </div>
                            <p class="faq-ans faq-answer-pad faq-answer-bg">Your disaster protection gives your family decisions by giving the advantages to assist pay with offing obligations, to help meet lodging installments and continuous everyday costs, to help store school training for your kids or grand kids, and a whole lot more. Disaster protection gives money when it's required most.
                            </p>
                        </div>

                        <div class="faq-row-bottom">
                            <div class="faq-title faq-bg">
                                <div class="distab faq-bg-width">
                                    <h3 class="faq-heading distab-cell-middle">What are the Types of Life Insurance??
									</h3>
                                    <p class="down-arrow down-arrow-left distab-cell-middle"></p>
                                </div>
                            </div>
                            <p class="faq-ans faq-answer-pad faq-answer-bg"> 1.Term life insurance.<br>
                            2. Whole life insurance.<br>
                            3. Universal life insurance.<br>
                            4. Indexed universal life insurance.<br>
                            5. Variable life insurance.<br>
                            6. Final expense insurance.<br>
                            7. Group life insurance.<br>
                           
                            </p>
                        </div>

                        <div class="faq-row-bottom">
                            <div class="faq-title faq-bg">
                                <div class="distab faq-bg-width">
                                    <h3 class="faq-heading distab-cell-middle">Who is most likely to buy Life Insurance?
									</h3>
                                    <p class="down-arrow down-arrow-left distab-cell-middle"></p>
                                </div>
                            </div>
                            <p class="faq-ans faq-answer-pad faq-answer-bg"> Obviously, individuals purchase disaster protection for an assortment of reasons. Probably the most widely recognized are to assist your family with supplanting lost pay, to cover your finish of-life costs, and to give up something for the future. All things considered, a new LIMRA industry study of 5,500 U.S. families discovered there are various key achievements that can provoke individuals to search for inclusion. These incorporate everything from purchasing a house to the passing of a friend or family member. And keeping in mind that not all life occasions are made equivalent, research shows the a greater amount of these achievements you hit, the more probable you are to purchase extra security.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="faq-col">
                        <div class="faq-row-bottom">
                            <div class="faq-title faq-bg">
                                <div class="distab faq-bg-width">
                                    <h3 class="faq-heading distab-cell-middle">Plan Your Life Efficiently with Insurance?
									</h3>
                                    <p class="down-arrow down-arrow-left distab-cell-middle"></p>
                                </div>
                            </div>
                            <p class="faq-ans faq-answer-pad faq-answer-bg">Five Steps To Making A Better Financial Decision With Life Insurance.<br/>
                            1. Provide financial shield to your family with term life..<br>
                            2. Child plan to secure their dreams.<br>
                            3. Plan your retirement.<br>
                            4. Short-term financial goals.<br>
                            5. Variable life insurance.<br>
                            6. Long-term investment and savings.<br>
                         
                            </p>
                        </div>

                        <div class="faq-row-bottom">
                            <div class="faq-title faq-bg">
                                <div class="distab faq-bg-width">
                                    <h3 class="faq-heading distab-cell-middle"> Why Should You Consider Life Insurance? 
									</h3>
                                    <p class="down-arrow down-arrow-left distab-cell-middle"></p>
                                </div>
                            </div>
                            <p class="faq-ans faq-answer-pad faq-answer-bg"> To guarantee that your close family has some monetary help in case of your destruction. To back your youngsters' schooling and different requirements. To have a reserve funds plan for the future so you have a consistent kind of revenue after retirement.
                            </p>
                        </div>

                        <div class="faq-row-bottom">
                            <div class="faq-title faq-bg">
                                <div class="distab faq-bg-width">
                                    <h3 class="faq-heading distab-cell-middle">How To Select the Best Life insurance Plan?
									</h3>
                                    <p class="down-arrow down-arrow-left distab-cell-middle">
                                          
                                    </p>
                                </div>
                            </div>
                            <p class="faq-ans faq-answer-pad faq-answer-bg">
                                  1. Engage With Insurance Advisor.<br>
                                            2. Calculate the Life Cover During Meeting With Relationship Manager.<br>
                                            3. Compare Insurance Plans During Meeting.<br>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--================================= FREQUENTLY ASKED QUESTIONS END =============================================-->

    <!--================================= PRICE TABLE START =============================================-->
    <section class="container-fluid section-space section-bg-2" id="price">
        <div class="container">
            <h1 class="center">Covid - 19</h1>
            <div class="row center">
               <p>Amplify Your Protection Against Corona Virus With A COVID-19 Protection Policy.</p>
               <p>Introducing a life insurance plan for your financial security during the COVID-19 pandemic.</p>
               <p>COVID Shield+ is a newly launched plan offering insurance coverage for coronavirus disease or COVID-19. COVID Shield+ has been specifically designed to cater to the financial expenses caused due to COVID-19. The treatment for COVID-19 can require lakhs of rupees and lack of proper medical care could also lead to fatalities. In a situation like this, it is best to be prepared financially to secure your family’s future.</p>
           <p>COVID Shield+ offers you insurance covering coronavirus disease, which can be of great relief if you are diagnosed with COVID-19 and your symptoms are critical. You will also have the peace of mind that if God forbid, your Coronavirus infection turns out to be fatal, your family will still be financially protected with an insurance policy for COVID-19.</p>
            </div>
           
        </div>
    </section>
    <!--================================= PRICE TABLE END =============================================-->

    <!--================================= BUILDER START =============================================-->
    <section class="container-fluid section-bg-1 no-padding">
        <div class="builder-bgimage bgimage-property builder-bgimage-height">
        </div>
        <div class="container">
            <div class="builder-bgimage-pad">
                <div class="row">
                    <div class="col-md-6 builder-content-height">

                    </div>
                    <div class="col-md-6 builder-content-height builder-content-pad">
                        <div class="distab why-row-bottom">
                           
                            <div class="distab-cell why-content-pad">
                                <h3 class="left h3-bottom"><a href="#">Pension Plan </a></h3>
                                <p class="left">Plan where amassing stage is utilized for making corpus prior to beginning month to month annuity at vesting date.</p>
                            </div>
                        </div>

                        <div class="distab why-row-bottom">
                           
                            <div class="distab-cell why-content-pad">
                                <h3 class="left h3-bottom"><a href="#">Child Insurance Plans </a></h3>
                                <p class="left">In the event that you are a parent, you'd realize how having your first kid is the delight of all delights.</p>
                            </div>
                        </div>

                        <div class="distab why-row-bottom">
                           
                            <div class="distab-cell why-content-pad">
                                <h3 class="left h3-bottom"><a href="#">Life Insurance Plan </a></h3>
                                <p class="left">Disaster protection is an agreement between a safeguarded and a Life Insurance Company,where the Insurance organization pays a single amount.</p>
                            </div>
                        </div>

                        <div class="distab">
                           
                            <div class="distab-cell why-content-pad">
                                <h3 class="left h3-bottom"><a href="#">Investment Plan </a></h3>
                                <p class="left">Venture arranging is a huge piece of monetary arranging. At the point when we talk about speculation arranging or the correct venture plans to invest.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--================================= BUILDER END =============================================-->

    <!--================================= TESTIMONIAL START =============================================-->
    <section class="container-fluid section-space section-bg-1 testimonial-indicator">
        <div class="container">
            <h1 class="center">Testimonial</h1>
            <div id="owl-demo1" class="owl-carousel owl-theme no-margin no-padding">
                <div class="item">
                    <div class="center quote-bottom">
                        <img src="https://i.ibb.co/GMzhBv3/men1.png" alt="quote" class="image-center" style="width: 120px;" />
                    </div>
                    <div class="col-md-8 column-center no-padding">
                        <p class="center p-bottom testimonial-text ls"> I love the way they communicate everything. Inclusions and exclusions were explained in a right way to help me choose right product at right pricing.</p>
                    </div>
                    <div class="distab image-center">
                        <div class="distab-cell-middle">
                           
                        </div>
                        <div class="distab-cell-middle testimonial-author-pad">
                            <p class="left testimonial-author ls">Krishna Sharma</p>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="center quote-bottom">
                        <img src="https://i.ibb.co/GMzhBv3/men1.png" alt="quote" class="image-center" style="width: 120px;" />
                    </div>
                    <div class="col-md-8 column-center no-padding">
                        <p class="center p-bottom testimonial-text ls"> Best place to buy an insurance especially if u are planning fr health insurance then definitely buy frm there bcz they provide a lots f best services fr their clients.</p>
                    </div>
                    <div class="distab image-center">
                        <div class="distab-cell-middle">
                            
                        </div>
                        <div class="distab-cell-middle testimonial-author-pad">
                            <p class="left testimonial-author ls">Raj Sharma</p>
                        </div>
                    </div>
                </div>
           </div>
        </div>
    </section>
    <!--================================= TESTIMONIAL END =============================================-->

    <!--================================= FOOTER-MAP START =============================================-->
    <section class="container-fluid section-space section-bg-2" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 common-res-bottom-1">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3607.0819859883295!2d82.99304611501161!3d25.301449383847856!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x398e31ff2d972a61%3A0xfe4b6767ea984649!2sFaculty%20of%20Education%20BHU!5e0!3m2!1sen!2sin!4v1609244702719!5m2!1sen!2sin" width='100%' height="400"  frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>

               
                </div>
            </div>
		
            
        </div>
    </section>
    <!--================================= FOOTER-MAP END =============================================-->

    <!--================================= FOOTER START =============================================-->
    <section class="container-fluid footer-section-space footer-bg">
        <div class="container white-text">
            <div class="row">
                <!--==========  COLUMN-1 ===============-->
                <div class="col-md-8 col-sm-12 col-xs-12 common-res-bottom common-res-bottom-1">
                    <h3 class="left posts-heading-bottom">Disclaimer!</h3>
                    <div class="distab posts-bottom">
                        <div class="left distab-cell-middle what-left-pad">
                            <p class="left">This is to inform that FirstInsure.in do not charge any fees/security deposit/advances towards outsourcing any of its activities. All stake holders are cautioned against any such fraud.</p>
                        </div>
                    </div>

                </div>

                <!--==========  COLUMN-2 ===============-->
               <!-- <div class="col-md-2 col-sm-6 col-xs-6 common-full res-footer-links-bottom">
                    <h3 class="left posts-heading-bottom">important links</h3>
                    <div class="left">
                        <ul class="footer-list-bk footer-list-bottom no-padding no-margin">

                            <li>
                                <p class="left distab ls"><img src="images/dot.png" alt="icon" class="footer-dot-right distab-cell-middle" /><span class="distab-cell-middle"><a href="#">Our Services</a></span>
                                </p>
                            </li>

                            <li>
                                <p class="left distab ls"><img src="images/dot.png" alt="icon" class="footer-dot-right distab-cell-middle" /><span class="distab-cell-middle"><a href="#">Our Features</a></span>
                                </p>
                            </li>

                            <li>
                                <p class="left distab ls"><img src="images/dot.png" alt="icon" class="footer-dot-right distab-cell-middle" /><span class="distab-cell-middle"><a href="#">Support</a></span>
                                </p>
                            </li>

                            <li>
                                <p class="left distab ls"><img src="images/dot.png" alt="icon" class="footer-dot-right distab-cell-middle" /><span class="distab-cell-middle"><a href="#">Contact Us</a></span>
                                </p>
                            </li>

                            <li>
                                <p class="left distab ls"><img src="images/dot.png" alt="icon" class="footer-dot-right distab-cell-middle" /><span class="distab-cell-middle"><a href="#">About Us</a></span>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>-->

                <!--==========  COLUMN-3 ===============-->
                <div class="col-md-4 col-sm-6 col-xs-6 common-full">
                    <h3 class="left posts-heading-bottom">contact us</h3>
                    <div class="distab footer-contact-bottom">
                        <div class="left distab-cell-middle">
                            <img src="images/32x32x1.png" alt="icon" />
                        </div>
                        <div class="left distab-cell-middle footer-contact-left">
                            <p class="left ls">B21/65 A-1,  Ground Floor, Rathyatra Kamachha Rd,
Kamachha,Varanasi West, Varanasi,
Uttar Pradesh-221010</p>
                        </div>
                    </div>

                    <div class="distab footer-contact-bottom">
                        <div class="left distab-cell-middle">
                            <img src="images/32x32x2.png" alt="icon" />
                        </div>
                        <div class="left distab-cell-middle footer-contact-left">
                            <p class="left ls"><a href="#">Info@firstinsure.in</a>
                            </p>
                        </div>
                    </div>

                    <div class="distab footer-contact-bottom">
                        <div class="left distab-cell-middle">
                            <img src="images/32x32x3.png" alt="icon" />
                        </div>
                        <div class="left distab-cell-middle footer-contact-left">
                            <p class="left ls">+91-757-008-0001 </p>
                        </div>
                    </div>

                    <div class="left">
                        <ul class="no-padding no-margin footer-icon footer-left-pad">
                            <li>
                                <a href="#">
                                    <img src="images/48x48x1.png" alt="icon" />
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <img src="images/48x48x2.png" alt="icon" />
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <img src="images/48x48x3.png" alt="icon" />
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <img src="images/48x48x4.png" alt="icon" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-br footer-br-bottom"></div>
            <p class="center ls">&copy; 2021, All Rights Reserved</p>
        </div>
    </section>
    <!--================================= FOOTER END =============================================-->
    <!-- JQUERY LIBRARY -->
    <script type="text/javascript" src="js/vendor/jquery.min.js"></script>
    <!-- BOOTSTRAP -->
    <script type="text/javascript" src="js/vendor/bootstrap.min.js"></script>

    <!-- SUBSCRIBE MAILCHIMP -->
    <script type="text/javascript" src="js/vendor/subscribe/subscribe_validate.js"></script>

    <!-- VALIDATION  -->
    <script type="text/javascript" src="js/vendor/validate/jquery.validate.min.js"></script>


    <!-- SLIDER JS FILES -->
    <script type="text/javascript" src="js/vendor/slider/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/vendor/slider/carousel.js"></script>

    <!-- SUBSCRIBE MAILCHIMP -->
    <script type="text/javascript" src="js/vendor/subscribe/subscribe_validate.js"></script>

    <!-- IMAGE OVERLAY JS FILE -->
    <script type="text/javascript" src="js/vendor/img-overlay/modernizr.js"></script>
    <script type="text/javascript" src="js/vendor/img-overlay/overlay.js"></script>
    <!-- LIGHT BOX GALLERY -->
    <script type="text/javascript" src="js/vendor/lightbox/ekko-lightbox.js"></script>
    <script type="text/javascript" src="js/vendor/lightbox/lightbox.js"></script>

    <!-- VIDEO -->
    <script type="text/javascript" src="js/vendor/video/video.js"></script>

    <!-- COUNTER JS FILES -->
    <script type="text/javascript" src="js/vendor/counter/counter-lib.js"></script>
    <script type="text/javascript" src="js/vendor/counter/jquery.counterup.min.js"></script>

    <!-- PHONE_NUMBER VALIDATION JS -->
    <script type="text/javascript" src="js/vendor/phone_number/phone_number.js"></script>

    <!-- STYLE SWITCHER JS -->
    <script type="text/javascript" src="js/vendor/style_switcher/jquery-template.js"></script>
    <script type="text/javascript" src="js/vendor/style_switcher/settings.js"></script>

    <!-- THEME JS -->
    <script type="text/javascript" src="js/custom/custom.js"></script>

</body>

</html>