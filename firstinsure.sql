-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 13, 2021 at 05:06 AM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `firstinsure`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `type` varchar(100) NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `name`, `password`, `type`, `timestamp`) VALUES
(1, 'vishal@firstinsure.in', 'Vishal', '123456', 'admin', '2021-01-10 06:24:46'),
(2, 'Edelwiess@firstinsure.in', 'Edelwiess_etli', '123456', 'sub_admin', '2021-01-10 08:54:57'),
(3, 'First@firstinsure.in', 'First Caller', '123456', 'caller', '2021-01-10 08:54:47');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` varchar(6) NOT NULL DEFAULT '',
  `city_name` varchar(18) DEFAULT NULL,
  `state_id` varchar(7) DEFAULT NULL,
  `city_code` varchar(8) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_name`, `state_id`, `city_code`) VALUES
('1', 'Raipur', '4', 'CG001'),
('2', 'Dhanbad', '12', 'JH001'),
('3', 'Ranchi', '12', 'JH002'),
('4', 'Bhubaneshwar', '26', 'OD001'),
('5', 'Cuttack', '26', 'OD002'),
('6', 'Kolkata', '33', 'WB001'),
('7', 'Chandigarh', '28', 'CH001'),
('8', 'Delhi', '5', 'DL001'),
('9', 'Gurgaon', '9', 'HR001'),
('10', 'Jalandhar', '27', 'PB001'),
('11', 'Ludhiana', '27', 'PB002'),
('12', 'Noida', '31', 'UP001'),
('13', 'Lucknow', '31', 'UP002'),
('14', 'Kanpur', '31', 'UP003'),
('15', 'Bareilly', '31', 'UP004'),
('16', 'Dehradun', '32', 'UK001'),
('17', 'Guntur', '1', 'AP001'),
('18', 'Hyderabad', '1', 'AP002'),
('19', 'Vijayawada', '1', 'AP003'),
('20', 'Visakhapatnam', '1', 'AP004'),
('21', 'Warangal', '1', 'AP005'),
('22', 'Bangalore', '13', 'KA001'),
('23', 'Mangalore', '13', 'KA002'),
('24', 'Mysore', '13', 'KA003'),
('25', 'Cochin', '14', 'KL001'),
('26', 'Thrissur', '14', 'KL002'),
('27', 'Alappuzha', '14', 'KL003'),
('28', 'Idukki', '14', 'KL004'),
('29', 'Kannur', '14', 'KL005'),
('30', 'Kasargod', '14', 'KL006'),
('31', 'Kollam', '14', 'KL007'),
('32', 'Kottayam', '14', 'KL008'),
('33', 'Kozhikode', '14', 'KL009'),
('34', 'Malappuram', '14', 'KL010'),
('35', 'Palakkad', '14', 'KL011'),
('36', 'Pathanamthitta', '14', 'KL012'),
('37', 'Thiruvananthapuram', '14', 'KL013'),
('38', 'Wayanad', '14', 'KL014'),
('39', 'Chennai', '30', 'TN001'),
('40', 'Coimbatore', '30', 'TN002'),
('41', 'Madurai', '30', 'TN003'),
('42', 'Tiruchirappalli', '30', 'TN004'),
('43', 'Ahmedabad', '6', 'GJ001'),
('44', 'Bharuch', '6', 'GJ002'),
('45', 'Surat', '6', 'GJ003'),
('46', 'Vadodara', '6', 'GJ004'),
('47', 'Valsad', '6', 'GJ005'),
('48', 'Bhopal', '17', 'MP001'),
('49', 'Indore', '17', 'MP002'),
('50', 'Jabalpur', '17', 'MP003'),
('51', 'Mumbai', '18', 'MH001'),
('52', 'Thane', '18', 'MH002'),
('53', 'Pune', '18', 'MH003'),
('54', 'Nashik', '18', 'MH004'),
('55', 'Nagpur', '18', 'MH005'),
('56', 'Aurangabad', '18', 'MH006');

-- --------------------------------------------------------

--
-- Table structure for table `digital_card`
--

CREATE TABLE `digital_card` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `mobile` varchar(200) NOT NULL DEFAULT '',
  `designation` varchar(100) NOT NULL DEFAULT '',
  `whatsApp` varchar(100) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `skype_id` varchar(100) NOT NULL DEFAULT '',
  `fb_id` varchar(200) NOT NULL DEFAULT '',
  `insta_id` varchar(200) NOT NULL DEFAULT '',
  `tw_id` varchar(250) NOT NULL DEFAULT '',
  `linkedin_id` varchar(200) NOT NULL DEFAULT '',
  `youtube_id` varchar(200) NOT NULL DEFAULT '',
  `pint_id` varchar(200) NOT NULL DEFAULT '',
  `image` varchar(200) NOT NULL DEFAULT '',
  `video` varchar(200) NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `final_lead`
--

CREATE TABLE `final_lead` (
  `id` int(11) NOT NULL,
  `salutation` varchar(100) NOT NULL DEFAULT '',
  `first_name` varchar(100) NOT NULL DEFAULT '',
  `last_name` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `mobile` varchar(30) NOT NULL DEFAULT '',
  `dob` varchar(100) NOT NULL DEFAULT '',
  `gender` varchar(20) NOT NULL DEFAULT '',
  `state_id` varchar(30) NOT NULL DEFAULT '',
  `pincode` varchar(100) NOT NULL DEFAULT '',
  `city_code` text NOT NULL,
  `appointment_date` varchar(100) NOT NULL DEFAULT '',
  `allocated_to` varchar(100) NOT NULL DEFAULT '',
  `allocated_at` varchar(100) NOT NULL DEFAULT '',
  `address_1` varchar(200) NOT NULL DEFAULT '',
  `address_2` varchar(100) NOT NULL DEFAULT '',
  `income` varchar(100) NOT NULL DEFAULT '',
  `cc_remarks` varchar(250) NOT NULL DEFAULT '',
  `follow_up_time` varchar(100) NOT NULL DEFAULT '',
  `status` varchar(30) NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `final_lead`
--

INSERT INTO `final_lead` (`id`, `salutation`, `first_name`, `last_name`, `email`, `mobile`, `dob`, `gender`, `state_id`, `pincode`, `city_code`, `appointment_date`, `allocated_to`, `allocated_at`, `address_1`, `address_2`, `income`, `cc_remarks`, `follow_up_time`, `status`, `timestamp`) VALUES
(1, 'Mr', 'krishna', ' sharma', 'admin@gmail.com', '9716435404', '2020-12-02', 'Male', '9', '122001', 'Gurgaon', '2021-01-13T14:22', 'N00986', '2021-01-0211:11', 'address df', 'dd', '10000', 'testing', '2021-02-22T11:11', 'Done', '2021-01-02 13:36:43'),
(2, 'Mr', 'Ajay', 'Sharma', 'krishna22@gmail.com', '9988877774', '2000-10-30', 'Male', '9', '122001', 'HR001', '', '', '2021-01-09 05:07:11', '290 sector 4', '290 sector 4', '200000', 'testing', '', 'Assigned', '2021-01-09 05:07:12'),
(3, 'Mr', 'Vishal', 'Sharma', 'vishal.5228@gmail.com', '7570080001', '1999-03-04', 'Male', '5', '11001', 'Delhi', '2021-01-09T15:32', '', '2021-01-09 05:27:34', 'Sector 11', 'Sector 11', '10000', 'testing', '', 'Assigned', '2021-01-09 05:48:51'),
(4, 'Mr', 'Vishal ', 'Sharma', 'vishal.5228@gmail.com', '9811290839', '1991-12-01', 'Male', '5', '110096', 'DL001', '2021-01-09T15:00', 'N70031', '2021-01-09 06:06:41', 'B 319 B1', 'New Ashok Nagar', '5000000', 'testing', '', 'Assigned', '2021-01-09 06:06:51'),
(5, 'Mr', 'Sangita ', 'Singh', 'abc@gmail.com', '8882955130', '1983-12-03', 'Female', '5', '110053', 'Delhi', '2021-01-11T19:00', 'N0A394', '2021-01-11 11:49:57', 'C 7 126 Yamuna Vihar Near by C 6 market', ' landmark hanuman vatika 110053', '45000', '', '', 'Assigned', '2021-01-11 11:49:59'),
(6, 'Mr', 'Sangita ', 'Singh', 'abc@gmail.com', '8882955130', '1983-12-03', 'Female', '5', '110053', 'DL001', '2021-01-11T19:00', 'N0A394', '2021-01-11 12:03:36', 'C 7 126 Yamuna Vihar Near by C 6 market', 'landmark hanuman vatika 110053', '45000', '', '', 'Assigned', '2021-01-11 12:03:43'),
(7, 'Mr', 'Saiyad ', 'Aslam', 'abc@gmail.com', '9880241954', '1991-12-09', 'Male', '13', '560066', 'KA001', '2021-01-11T20:08', 'N00216', '2021-01-11 12:08:54', 'Door No 375 3rd Floor  V K Nagar  ', 'Near By Shiva Temple  ', '50000', '', '', 'Assigned', '2021-01-11 12:09:03'),
(8, 'Mr', 'Himanshu', '', 'abc@gmail.com', '9451901268', '1991-12-01', 'Male', '18', '400013', 'MH001', '2021-01-11T20:08', 'N0A699', '2021-01-11 12:14:15', 'D1 4 6 Opp Apna Bazar Vashi Sector ', '1 Navi Mumbai 400703', '39000', '', '', 'Assigned', '2021-01-11 12:14:21'),
(9, 'Mr', 'Lokesh', '', 'abc@gmail.com', '9880307455', '1990-01-24', 'Male', '13', '560008', 'KA001', '2021-01-11T20:00', 'N01399', '2021-01-11 12:22:15', 'HN 155 Bank colony 9th cross 9th Main Nearby ', 'Om Shakti Temple Srinivishnar 560028', '65000', '', '', 'Assigned', '2021-01-11 12:22:22'),
(10, 'Mr', 'Noor', 'Alam', 'abc@gmail.com', '9163721313', '1986-07-25', 'Male', '33', '700013', 'WB001', '2021-01-11T20:00', 'N01457', '2021-01-11 12:43:45', 'Building No H 9  Room No 6 14  4th Floor  Nirmali ', 'New Market Police Station  Kolkata 700013', '70000', '', '', 'Assigned', '2021-01-11 12:43:52'),
(11, 'Mr', 'Korla ', 'Jayaram', '', '9290906042', '1978-07-24', 'Male', '1', '530009', 'AP004', '2021-01-12T17:00', 'N01361', '2021-01-12 05:34:27', 'House No G 2 Tatayya Ground Floor Sujatha Nagar ', ' Near By High Field Gol Vishakhapatnam 530051', '1300000', '', '', 'Assigned', '2021-01-12 05:34:35'),
(12, 'Mr', 'PrakashNarayan ', 'Tiwari', '', '9082375869', '1974-07-23', 'Male', '18', '400008', 'MH001', '2021-01-12T17:00', 'N00376', '2021-01-12 05:49:13', 'SAMS Fruit Products Pvt Ltd 3B Mapkhan Compound ', 'Mapkhan Nagar Marol Andheri E Mumbai 400059', '670000', '', '', 'Assigned', '2021-01-12 05:49:21'),
(13, 'Mr', 'SachinPratap ', 'Pawar', '', '9987933123', '1978-02-28', 'Male', '18', '400008', 'MH001', '2021-01-12T19:00', 'N0A779', '2021-01-12 05:53:55', 'House No 1363 Room No 13 Section 32 Near By ', ' Abhishek Apartment Ullhas Nagar ', '500000', '', '', 'Assigned', '2021-01-12 05:54:03'),
(14, 'Mr', 'Floyd McAtee', '', '', '7003172488', '1984-08-28', 'Male', '33', '700001', 'WB001', '2021-01-12T14:30', 'N01457', '2021-01-12 05:57:50', 'House No 47 Chandra Nath Roy Rd Picnic Garden ', 'Tiljala Kolkata 700039 Landmark Next To Tiljala ', '860000', '', '', 'Assigned', '2021-01-12 05:57:59'),
(15, 'Mr', 'Jitender Pratap Singh', '', '', '9913479246', '1968-12-02', 'Male', '6', '380008', 'GJ001', '2021-01-12T16:00', 'N01375', '2021-01-12 06:02:18', 'House No 24 72 Asarwa Ahemedabad 380016 Near By ', 'ESI Hospotal D7 Near By Asarwa Talao', '1200000', '', '', 'Assigned', '2021-01-12 06:02:25'),
(16, 'Mr', 'Philips Bonta', '', '', '9920151252', '1975-09-22', 'Male', '18', '400008', 'MH001', '2021-01-12T15:00', 'N0A220', '2021-01-12 06:07:27', 'House No 501 Naigaon East Juchandra Chandrapada Rd', 'Mahalakshmi Nagar Naigaon East Mumbai 401202 Land ', '400000', 'Own Business', '', 'Assigned', '2021-01-12 06:07:35'),
(17, 'Mr', 'Raghunath ', 'Choudhary', '', '9922330694', '1982-05-17', 'Male', '18', '422004', 'MH004', '2021-01-12T18:00', 'N00986', '2021-01-12 10:03:15', 'Geeta nagar bus stop builder road Opp', 'Behind shiva hotel builder road nashik 422004', '450000', '', '', 'Assigned', '2021-01-12 10:03:21'),
(18, 'Mr', 'Prakash Sharma', '', '', '9703368804', '1986-12-02', 'Male', '5', '110030', 'DL001', '2021-01-13T16:00', 'N70031', '2021-01-13 08:13:10', 'Company Black Smith Project pvt ltd 100 Foota', 'Road Ghitorni Pincode 110030 Designation Supervisi', '600000', '', '', 'Assigned', '2021-01-13 08:13:17');

-- --------------------------------------------------------

--
-- Table structure for table `hms`
--

CREATE TABLE `hms` (
  `id` int(30) NOT NULL,
  `city` varchar(18) DEFAULT NULL,
  `agent_code` varchar(9) DEFAULT NULL,
  `status` varchar(6) DEFAULT NULL,
  `designation_code` varchar(15) DEFAULT NULL,
  `derived_designation` varchar(18) DEFAULT NULL,
  `agent_name` varchar(34) DEFAULT NULL,
  `bm_code` varchar(7) DEFAULT NULL,
  `bm_name` varchar(29) DEFAULT NULL,
  `ash_code` varchar(8) DEFAULT NULL,
  `ash_name` varchar(28) DEFAULT NULL,
  `cbh_code` varchar(8) DEFAULT NULL,
  `cbh_name` varchar(26) DEFAULT NULL,
  `rh_code` varchar(7) DEFAULT NULL,
  `rh_name` varchar(21) DEFAULT NULL,
  `zh_code` varchar(7) DEFAULT NULL,
  `zh_name` varchar(20) DEFAULT NULL,
  `nh_code` varchar(7) DEFAULT NULL,
  `nh_name` varchar(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hms`
--

INSERT INTO `hms` (`id`, `city`, `agent_code`, `status`, `designation_code`, `derived_designation`, `agent_name`, `bm_code`, `bm_name`, `ash_code`, `ash_name`, `cbh_code`, `cbh_name`, `rh_code`, `rh_name`, `zh_code`, `zh_name`, `nh_code`, `nh_name`) VALUES
(1, 'Chennai', 'N01146', 'ACTIVE', 'D - BDM', 'RM', 'Mithra .', 'N00258', 'Dhamodharakumar K', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(2, 'Chennai', 'N00409', 'ACTIVE', 'D - BDM', 'RM', 'A Raja', 'N00258', 'Dhamodharakumar K', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(3, 'Cochin', 'N01818', 'ACTIVE', 'D - DRM', 'RM', 'BIBIN MATHEW', 'N00329', 'Anish Kumar A Krishnan', 'N70071', 'Abhilash K K', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(4, 'Cochin', 'N01853', 'ACTIVE', 'D - SBDM', 'RM', 'SHEENAMOL P D', 'N00329', 'Anish Kumar A Krishnan', 'N70071', 'Abhilash K K', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(5, 'Cochin', 'N01946', 'ACTIVE', 'D - BDM', 'RM', 'MINTU LIVERA', 'N00329', 'Anish Kumar A Krishnan', 'N70071', 'Abhilash K K', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(6, 'Cochin', 'N01964', 'ACTIVE', 'D - DFE', 'RM', 'CHANDRADASAN K P', 'N00329', 'Anish Kumar A Krishnan', 'N70071', 'Abhilash K K', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(7, 'Cochin', 'N0A694', 'ACTIVE', 'D - SFE', 'RM', 'VISHNU RAJ', 'N01534', 'SAHEED ABOOBACKER P A', 'N70071', 'Abhilash K K', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(8, 'Cochin', 'N01160', 'ACTIVE', 'D - FE', 'RM', 'Bibin T P', 'N00329', 'Anish Kumar A Krishnan', 'N70071', 'Abhilash K K', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(9, 'Cochin', 'N0A731', 'ACTIVE', 'D - SFE', 'RM', 'AJEESH H', 'N01534', 'SAHEED ABOOBACKER P A', 'N70071', 'Abhilash K K', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(10, 'Cochin', 'N01846', 'ACTIVE', 'D - SFE', 'RM', 'MANEESH KUMAR M', 'N00329', 'Anish Kumar A Krishnan', 'N70071', 'Abhilash K K', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(11, 'Cochin', 'N0A062', 'ACTIVE', 'D - AFE', 'RM', 'BEENA JACOB', 'N01534', 'SAHEED ABOOBACKER P A', 'N70071', 'Abhilash K K', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(12, 'Cochin', 'N0A661', 'ACTIVE', 'D - SFE', 'RM', 'ARUN PRAKASH', 'N01534', 'SAHEED ABOOBACKER P A', 'N70071', 'Abhilash K K', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(13, 'Thiruvananthapuram', 'N0A064', 'ACTIVE', 'D - SRM', 'RM', 'REJITH R', 'N0A217', 'HONEY P J', 'N7A005', 'JUSTIN ABRAHAM', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(14, 'Cochin', 'N0A579', 'ACTIVE', 'D - SFE', 'RM', 'SARITHA K SALIM', 'N0A217', 'HONEY P J', 'N7A005', 'JUSTIN ABRAHAM', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(15, 'Cochin', 'N0A706', 'ACTIVE', 'D - SFE', 'RM', 'HEMIL M VARGHESE', 'N0A217', 'HONEY P J', 'N7A005', 'JUSTIN ABRAHAM', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(16, 'Thrissur', 'N0A649', 'ACTIVE', 'D - DFE', 'RM', 'SRAVAN P M', 'N0A413', 'RAJI DILEEP', 'N7A005', 'JUSTIN ABRAHAM', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(17, 'Cochin', 'N0A724', 'ACTIVE', 'D - FE', 'RM', 'NITHIN JOSEPH', 'N0A217', 'HONEY P J', 'N7A005', 'JUSTIN ABRAHAM', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(18, 'Cochin', 'N0A272', 'ACTIVE', 'D - DRM', 'RM', 'RAJEENA RAJEENA', 'N0A413', 'RAJI DILEEP', 'N7A005', 'JUSTIN ABRAHAM', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(19, 'Thrissur', 'N0A301', 'ACTIVE', 'D - AFE', 'RM', 'SIBI P XAVIER', 'N0A413', 'RAJI DILEEP', 'N7A005', 'JUSTIN ABRAHAM', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(20, 'Cochin', 'N0A106', 'ACTIVE', 'D - AFE', 'RM', 'REETHU ANTONY N', 'N0A217', 'HONEY P J', 'N7A005', 'JUSTIN ABRAHAM', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(21, 'Thrissur', 'N0A647', 'ACTIVE', 'D - FE', 'RM', 'SREEKANTH P NAIR', 'N0A413', 'RAJI DILEEP', 'N7A005', 'JUSTIN ABRAHAM', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(22, 'Thrissur', 'N0A790', 'ACTIVE', 'D - BDM', 'RM', 'JITHESH M J', 'N0A413', 'RAJI DILEEP', 'N7A005', 'JUSTIN ABRAHAM', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(23, 'Chennai', 'N0A123', 'ACTIVE', 'D - SFE', 'RM', 'DIVYA PRIYA RAMACHANDRAN', 'N00181', 'Sakthivel R S', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(24, 'Chennai', 'N0A734', 'ACTIVE', 'D - BDM', 'RM', 'MADHAVAN KRISHNAMOORTHY', 'N00181', 'Sakthivel R S', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(25, 'Chennai', 'N0A674', 'ACTIVE', 'D - BDM', 'RM', 'MARTIN LAWRANCE M', 'N00258', 'Dhamodharakumar K', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(26, 'Chennai', 'N01919', 'ACTIVE', 'D - DFE', 'RM', 'MANIGANDAN MANIGANDAN', 'N00181', 'Sakthivel R S', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(27, 'Chennai', 'N00246', 'ACTIVE', 'D - SBDM', 'RM', 'Sathish Kumar', 'N00181', 'Sakthivel R S', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(28, 'Chennai', 'N0A704', 'ACTIVE', 'D - BDM', 'RM', 'RAJ KUMAR SHANMUGAM', 'N00258', 'Dhamodharakumar K', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(29, 'Trichy', 'N0A080', 'ACTIVE', 'D - DFE', 'RM', 'ROYALRAJ R', 'N00258', 'Dhamodharakumar K', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(30, 'Chennai', 'N0A557', 'ACTIVE', 'D - FE', 'RM', 'VIGNESHWARAN M', 'N00258', 'Dhamodharakumar K', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(31, 'Chennai', 'N0A671', 'ACTIVE', 'D - DFE', 'RM', 'JEEVANANTHAN MARIMUTHU', 'N00258', 'Dhamodharakumar K', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(32, 'Chennai', 'N00271', 'ACTIVE', 'D - SFE', 'RM', 'B Anandkarthik', 'N0A766', 'GOPIKRISHNAN PANDIYAN', 'N7A026', 'SENTHIL KUMAR S', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(33, 'Chennai', 'N00270', 'ACTIVE', 'D - BDM', 'RM', 'Ganesan .', 'N00181', 'Sakthivel R S', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(34, 'Chennai', 'N00240', 'ACTIVE', 'D - BDM', 'RM', 'Suresh Mohan', 'N00181', 'Sakthivel R S', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(35, 'Hyderabad', 'N01373', 'ACTIVE', 'D - BDM', 'RM', 'SAMBA SPANDANA', 'N01229', 'Bujunuru Kranthi', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(36, 'Hyderabad', 'N01283', 'ACTIVE', 'D - SBDM', 'RM', 'Bhaskar Reddy Dantapally', 'N01229', 'Bujunuru Kranthi', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(37, 'Hyderabad', 'N01312', 'ACTIVE', 'D - DFE', 'RM', 'RAMPANGU PRATYUSHA', 'N01229', 'Bujunuru Kranthi', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(38, 'Hyderabad', 'N01309', 'ACTIVE', 'D - SBDM', 'RM', 'BODDU MANIKESH KUMAR', 'N01229', 'Bujunuru Kranthi', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(39, 'Hyderabad', 'N0A775', 'ACTIVE', 'D - AFE', 'RM', 'KANDUKURI KAVYA SREE', 'N0A361', 'VIDYASAGAR BODDI REDDY', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(40, 'Hyderabad', 'N0A514', 'ACTIVE', 'D - AFE', 'RM', 'ANGOOR SOWMYA', 'N0A361', 'VIDYASAGAR BODDI REDDY', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(41, 'Hyderabad', 'N0A538', 'ACTIVE', 'D - SFE', 'RM', 'M BALARAM', 'N0A361', 'VIDYASAGAR BODDI REDDY', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(42, 'Hyderabad', 'N0A554', 'ACTIVE', 'D - BDM', 'RM', 'VASUPALLI HEMANTH KUMAR', 'N0A361', 'VIDYASAGAR BODDI REDDY', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(43, 'Hyderabad', 'N0A424', 'ACTIVE', 'D - FE', 'RM', 'KANDHUKURI HARISH', 'N0A361', 'VIDYASAGAR BODDI REDDY', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(44, 'Hyderabad', 'N0A207', 'ACTIVE', 'D - DFE', 'RM', 'PUNNA BHANU CHANDER', 'N00198', 'Ajay Maddipatla', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(45, 'Hyderabad', 'N0A652', 'ACTIVE', 'D - BDM', 'RM', 'VALLAPUREDDY MAHENDER REDDY', 'N00198', 'Ajay Maddipatla', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(46, 'Hyderabad', 'N0A567', 'ACTIVE', 'D - AFE', 'RM', 'RANGISETTI ANITHA', 'N01229', 'Bujunuru Kranthi', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(47, 'Hyderabad', 'N0A780', 'ACTIVE', 'D - DFE', 'RM', 'PRASANYA LAKSHMI DEVI', 'N00198', 'Ajay Maddipatla', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(48, 'Hyderabad', 'N0A072', 'ACTIVE', 'D - DRM', 'RM', 'KONDAM SKYLAB', 'N00198', 'Ajay Maddipatla', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(49, 'Hyderabad', 'N01404', 'ACTIVE', 'D - SBDM', 'RM', 'SWATHI MADABHUSHINI', 'N01026', 'T Arun Kumar', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(50, 'Hyderabad', 'N01932', 'ACTIVE', 'D - DCRM', 'RM', 'PENTA VENKATESHAM', 'N01831', 'CENIGARAM SRIKANTH', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(51, 'Hyderabad', 'N01943', 'ACTIVE', 'D - SFE', 'RM', 'CHAGANTI NAGA JYOTHI', 'N01831', 'CENIGARAM SRIKANTH', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(52, 'Hyderabad', 'N0A570', 'ACTIVE', 'D - BDM', 'RM', 'BODA SRAVANTHI', 'N00537', 'G V N Manikanta Bejawada', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(53, 'Hyderabad', 'N00038', 'ACTIVE', 'D - SBDM', 'RM', 'Chirra Naresh', 'N01831', 'CENIGARAM SRIKANTH', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(54, 'Hyderabad', 'N00967', 'ACTIVE', 'D - FE', 'RM', 'Bonagiri Chinamareswarrao', 'N01026', 'T Arun Kumar', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(55, 'Hyderabad', 'N00083', 'ACTIVE', 'D - SRM', 'RM', 'G Arun Kumar', 'N01026', 'T Arun Kumar', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(56, 'Hyderabad', 'N0A018', 'ACTIVE', 'D - BDM', 'RM', 'KATIKALA CHANDRASEKHAR', 'N00537', 'G V N Manikanta Bejawada', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(57, 'Hyderabad', 'N0A586', 'ACTIVE', 'D - BDM', 'RM', 'BIJJA RAMAKRISHNA', 'N01831', 'CENIGARAM SRIKANTH', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(58, 'Hyderabad', 'N01812', 'ACTIVE', 'D - BDM', 'RM', 'VANKDAVATH VIJAY KUMAR', 'N00537', 'G V N Manikanta Bejawada', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(59, 'Hyderabad', 'N01935', 'ACTIVE', 'D - BDM', 'RM', 'RAJESH C J', 'N01831', 'CENIGARAM SRIKANTH', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(60, 'Hyderabad', 'N0A771', 'ACTIVE', 'D - AFE', 'RM', 'PERAM LAKSHMI', 'N01026', 'T Arun Kumar', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(61, 'Hyderabad', 'N0A772', 'ACTIVE', 'D - AFE', 'RM', 'PAVULURU SRI HARITHA', 'N01026', 'T Arun Kumar', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(62, 'Hyderabad', 'N01957', 'ACTIVE', 'D - SFE', 'RM', 'SHAIK JILANI', 'N00537', 'G V N Manikanta Bejawada', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(63, 'Hyderabad', 'N0A370', 'ACTIVE', 'D - BDM', 'RM', 'BHARATH KUMAR NEELAKANTI', 'N01026', 'T Arun Kumar', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(64, 'Bangalore', 'N01399', 'ACTIVE', 'D - SBDM', 'RM', 'SRIKANT KUMAR PADHY', 'N00042', 'Shankar K', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(65, 'Mangalore', 'N0A132', 'ACTIVE', 'D - AFE', 'RM', 'SHWETHA SHWETHA', 'N0A308', 'CHANDRASHEKHER PATIL', 'N7A008', 'KUMARA D B', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(66, 'Bangalore', 'N0A598', 'ACTIVE', 'D - SFE', 'RM', 'PRAKASH N', 'N0A464', 'RAJASHEKAR T', 'N7A008', 'KUMARA D B', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(67, 'Bangalore', 'N01430', 'ACTIVE', 'D - BDM', 'RM', 'RAGHU T', 'N0A631', 'JITENDRA KUMAR', 'N70080', 'Deepak Kumar', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(68, 'Bangalore', 'N0A398', 'ACTIVE', 'D - DFE', 'RM', 'NAWAZ SHARIFF', 'N0A631', 'JITENDRA KUMAR', 'N70080', 'Deepak Kumar', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(69, 'Bangalore', 'N0A754', 'ACTIVE', 'D - SFE', 'RM', 'SANGEETA NARENDRA', 'N0A631', 'JITENDRA KUMAR', 'N70080', 'Deepak Kumar', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(70, 'Bangalore', 'N0A774', 'ACTIVE', 'D - DRM', 'RM', 'AMJAD KHAN', 'N0A631', 'JITENDRA KUMAR', 'N70080', 'Deepak Kumar', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(71, 'Bangalore', 'N0A596', 'ACTIVE', 'D - SFE', 'RM', 'LAVANYA KUMARI B', 'N0A464', 'RAJASHEKAR T', 'N7A008', 'KUMARA D B', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(72, 'Bangalore', 'N0A397', 'ACTIVE', 'D - FE', 'RM', 'PRATHIBHA G', 'N0A308', 'CHANDRASHEKHER PATIL', 'N7A008', 'KUMARA D B', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(73, 'Bangalore', 'N0A513', 'ACTIVE', 'D - SFE', 'RM', 'SANTOSH M', 'N0A308', 'CHANDRASHEKHER PATIL', 'N7A008', 'KUMARA D B', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(74, 'Bangalore', 'N0A518', 'ACTIVE', 'D - SFE', 'RM', 'BHANU PRAKASH R', 'N0A308', 'CHANDRASHEKHER PATIL', 'N7A008', 'KUMARA D B', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(75, 'Bangalore', 'N0A483', 'ACTIVE', 'D - DRM', 'RM', 'ABHISHEK G K', 'N0A464', 'RAJASHEKAR T', 'N7A008', 'KUMARA D B', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(76, 'Bangalore', 'N0A484', 'ACTIVE', 'D - SFE', 'RM', 'KISHORE KUMAR D', 'N0A464', 'RAJASHEKAR T', 'N7A008', 'KUMARA D B', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(77, 'Bangalore', 'N02018', 'ACTIVE', 'D - SBDM', 'RM', 'SYED ALHA BAKASH KHATAI', 'N00216', 'Amit Kumar', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(78, 'Bangalore', 'N01140', 'ACTIVE', 'D - DRM', 'RM', 'Sreenivasa Rao Goraga', 'N00046', 'Tanneru Vijay Kumar', 'N70080', 'Deepak Kumar', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(79, 'Bangalore', 'N01719', 'ACTIVE', 'D - SFE', 'RM', 'ANIL KUMAR', 'N00046', 'Tanneru Vijay Kumar', 'N70080', 'Deepak Kumar', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(80, 'Bangalore', 'N01998', 'ACTIVE', 'D - BDM', 'RM', 'S ABDUL HAFEEZ', 'N00046', 'Tanneru Vijay Kumar', 'N70080', 'Deepak Kumar', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(81, 'Bangalore', 'N0A485', 'ACTIVE', 'D - RM', 'RM', 'MAHAMED MUJEEB AHAMED KHAN', 'N00046', 'Tanneru Vijay Kumar', 'N70080', 'Deepak Kumar', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(82, 'Bangalore', 'N01913', 'ACTIVE', 'D - SFE', 'RM', 'SACHIN PANDEY', 'N00216', 'Amit Kumar', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(83, 'Bangalore', 'N0A660', 'ACTIVE', 'D - AFE', 'RM', 'SELVAM THEJESH KUMAR', 'N00082', 'Sumantkumar .', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(84, 'Bangalore', 'N0A235', 'ACTIVE', 'D - SBDM', 'RM', 'AMITH GANAPATHY', 'N00216', 'Amit Kumar', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(85, 'Bangalore', 'N0A750', 'ACTIVE', 'D - BDM', 'RM', 'QAMRUL HASAN', 'N00082', 'Sumantkumar .', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(86, 'Bangalore', 'N0A046', 'ACTIVE', 'D - DRM', 'RM', 'NARINDER SINGH', 'N00082', 'Sumantkumar .', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(87, 'Bangalore', 'N0A695', 'ACTIVE', 'D - SFE', 'RM', 'CHANDAN B S', 'N00082', 'Sumantkumar .', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(88, 'Bangalore', 'N0A183', 'ACTIVE', 'D - DCRM', 'RM', 'AMBARISHA AMBARISHA', 'N00042', 'Shankar K', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(89, 'Bangalore', 'N0A336', 'ACTIVE', 'D - DRM', 'RM', 'NARAYANASWAMY V', 'N00042', 'Shankar K', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(90, 'Bangalore', 'N0A373', 'ACTIVE', 'D - SFE', 'RM', 'MILLAN SAHOO', 'N00042', 'Shankar K', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(91, 'Bangalore', 'N00057', 'ACTIVE', 'D - RM', 'RM', 'Darshan U', 'N00042', 'Shankar K', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(92, 'Bangalore', 'N0A578', 'ACTIVE', 'D - FE', 'RM', 'SARMADI KHANUM', 'N00042', 'Shankar K', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(93, 'Bangalore', 'N0A686', 'ACTIVE', 'D - BDM', 'RM', 'SOWMYA N', 'N00042', 'Shankar K', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(94, 'Lucknow', 'N01144', 'ACTIVE', 'D - FE', 'RM', 'Neha Tiwari', 'N01803', 'DEEPESH SINGH', 'N70049', 'Neeraj Shukla', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(95, 'Lucknow', 'N0A197', 'ACTIVE', 'D - SBDM', 'RM', 'GAURAV PANDEY', 'N01803', 'DEEPESH SINGH', 'N70049', 'Neeraj Shukla', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(96, 'Lucknow', 'N0A230', 'ACTIVE', 'D - AFE', 'RM', 'DEEP SHIKHA', 'N01803', 'DEEPESH SINGH', 'N70049', 'Neeraj Shukla', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(97, 'Bareilly', 'N0A409', 'ACTIVE', 'D - FE', 'RM', 'ANIL KUMAR', 'N01746', 'RAVI KUMAR SAXENA', 'N70049', 'Neeraj Shukla', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(98, 'Bareilly', 'N0A499', 'ACTIVE', 'D - DFE', 'RM', 'PREETI SHARMA', 'N01746', 'RAVI KUMAR SAXENA', 'N70049', 'Neeraj Shukla', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(99, 'Lucknow', 'N01035', 'ACTIVE', 'D - SFE', 'RM', 'Ambrish Trivedi', 'N01803', 'DEEPESH SINGH', 'N70049', 'Neeraj Shukla', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(100, 'Kanpur', 'N0A096', 'ACTIVE', 'D - BDM', 'RM', 'MANEESH KUMAR KUSHWAHA', 'N01627', 'BRIJENDRA SHARMA', 'N7A016', 'JAI PRAKASH', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(101, 'Kanpur', 'N0A606', 'ACTIVE', 'D - SBDM', 'RM', 'JAY SINGH', 'N01643', 'PRAFULL CHOWDHARY', 'N7A016', 'JAI PRAKASH', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(102, 'Kanpur', 'N0A657', 'ACTIVE', 'D - FE', 'RM', 'ABHISHEK TIWARI TIWARI', 'N01643', 'PRAFULL CHOWDHARY', 'N7A016', 'JAI PRAKASH', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(103, 'Kanpur', 'N0A777', 'ACTIVE', 'D - SFE', 'RM', 'ARUN KUMAR SINGH', 'N01643', 'PRAFULL CHOWDHARY', 'N7A016', 'JAI PRAKASH', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(104, 'Kanpur', 'N01890', 'ACTIVE', 'D - SRM', 'RM', 'VINOD KUMAR SINGH', 'N01643', 'PRAFULL CHOWDHARY', 'N7A016', 'JAI PRAKASH', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(105, 'Lucknow', 'N0A412', 'ACTIVE', 'D - DFE', 'RM', 'PRADIP KUMAR', 'N01627', 'BRIJENDRA SHARMA', 'N7A016', 'JAI PRAKASH', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(106, 'Kanpur', 'N0A668', 'ACTIVE', 'D - SFE', 'RM', 'ADITYA KUMAR AWASTHI', 'N01627', 'BRIJENDRA SHARMA', 'N7A016', 'JAI PRAKASH', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(107, 'Lucknow', 'N0A637', 'ACTIVE', 'D - AFE', 'RM', 'VIVEK KUMAR TRIPATHI', 'N0A619', 'ANOOP HARMAN DAS', 'N7A023', 'TANVIKA BEG', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(108, 'Lucknow', 'N0A664', 'ACTIVE', 'D - BDM', 'RM', 'SANDEEP KUMAR SRIVASTAVA', 'N0A619', 'ANOOP HARMAN DAS', 'N7A023', 'TANVIKA BEG', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(109, 'Bareilly', 'N01956', 'ACTIVE', 'D - FE', 'RM', 'NIKITA SHARMA', 'N01746', 'RAVI KUMAR SAXENA', 'N70049', 'Neeraj Shukla', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(110, 'Bareilly', 'N0A165', 'ACTIVE', 'D - AFE', 'RM', 'PIYUSH KUMAR', 'N01746', 'RAVI KUMAR SAXENA', 'N70049', 'Neeraj Shukla', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(111, 'Lucknow', 'N0A673', 'ACTIVE', 'D - SFE', 'RM', 'RAJU PRASAD', 'N0A619', 'ANOOP HARMAN DAS', 'N7A023', 'TANVIKA BEG', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(112, 'Lucknow', 'N0A745', 'ACTIVE', 'D - FE', 'RM', 'ASHUTOSH TIWARI', 'N0A619', 'ANOOP HARMAN DAS', 'N7A023', 'TANVIKA BEG', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(113, 'Lucknow', 'N0A793', 'ACTIVE', 'D - SFE', 'RM', 'SHADAB KHAN', 'N0A619', 'ANOOP HARMAN DAS', 'N7A023', 'TANVIKA BEG', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(114, 'Kanpur', 'N0A460', 'ACTIVE', 'D - BDM', 'RM', 'HIRALAL YADAV', 'N01627', 'BRIJENDRA SHARMA', 'N7A016', 'JAI PRAKASH', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(115, 'Delhi', 'N0A585', 'ACTIVE', 'D - AFE', 'RM', 'SHIKHA SHIKHA', 'N01151', 'Lalit Sharma', 'N70031', 'Manohar Singh', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(116, 'Bareilly', 'N01238', 'ACTIVE', 'D - FE', 'RM', 'Shivendra Singh Chauhan', 'N01746', 'RAVI KUMAR SAXENA', 'N70049', 'Neeraj Shukla', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(117, 'Delhi', 'N01552', 'ACTIVE', 'D - SBDM', 'RM', 'DHEERAJ TIWARI', 'N01151', 'Lalit Sharma', 'N70031', 'Manohar Singh', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(118, 'Delhi', 'N0A394', 'ACTIVE', 'D - BDM', 'RM', 'RAJAT THAKUR', 'N01151', 'Lalit Sharma', 'N70031', 'Manohar Singh', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(119, 'Delhi', 'N0A581', 'ACTIVE', 'D - FE', 'RM', 'RAKESH KUMAR', 'N01151', 'Lalit Sharma', 'N70031', 'Manohar Singh', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(120, 'Delhi', 'N0A406', 'ACTIVE', 'D - SBDM', 'RM', 'HIMANSHU KHANNA', 'N00299', 'Naveen Kumar', 'N70031', 'Manohar Singh', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(121, 'Delhi', 'N0A504', 'ACTIVE', 'D - AFE', 'RM', 'SANTOSH YADAV', 'N00299', 'Naveen Kumar', 'N70031', 'Manohar Singh', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(122, 'Delhi', 'N0A717', 'ACTIVE', 'D - FE', 'RM', 'POONAM YADAV', 'N00299', 'Naveen Kumar', 'N70031', 'Manohar Singh', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(123, 'Delhi', 'N0A716', 'ACTIVE', 'D - SFE', 'RM', 'ANKIT KUMAR SHIV', 'N00469', 'Ankit Kumar', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(124, 'Delhi', 'N0A146', 'ACTIVE', 'D - RM', 'RM', 'POOJA SHREE', 'N00469', 'Ankit Kumar', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(125, 'Delhi', 'N0A715', 'ACTIVE', 'D - BDM', 'RM', 'SAKSHAM CHOUDHARY', 'N0A260', 'RISHIKESH JHA', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(126, 'Delhi', 'N0A714', 'ACTIVE', 'D - FE', 'RM', 'PRAVENDER SINGH', 'N0A260', 'RISHIKESH JHA', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(127, 'Delhi', 'N0A746', 'ACTIVE', 'D - BDM', 'RM', 'SAROJ GANDHI', 'N0A260', 'RISHIKESH JHA', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(128, 'Delhi', 'N0A778', 'ACTIVE', 'D - SFE', 'RM', 'ABHIJEET SHYAM CHOWDHURY', 'N00469', 'Ankit Kumar', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(129, 'Delhi', 'N0A228', 'ACTIVE', 'D - RM', 'RM', 'KHUSHBOO KUMARI', 'N00469', 'Ankit Kumar', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(130, 'Delhi', 'N0A459', 'ACTIVE', 'D - DRM', 'RM', 'NILESH KUMAR', 'N00469', 'Ankit Kumar', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(131, 'Delhi', 'N00972', 'ACTIVE', 'D - SBDM', 'RM', 'Mukesh Sharma', 'N0A478', 'DHIRAJ NARULA', 'N7A004', 'NITIN KUMAR', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(132, 'Delhi', 'N00662', 'ACTIVE', 'D - CRM', 'RM', 'Vijay Kumar', 'N00596', 'Ajeet Kumar', 'N7A004', 'NITIN KUMAR', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(133, 'Delhi', 'N0A601', 'ACTIVE', 'D - FE', 'RM', 'ARUN ARORA', 'N0A478', 'DHIRAJ NARULA', 'N7A004', 'NITIN KUMAR', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(134, 'Delhi', 'N01690', 'ACTIVE', 'D - FE', 'RM', 'SIMA SINGH', 'N00596', 'Ajeet Kumar', 'N7A004', 'NITIN KUMAR', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(135, 'Delhi', 'N0A683', 'ACTIVE', 'D - SFE', 'RM', 'GAURAV KUMAR MAURYA', 'N00596', 'Ajeet Kumar', 'N7A004', 'NITIN KUMAR', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(136, 'Nashik', 'N01062', 'ACTIVE', 'D - SFE', 'RM', 'Bhushan Pandharinath Amrutkar', 'N00986', 'Shailesh Ramdas Mahajan', 'N70043', 'Lalita D Kolte', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(137, 'Bhopal', 'N01548', 'ACTIVE', 'D - FE', 'RM', 'NARENDRA RAGHUWANSHI', 'N00093', 'Anand Singh Chauhan', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(138, 'Bhopal', 'N0A527', 'ACTIVE', 'D - FE', 'RM', 'SUSHMA SHARMA', 'N00093', 'Anand Singh Chauhan', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(139, 'Bhopal', 'N01625', 'ACTIVE', 'D - SBDM', 'RM', 'TARUN MISHRA', 'N0A688', 'MANISH ACHARYA', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(140, 'Bhopal', 'N0A607', 'ACTIVE', 'D - BDM', 'RM', 'SABIHA NAGORI', 'N0A688', 'MANISH ACHARYA', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(141, 'Bhopal', 'N0A600', 'ACTIVE', 'D - SFE', 'RM', 'MAHENDRA KUMAR VERMA', 'N0A688', 'MANISH ACHARYA', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(142, 'Bhopal', 'N0A785', 'ACTIVE', 'D - AFE', 'RM', 'NANDKISHORE KEER', 'N0A688', 'MANISH ACHARYA', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(143, 'Indore', 'N0A705', 'ACTIVE', 'D - BDM', 'RM', 'VIKRAM KAUSHAL', 'N0A691', 'TEJENDRA DADORE', 'N70104', 'AKHILESH KUMAR GUPTA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(144, 'Bhopal', 'N01848', 'ACTIVE', 'D - FE', 'RM', 'AKASH AJMERA', 'N00093', 'Anand Singh Chauhan', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(145, 'Bhopal', 'N00184', 'ACTIVE', 'D - FE', 'RM', 'Sapna Mishra', 'N0A688', 'MANISH ACHARYA', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(146, 'Bhopal', 'N00110', 'ACTIVE', 'D - BDM', 'RM', 'Amit Chourasiya', 'N0A688', 'MANISH ACHARYA', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(147, 'Bhopal', 'N01558', 'ACTIVE', 'D - SFE', 'RM', 'RAM SINGH TOMAR', 'N0A688', 'MANISH ACHARYA', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(148, 'Indore', 'N0A681', 'ACTIVE', 'D - AFE', 'RM', 'RAKESH MANDRAI', 'N0A728', 'SUNIL GURJAR', 'N70104', 'AKHILESH KUMAR GUPTA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(149, 'Bhopal', 'N0A701', 'ACTIVE', 'D - FE', 'RM', 'SONAM SINGH', 'N00093', 'Anand Singh Chauhan', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(150, 'Bhopal', 'N00386', 'ACTIVE', 'D - DCRM', 'RM', 'Ritu Agrawal', 'N00093', 'Anand Singh Chauhan', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(151, 'Bhopal', 'N00109', 'ACTIVE', 'D - SCRM', 'RM', 'Sudha Faye', 'N00093', 'Anand Singh Chauhan', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(152, 'Indore', 'N01949', 'ACTIVE', 'D - DRM', 'RM', 'MANISH URDHWARESHE', 'N0A691', 'TEJENDRA DADORE', 'N70104', 'AKHILESH KUMAR GUPTA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(153, 'Nagpur', 'N0A642', 'ACTIVE', 'D - FE', 'RM', 'SUSHIL SEVAKRAM TONGE', 'N0A669', 'ASHOK SHRI KRUSHNA GABHANE', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(154, 'Indore', 'N0A732', 'ACTIVE', 'D - DFE', 'RM', 'PREM SONERE', 'N0A691', 'TEJENDRA DADORE', 'N70104', 'AKHILESH KUMAR GUPTA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(155, 'Indore', 'N0A805', 'ACTIVE', 'D - BDM', 'RM', 'NISHANT JAGGI', 'N0A691', 'TEJENDRA DADORE', 'N70104', 'AKHILESH KUMAR GUPTA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(156, 'Indore', 'N0A565', 'ACTIVE', 'D - DRM', 'RM', 'MOHIT JAIN', 'N0A728', 'SUNIL GURJAR', 'N70104', 'AKHILESH KUMAR GUPTA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(157, 'Indore', 'N0A629', 'ACTIVE', 'D - BDM', 'RM', 'REENA HARBAT THOMAS', 'N0A728', 'SUNIL GURJAR', 'N70104', 'AKHILESH KUMAR GUPTA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(158, 'Indore', 'N0A636', 'ACTIVE', 'D - SFE', 'RM', 'AMAN JOSHI', 'N0A728', 'SUNIL GURJAR', 'N70104', 'AKHILESH KUMAR GUPTA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(159, 'Nagpur', 'N0A747', 'ACTIVE', 'D - AFE', 'RM', 'ASHISH ASHOK CHAUBEY', 'N0A763', 'VARIJ SANJEEV DUBEY', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(160, 'Nagpur', 'N01085', 'ACTIVE', 'D - DRM', 'RM', 'Mahesh Mangilal Sharma', 'N0A669', 'ASHOK SHRI KRUSHNA GABHANE', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(161, 'Nagpur', 'N01432', 'ACTIVE', 'D - DRM', 'RM', 'JYOTI SAHARE', 'N0A669', 'ASHOK SHRI KRUSHNA GABHANE', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(162, 'Nagpur', 'N01824', 'ACTIVE', 'D - SFE', 'RM', 'ROSHAN KESHAV RAMTEKE', 'N0A669', 'ASHOK SHRI KRUSHNA GABHANE', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(163, 'Nagpur', 'N0A733', 'ACTIVE', 'D - AFE', 'RM', 'KIRAN SHEKHAR PANTAVANE', 'N0A763', 'VARIJ SANJEEV DUBEY', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(164, 'Nagpur', 'N0A799', 'ACTIVE', 'D - SFE', 'RM', 'GOPAL BHARDWAJ', 'N0A763', 'VARIJ SANJEEV DUBEY', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(165, 'Nagpur', 'N0A650', 'ACTIVE', 'D - DFE', 'RM', 'PRIYANKA MILIND KELKAR', 'N0A669', 'ASHOK SHRI KRUSHNA GABHANE', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(166, 'Nagpur', 'N0A659', 'ACTIVE', 'D - DFE', 'RM', 'RAVI KAMLESH MOHABIA', 'N0A763', 'VARIJ SANJEEV DUBEY', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(167, 'Nagpur', 'N0A670', 'ACTIVE', 'D - DFE', 'RM', 'JAYSHREE RAMESH KOHAD', 'N0A763', 'VARIJ SANJEEV DUBEY', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(168, 'Nagpur', 'N0A738', 'ACTIVE', 'D - AFE', 'RM', 'ANKITA ISHWARCHANDRA SHAHU', 'N0A763', 'VARIJ SANJEEV DUBEY', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(169, 'Bhopal', 'N01406', 'ACTIVE', 'D - RM', 'RM', 'SEEMA CHOUREY', 'N00093', 'Anand Singh Chauhan', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(170, 'Aurangabad', 'N0A620', 'ACTIVE', 'D - DFE', 'RM', 'SACHIN KACHARU JADHAV', 'N00423', 'Atul V Athawale', 'N70043', 'Lalita D Kolte', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(171, 'Aurangabad', 'N0A684', 'ACTIVE', 'D - AFE', 'RM', 'MAHENDRA BHAGWANRAO DABHADE', 'N00423', 'Atul V Athawale', 'N70043', 'Lalita D Kolte', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(172, 'Aurangabad', 'N01461', 'ACTIVE', 'D - BDM', 'RM', 'SANTOSH BHIMRAO WAGHMARE', 'N00423', 'Atul V Athawale', 'N70043', 'Lalita D Kolte', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(173, 'Aurangabad', 'N0A399', 'ACTIVE', 'D - AFE', 'RM', 'MILIND ARJUN BANSODE', 'N00423', 'Atul V Athawale', 'N70043', 'Lalita D Kolte', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(174, 'Aurangabad', 'N0A616', 'ACTIVE', 'D - FE', 'RM', 'ONKAR DEEPAK DHARMADHIKARI', 'N00423', 'Atul V Athawale', 'N70043', 'Lalita D Kolte', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(175, 'Nashik', 'N0A124', 'ACTIVE', 'D - BDM', 'RM', 'PRAVIN SUKADEO SAGARE', 'N00986', 'Shailesh Ramdas Mahajan', 'N70043', 'Lalita D Kolte', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(176, 'Nashik', 'N0A256', 'ACTIVE', 'D - FE', 'RM', 'PRERNA UDAY PATIL', 'N00986', 'Shailesh Ramdas Mahajan', 'N70043', 'Lalita D Kolte', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(177, 'Nashik', 'N01687', 'ACTIVE', 'D - SBDM', 'RM', 'PRASAD KEWAL JAGTAP', 'N00986', 'Shailesh Ramdas Mahajan', 'N70043', 'Lalita D Kolte', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(178, 'Nashik', 'N0A594', 'ACTIVE', 'D - BDM', 'RM', 'YATINDRASINGH PARBHATSINGH GIRASE', 'N0A591', 'SAGAR SHIVAJI GHUMRE', 'N7A019', 'VISHAL SURESH BHAMARE', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(179, 'Nashik', 'N0A648', 'ACTIVE', 'D - SFE', 'RM', 'ANANT SHAMRAO TONPE', 'N0A591', 'SAGAR SHIVAJI GHUMRE', 'N7A019', 'VISHAL SURESH BHAMARE', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(180, 'Nashik', 'N0A635', 'ACTIVE', 'D - AFE', 'RM', 'SWAPNIL DILIP SANGPAL', 'N0A633', 'SANDESH PRABHAKAR GAVANDE', 'N7A019', 'VISHAL SURESH BHAMARE', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(181, 'Nashik', 'N0A719', 'ACTIVE', 'D - BDM', 'RM', 'PRATAP VALMIK GAIKE', 'N0A633', 'SANDESH PRABHAKAR GAVANDE', 'N7A019', 'VISHAL SURESH BHAMARE', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(182, 'Nashik', 'N0A726', 'ACTIVE', 'D - AFE', 'RM', 'PRAFFUL GOVIND GAIKWAD', 'N0A633', 'SANDESH PRABHAKAR GAVANDE', 'N7A019', 'VISHAL SURESH BHAMARE', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(183, 'Vadodara', 'N0A034', 'ACTIVE', 'D - FE', 'RM', 'HETAL DINESHBHAI PARMAR', 'N00364', 'Himanshu M Thakkar', 'N7A021', 'SANDIPKUMAR SHAH', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(184, 'Vadodara', 'N0A730', 'ACTIVE', 'D - DFE', 'RM', 'CHAITANYA HANDE', 'N00364', 'Himanshu M Thakkar', 'N7A021', 'SANDIPKUMAR SHAH', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(185, 'Vadodara', 'N0A003', 'ACTIVE', 'D - SBDM', 'RM', 'SAJID VHORA', 'N00372', 'Sabbirhusen Yunusbhai Rana', 'N7A021', 'SANDIPKUMAR SHAH', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(186, 'Ahmedabad', 'N01992', 'ACTIVE', 'D - SBDM', 'RM', 'HARDIKKUMAR MAKAVANA', 'N00321', 'Akhil Goyal', 'N70084', 'Ravi Kamleshbhai Thakkar', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(187, 'Ahmedabad', 'N0A762', 'ACTIVE', 'D - SFE', 'RM', 'KALPESH PANDYA', 'N01375', 'HARSH KIRANBHAI PANCHAL', 'N70084', 'Ravi Kamleshbhai Thakkar', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(188, 'Ahmedabad', 'N0A612', 'ACTIVE', 'D - AFE', 'RM', 'JAY ATULKUMAR MEHTA', 'N01375', 'HARSH KIRANBHAI PANCHAL', 'N70084', 'Ravi Kamleshbhai Thakkar', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(189, 'Ahmedabad', 'N0A326', 'ACTIVE', 'D - DFE', 'RM', 'VISHNU RAMESHWAR DHOBI', 'N00321', 'Akhil Goyal', 'N70084', 'Ravi Kamleshbhai Thakkar', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(190, 'Ahmedabad', 'N01838', 'ACTIVE', 'D - FE', 'RM', 'TANUJ SEVAK', 'N00321', 'Akhil Goyal', 'N70084', 'Ravi Kamleshbhai Thakkar', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(191, 'Ahmedabad', 'N01865', 'ACTIVE', 'D - SFE', 'RM', 'RAVI RAVI', 'N00321', 'Akhil Goyal', 'N70084', 'Ravi Kamleshbhai Thakkar', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(192, 'Vadodara', 'N0A680', 'ACTIVE', 'D - AFE', 'RM', 'ANJALIBEN THAKUR', 'N00372', 'Sabbirhusen Yunusbhai Rana', 'N7A021', 'SANDIPKUMAR SHAH', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(193, 'Vadodara', 'N0A749', 'ACTIVE', 'D - BDM', 'RM', 'NILESH HARSHADBHAI PATHAK', 'N00372', 'Sabbirhusen Yunusbhai Rana', 'N7A021', 'SANDIPKUMAR SHAH', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(194, 'Vadodara', 'N0A640', 'ACTIVE', 'D - BDM', 'RM', 'URVI PUROHIT', 'N00372', 'Sabbirhusen Yunusbhai Rana', 'N7A021', 'SANDIPKUMAR SHAH', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(195, 'Vadodara', 'N0A663', 'ACTIVE', 'D - DFE', 'RM', 'PRITESH SHRIKANTBHAI PATNI', 'N00372', 'Sabbirhusen Yunusbhai Rana', 'N7A021', 'SANDIPKUMAR SHAH', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(196, 'Mumbai', 'N0A385', 'ACTIVE', 'D - DFE', 'RM', 'MEGHA CHANDRAKANT WAGH', 'N0A205', 'PRAMOD PAWAR', 'N70067', 'Ganesh Gaonkar', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(197, 'Mumbai', 'N0A626', 'ACTIVE', 'D - AFE', 'RM', 'SAIRAJ DHANRAJ THAPA', 'N0A205', 'PRAMOD PAWAR', 'N70067', 'Ganesh Gaonkar', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(198, 'Mumbai', 'N0A741', 'ACTIVE', 'D - SFE', 'RM', 'VISHWANATH PRAJAPATI', 'N0A205', 'PRAMOD PAWAR', 'N70067', 'Ganesh Gaonkar', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(199, 'Mumbai', 'N0A462', 'ACTIVE', 'D - DFE', 'RM', 'SAGAR RAVINDRA CHANDWADKAR', 'N00384', 'Suresh B Sumra', 'N70067', 'Ganesh Gaonkar', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(200, 'Mumbai', 'N0A709', 'ACTIVE', 'D - FE', 'RM', 'SWETA SINGH', 'N00384', 'Suresh B Sumra', 'N70067', 'Ganesh Gaonkar', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(201, 'Mumbai', 'N0A722', 'ACTIVE', 'D - BDM', 'RM', 'GANESH RAMESH KADAM', 'N00384', 'Suresh B Sumra', 'N70067', 'Ganesh Gaonkar', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(202, 'Mumbai', 'N0A559', 'ACTIVE', 'D - DFE', 'RM', 'YOGESH UTTAM THORAT', 'N0A298', 'BHARAT RAMESH SHIVHARE', 'N7A009', 'PRASHANT VASHISHTH MUNDHE', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(203, 'Mumbai', 'N01795', 'ACTIVE', 'D - SFE', 'RM', 'RAVINDRA NANU KEDAR', 'N01927', 'KALPESH DATTARAM JOIL', 'N7A009', 'PRASHANT VASHISHTH MUNDHE', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(204, 'Mumbai', 'N0A295', 'ACTIVE', 'D - DFE', 'RM', 'SHEKHAR BHARATRAO SUPEKAR', 'N0A298', 'BHARAT RAMESH SHIVHARE', 'N7A009', 'PRASHANT VASHISHTH MUNDHE', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(205, 'Mumbai', 'N0A347', 'ACTIVE', 'D - AFE', 'RM', 'RAKESHKUMAR RAMNARAYAN VISHWAKARMA', 'N0A298', 'BHARAT RAMESH SHIVHARE', 'N7A009', 'PRASHANT VASHISHTH MUNDHE', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(206, 'Mumbai', 'N0A453', 'ACTIVE', 'D - AFE', 'RM', 'SIYARAM PAL', 'N0A075', 'MOHD SUHEL SHAMASHADA', 'N7A009', 'PRASHANT VASHISHTH MUNDHE', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S');
INSERT INTO `hms` (`id`, `city`, `agent_code`, `status`, `designation_code`, `derived_designation`, `agent_name`, `bm_code`, `bm_name`, `ash_code`, `ash_name`, `cbh_code`, `cbh_name`, `rh_code`, `rh_name`, `zh_code`, `zh_name`, `nh_code`, `nh_name`) VALUES
(207, 'Mumbai', 'N0A806', 'ACTIVE', 'D - SFE', 'RM', 'ROHIT SANJAY DUDHAGAONKAR', 'N0A608', 'DARSHAN BHOLA GUPTA', 'N7A018', 'DIPESH KUMAR', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(208, 'Mumbai', 'N0A545', 'ACTIVE', 'D - FE', 'RM', 'PRASHANT ASHOK NALAWADE', 'N01927', 'KALPESH DATTARAM JOIL', 'N7A009', 'PRASHANT VASHISHTH MUNDHE', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(209, 'Mumbai', 'N0A759', 'ACTIVE', 'D - SBDM', 'RM', 'SHERBAHADUR GANGAPRASAD VERMA', 'N01927', 'KALPESH DATTARAM JOIL', 'N7A009', 'PRASHANT VASHISHTH MUNDHE', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(210, 'Mumbai', 'N0A801', 'ACTIVE', 'D - AFE', 'RM', 'BHAVESH VIKRANT RANE', 'N01927', 'KALPESH DATTARAM JOIL', 'N7A009', 'PRASHANT VASHISHTH MUNDHE', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(211, 'Mumbai', 'N0A617', 'ACTIVE', 'D - BDM', 'RM', 'SUJATA KRISHNA SHETTY', 'N0A608', 'DARSHAN BHOLA GUPTA', 'N7A018', 'DIPESH KUMAR', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(212, 'Mumbai', 'N0A699', 'ACTIVE', 'D - BDM', 'RM', 'PRASHANT VISHNU PATANGE', 'N0A608', 'DARSHAN BHOLA GUPTA', 'N7A018', 'DIPESH KUMAR', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(213, 'Mumbai', 'N0A779', 'ACTIVE', 'D - SFE', 'RM', 'IMTIYAZ SALEEM PATHAN', 'N0A608', 'DARSHAN BHOLA GUPTA', 'N7A018', 'DIPESH KUMAR', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(214, 'Mumbai', 'N0A788', 'ACTIVE', 'D - SFE', 'RM', 'SUNITA MANGESH DUBEY', 'N0A608', 'DARSHAN BHOLA GUPTA', 'N7A018', 'DIPESH KUMAR', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(215, 'Mumbai', 'N0A792', 'ACTIVE', 'D - SFE', 'RM', 'GAURAV UMESH YADAV', 'N0A608', 'DARSHAN BHOLA GUPTA', 'N7A018', 'DIPESH KUMAR', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(216, 'Mumbai', 'N00379', 'ACTIVE', 'D - DRM', 'RM', 'Sunil Nirmal', 'N00376', 'Vinayak Manoher More', 'N70065', 'Atul Chandraprakash Tiwari', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(217, 'Mumbai', 'N00394', 'ACTIVE', 'D - RM', 'RM', 'Pradeep Kumar Dubey', 'N00376', 'Vinayak Manoher More', 'N70065', 'Atul Chandraprakash Tiwari', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(218, 'Mumbai', 'N00381', 'ACTIVE', 'D - DRM', 'RM', 'Prakash M Jaiswal', 'N00376', 'Vinayak Manoher More', 'N70065', 'Atul Chandraprakash Tiwari', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(219, 'Mumbai', 'N01660', 'ACTIVE', 'D - DFE', 'RM', 'MANISH KUMAR JHA', 'N00622', 'Shiv Varan Tiwari', 'N70065', 'Atul Chandraprakash Tiwari', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(220, 'Mumbai', 'N01664', 'ACTIVE', 'D - SFE', 'RM', 'KAMALUDDIN JALALUDDIN KHAN', 'N00622', 'Shiv Varan Tiwari', 'N70065', 'Atul Chandraprakash Tiwari', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(221, 'Mumbai', 'N01951', 'ACTIVE', 'D - DFE', 'RM', 'GIRISH CHHEDILAL PANDEY', 'N00622', 'Shiv Varan Tiwari', 'N70065', 'Atul Chandraprakash Tiwari', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(222, 'Mumbai', 'N0A157', 'ACTIVE', 'D - DFE', 'RM', 'RUPESH SETE SARKI', 'N00622', 'Shiv Varan Tiwari', 'N70065', 'Atul Chandraprakash Tiwari', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(223, 'Mumbai', 'N0A220', 'ACTIVE', 'D - SBDM', 'RM', 'PRAFUL MAHABAL SALIAN', 'N00622', 'Shiv Varan Tiwari', 'N70065', 'Atul Chandraprakash Tiwari', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(224, 'Mumbai', 'N02006', 'ACTIVE', 'D - AFE', 'RM', 'ARUN KUMAR SAHANI', 'N00743', 'Suraj Perkha', 'N7A011', 'RAMKRISHNA BAJRANG KHATAVKAR', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(225, 'Mumbai', 'N01626', 'ACTIVE', 'D - DFE', 'RM', 'VISHAL VASANT VANGE', 'N00743', 'Suraj Perkha', 'N7A011', 'RAMKRISHNA BAJRANG KHATAVKAR', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(226, 'Bangalore', 'N00210', 'ACTIVE', 'D - SBDM', 'RM', 'Bhavya K', 'N00145', 'S Ranjith Kumar', 'N70010', 'Ratikant Raajbanshi', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(227, 'Bangalore', 'N01610', 'ACTIVE', 'D - RM', 'RM', 'KATHERAGANDLA KISHORE', 'N00145', 'S Ranjith Kumar', 'N70010', 'Ratikant Raajbanshi', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(228, 'Bangalore', 'N01975', 'ACTIVE', 'D - DFE', 'RM', 'BHAVYASHREE B S', 'N00145', 'S Ranjith Kumar', 'N70010', 'Ratikant Raajbanshi', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(229, 'Bangalore', 'N0A269', 'ACTIVE', 'D - AFE', 'RM', 'S ROSHINI', 'N00145', 'S Ranjith Kumar', 'N70010', 'Ratikant Raajbanshi', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(230, 'Bangalore', 'N0A270', 'ACTIVE', 'D - DFE', 'RM', 'AJAY CHANDRAN', 'N00145', 'S Ranjith Kumar', 'N70010', 'Ratikant Raajbanshi', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(231, 'Ahmedabad', 'N0A757', 'ACTIVE', 'D - AFE', 'RM', 'DIPAK JASHVANTBHAI JAISWAL', 'N0A625', 'ARPITA CHAND', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(232, 'Pune', 'N0A761', 'ACTIVE', 'D - FE', 'RM', 'PRABHAKAR SHIVSHARAN KOLI', 'N0A656', 'DILIP SURYAKANT PATIL', 'N7A025', 'VIVEK NARAYANRAO SHINDE', 'Dummy', 'Dummy', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(233, 'Bangalore', 'N0A007', 'ACTIVE', 'D - SFE', 'RM', 'DIXITH P', 'Dummy', 'Dummy', 'N7A008', 'KUMARA D B', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(234, 'Lucknow', 'N7A016', 'ACTIVE', 'D - ASH', 'ASH', 'JAI PRAKASH', 'NA', 'NA', 'NA', 'NA', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(235, 'Chennai', 'N90010', 'ACTIVE', 'D - RH', 'RH', 'Sathick Ahmed U', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(236, 'Bangalore', 'N7A008', 'ACTIVE', 'D - ASH', 'ASH', 'KUMARA D B', 'NA', 'NA', 'NA', 'NA', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(237, 'Chennai', 'N00312', 'ACTIVE', 'D - BDM', 'RM', 'Praveenkumar P', 'N00173', 'Mohan K', 'Dummy', 'Dummy', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(238, 'Chandigarh', 'N0A141', 'ACTIVE', 'D - BM', 'BM', 'ANAND NIGAM', 'N0A141', 'ANAND NIGAM', 'N70024', 'Vishwa Mohan Yadav', 'Dummy', 'Dummy', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(239, 'Ranchi', 'N0A720', 'ACTIVE', 'D - DFE', 'RM', 'RAHUL PURI', 'N01231', 'Jaspreet Kaur Chawla', 'N70086', 'Abhishek Kumar', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(240, 'Ahmedabad', 'N01444', 'ACTIVE', 'D - SBDM', 'RM', 'JAY RAJUBHAI SHAH', 'N00369', 'Shah Chintan Dilipbhai', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(241, 'Pune', 'N0A783', 'ACTIVE', 'D - FE', 'RM', 'KIRAN MAHADEV DANDGE', 'N0A656', 'DILIP SURYAKANT PATIL', 'N7A025', 'VIVEK NARAYANRAO SHINDE', 'Dummy', 'Dummy', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(242, 'Bangalore', 'N01701', 'ACTIVE', 'D - RM', 'RM', 'MOHAMMED AZEEM UDDIN', 'Dummy', 'Dummy', 'N70010', 'Ratikant Raajbanshi', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(243, 'Bhubaneshwar', 'N7A024', 'ACTIVE', 'D - ASH', 'ASH', 'ALOK RANJAN JENA', 'NA', 'NA', 'NA', 'NA', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(244, 'Bangalore', 'N00145', 'ACTIVE', 'D - BM', 'BM', 'S Ranjith Kumar', 'N00145', 'S Ranjith Kumar', 'N70010', 'Ratikant Raajbanshi', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(245, 'Ahmedabad', 'N00370', 'ACTIVE', 'D - SBM', 'BM', 'Suresh Fulchandbhai Nagora', 'N00370', 'Suresh Fulchandbhai Nagora', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(246, 'Hyderabad', 'N01831', 'ACTIVE', 'D - CBM', 'BM', 'CENIGARAM SRIKANTH', 'N01831', 'CENIGARAM SRIKANTH', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(247, 'Ranchi', 'N01175', 'ACTIVE', 'D - FE', 'RM', 'Sweta Kumari', 'Dummy', 'Dummy', 'N70086', 'Abhishek Kumar', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(248, 'Hyderabad', 'N0A760', 'ACTIVE', 'D - SFE', 'RM', 'BOLLAPELLI NAVEEN', 'N0A692', 'DAASARLA JAGADEESHWAR', 'N7A006', 'IRENE SOMA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(249, 'Chandigarh', 'N01365', 'ACTIVE', 'D - BM', 'BM', 'VIJAY KUMAR RANA', 'N01365', 'VIJAY KUMAR RANA', 'N70024', 'Vishwa Mohan Yadav', 'Dummy', 'Dummy', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(250, 'Mumbai', 'N0A181', 'ACTIVE', 'D - DCRM', 'RM', 'KIRAN VISHWAS MAHADIK', 'Dummy', 'Dummy', 'N7A009', 'PRASHANT VASHISHTH MUNDHE', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(251, 'Surat', 'N0A764', 'ACTIVE', 'D - DFE', 'RM', 'SANJAY PAWAR', 'N00960', 'Shailendrasingh Gautam', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(252, 'Vadodara', 'N01695', 'ACTIVE', 'D - DFE', 'RM', 'MANOJ ARVIND PARULKAR', 'N01535', 'JAYENDRAKUMAR KIRITBHAI PATEL', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(253, 'Visakhapatnam', 'N0A605', 'ACTIVE', 'D - SFE', 'RM', 'BOMMALI KANNABABU', 'N00137', 'H Manohara Rao', 'N7A015', 'JAKKAMPUDI VEERA BRAHMACHARI', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(254, 'Nashik', 'N7A019', 'ACTIVE', 'D - ASH', 'ASH', 'VISHAL SURESH BHAMARE', 'NA', 'NA', 'NA', 'NA', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(255, 'Delhi', 'N0A260', 'ACTIVE', 'D - SBM', 'BM', 'RISHIKESH JHA', 'N0A260', 'RISHIKESH JHA', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(256, 'Surat', 'N0A773', 'ACTIVE', 'D - BM', 'BM', 'SUMIT TANWAR', 'N0A773', 'SUMIT TANWAR', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(257, 'Mumbai', 'N0A610', 'ACTIVE', 'D - BDM', 'RM', 'ANKITA SHARAD MAHAKAL', 'Dummy', 'Dummy', 'N7A018', 'DIPESH KUMAR', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(258, 'Ranchi', 'N0A226', 'ACTIVE', 'D - SFE', 'RM', 'PRIYANKA RANA', 'Dummy', 'Dummy', 'N70086', 'Abhishek Kumar', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(259, 'Nashik', 'N0A591', 'ACTIVE', 'D - BM', 'BM', 'SAGAR SHIVAJI GHUMRE', 'N0A591', 'SAGAR SHIVAJI GHUMRE', 'N7A019', 'VISHAL SURESH BHAMARE', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(260, 'Mumbai', 'N00743', 'ACTIVE', 'D - SBM', 'BM', 'Suraj Perkha', 'N00743', 'Suraj Perkha', 'N7A011', 'RAMKRISHNA BAJRANG KHATAVKAR', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(261, 'Mumbai', 'N90013', 'ACTIVE', 'D - RH', 'RH', 'Chandraprakash M Jain', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(262, 'Bhubaneshwar', 'N0A067', 'ACTIVE', 'D - AFE', 'RM', 'JAYANT RAMESH KOLHE', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(263, 'Ahmedabad', 'N0A190', 'ACTIVE', 'D - DFE', 'RM', 'HITESH BHARATBHAI VALA', 'N00370', 'Suresh Fulchandbhai Nagora', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(264, 'Bangalore', 'N0A448', 'ACTIVE', 'D - DRM', 'RM', 'RAJESH KUMAR', 'Dummy', 'Dummy', 'N70010', 'Ratikant Raajbanshi', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(265, 'Ahmedabad', 'N00405', 'ACTIVE', 'D - DFE', 'RM', 'Jainam Shashwat Dalal', 'N00369', 'Shah Chintan Dilipbhai', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(266, 'Mumbai', 'N7A018', 'ACTIVE', 'D - ASH', 'ASH', 'DIPESH KUMAR', 'NA', 'NA', 'NA', 'NA', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(267, 'Surat', 'N00960', 'ACTIVE', 'D - BM', 'BM', 'Shailendrasingh Gautam', 'N00960', 'Shailendrasingh Gautam', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(268, 'Delhi', 'N0A682', 'ACTIVE', 'D - BDM', 'RM', 'SHEKHAR TIWARI', 'Dummy', 'Dummy', 'N70031', 'Manohar Singh', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(269, 'Delhi', 'N00236', 'ACTIVE', 'D - FE', 'RM', 'Suryanarayanan A R', 'Dummy', 'Dummy', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(270, 'Vadodara', 'N0A755', 'ACTIVE', 'D - BM', 'BM', 'PRADIP JOTANGIYA', 'N0A755', 'PRADIP JOTANGIYA', 'N7A021', 'SANDIPKUMAR SHAH', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(271, 'Bhubaneshwar', 'N0A751', 'ACTIVE', 'D - BM', 'BM', 'SHYAMA SUNDAR JENA', 'N0A751', 'SHYAMA SUNDAR JENA', 'N7A024', 'ALOK RANJAN JENA', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(272, 'Lucknow', 'N0A443', 'ACTIVE', 'D - SBDM', 'RM', 'VARUN TIWARI', 'Dummy', 'Dummy', 'N7A023', 'TANVIKA BEG', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(273, 'Delhi', 'N80025', 'ACTIVE', 'D - CBH', 'CBH', 'Abir Dutta', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(274, 'Mumbai', 'N0A075', 'ACTIVE', 'D - BM', 'BM', 'MOHD SUHEL SHAMASHADA', 'N0A075', 'MOHD SUHEL SHAMASHADA', 'N7A009', 'PRASHANT VASHISHTH MUNDHE', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(275, 'Ahmedabad', 'N80027', 'ACTIVE', 'D - CBH', 'CBH', 'Biren Mukeshbhai Shah', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(276, 'Chennai', 'N0A623', 'ACTIVE', 'D - BM', 'BM', 'CHINNA KUMAR', 'N0A623', 'CHINNA KUMAR', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(277, 'Chandigarh', 'N0A387', 'ACTIVE', 'D - DFE', 'RM', 'NOOR SHARMA', 'N0A141', 'ANAND NIGAM', 'N70024', 'Vishwa Mohan Yadav', 'Dummy', 'Dummy', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(278, 'Delhi', 'N7A004', 'ACTIVE', 'D - ASH', 'ASH', 'NITIN KUMAR', 'NA', 'NA', 'NA', 'NA', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(279, 'Mumbai', 'N70067', 'ACTIVE', 'D - ASH', 'ASH', 'Ganesh Gaonkar', 'NA', 'NA', 'NA', 'NA', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(280, 'Hyderabad', 'N80010', 'ACTIVE', 'D - CBH', 'CBH', 'Madhu Madire', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(281, 'Hyderabad', 'N0A769', 'ACTIVE', 'D - FE', 'RM', 'GAJJI SAI KUMAR', 'Dummy', 'Dummy', 'N7A022', 'GIRIBABU NEELA', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(282, 'Bangalore', 'N0A767', 'ACTIVE', 'D - AFE', 'RM', 'PEDINI SPANDANA', 'Dummy', 'Dummy', 'N70010', 'Ratikant Raajbanshi', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(283, 'Visakhapatnam', 'N00137', 'ACTIVE', 'D - CBM', 'BM', 'H Manohara Rao', 'N00137', 'H Manohara Rao', 'N7A015', 'JAKKAMPUDI VEERA BRAHMACHARI', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(284, 'Pune', 'N70089', 'ACTIVE', 'D - ASH', 'ASH', 'Ramakant Hanumantrao Patil', 'NA', 'NA', 'NA', 'NA', 'Dummy', 'Dummy', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(285, 'Nagpur', 'N80008', 'ACTIVE', 'D - CBH', 'CBH', 'Ashish Kumar Kapdi', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(286, 'Bhubaneshwar', 'N0A163', 'ACTIVE', 'D - BM', 'BM', 'NISHANT PRAKASH KHANDELWAL', 'N0A163', 'NISHANT PRAKASH KHANDELWAL', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(287, 'Kolkata', 'N00149', 'ACTIVE', 'D - SBDM', 'RM', 'Mita Saha', 'N00022', 'Bablu Joardar', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(288, 'Hyderabad', 'N70012', 'ACTIVE', 'D - ASH', 'ASH', 'Konkala Laxmi Naidu', 'NA', 'NA', 'NA', 'NA', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(289, 'Delhi', 'N0A794', 'ACTIVE', 'D - FE', 'RM', 'SAMARJEET KUMAR', 'Dummy', 'Dummy', 'N7A004', 'NITIN KUMAR', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(290, 'Visakhapatnam', 'N0A560', 'ACTIVE', 'D - BDM', 'RM', 'PAIDI RAVI KUMAR', 'N00137', 'H Manohara Rao', 'N7A015', 'JAKKAMPUDI VEERA BRAHMACHARI', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(291, 'Delhi', 'N00596', 'ACTIVE', 'D - CBM', 'BM', 'Ajeet Kumar', 'N00596', 'Ajeet Kumar', 'N7A004', 'NITIN KUMAR', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(292, 'Hyderabad', 'N0A743', 'ACTIVE', 'D - BM', 'BM', 'B DIVYA', 'N0A743', 'B DIVYA', 'N70101', 'BALKI SHIVA RAJ', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(293, 'Hyderabad', 'N00064', 'ACTIVE', 'D - SBDM', 'RM', 'B Rayudu', 'Dummy', 'Dummy', 'N70101', 'BALKI SHIVA RAJ', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(294, 'Ahmedabad', 'N0A697', 'ACTIVE', 'D - FE', 'RM', 'MITESH ANILKUMAR PATEL', 'N00370', 'Suresh Fulchandbhai Nagora', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(295, 'Hyderabad', 'N0A727', 'ACTIVE', 'D - FE', 'RM', 'BABA NASIR KHAN', 'N0A692', 'DAASARLA JAGADEESHWAR', 'N7A006', 'IRENE SOMA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(296, 'Cochin', 'N7A005', 'ACTIVE', 'D - ASH', 'ASH', 'JUSTIN ABRAHAM', 'NA', 'NA', 'NA', 'NA', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(297, 'Vijaywada', 'N7A015', 'ACTIVE', 'D - ASH', 'ASH', 'JAKKAMPUDI VEERA BRAHMACHARI', 'NA', 'NA', 'NA', 'NA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(298, 'Chennai', 'N0A766', 'ACTIVE', 'D - BM', 'BM', 'GOPIKRISHNAN PANDIYAN', 'N0A766', 'GOPIKRISHNAN PANDIYAN', 'N7A026', 'SENTHIL KUMAR S', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(299, 'Lucknow', 'N80013', 'ACTIVE', 'D - CBH', 'CBH', 'Anuj Dixit', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(300, 'Chennai', 'N00258', 'ACTIVE', 'D - SBM', 'BM', 'Dhamodharakumar K', 'N00258', 'Dhamodharakumar K', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(301, 'Mumbai', 'N70065', 'ACTIVE', 'D - ASH', 'ASH', 'Atul Chandraprakash Tiwari', 'NA', 'NA', 'NA', 'NA', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(302, 'Mumbai', 'N00622', 'ACTIVE', 'D - CBM', 'BM', 'Shiv Varan Tiwari', 'N00622', 'Shiv Varan Tiwari', 'N70065', 'Atul Chandraprakash Tiwari', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(303, 'Bhopal', 'N00093', 'ACTIVE', 'D - SBM', 'BM', 'Anand Singh Chauhan', 'N00093', 'Anand Singh Chauhan', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(304, 'Vadodara', 'N80029', 'ACTIVE', 'D - CBH', 'CBH', 'Nehal J Sharma', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(305, 'Bangalore', 'N00042', 'ACTIVE', 'D - CBM', 'BM', 'Shankar K', 'N00042', 'Shankar K', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(306, 'Kolkata', 'N0A696', 'ACTIVE', 'D - FE', 'RM', 'SUBHO NANDY', 'N00400', 'Atanu Samadder', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(307, 'Visakhapatnam', 'N0A476', 'ACTIVE', 'D - FE', 'RM', 'MOKA KALA MURTY', 'N00137', 'H Manohara Rao', 'N7A015', 'JAKKAMPUDI VEERA BRAHMACHARI', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(308, 'Hyderabad', 'N01307', 'ACTIVE', 'D - DCRM', 'RM', 'SUBHASH CHANDER JANGAM', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(309, 'Kolkata', 'N00902', 'ACTIVE', 'D - FE', 'RM', 'Arindam Pal', 'N00035', 'Soumitra Nath', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(310, 'Chennai', 'N0A609', 'ACTIVE', 'D - DFE', 'RM', 'VALAVAN BOOPALAN', 'N0A623', 'CHINNA KUMAR', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(311, 'Nagpur', 'N7A020', 'ACTIVE', 'D - ASH', 'ASH', 'JITENDRA KUMAR BANSOD', 'NA', 'NA', 'NA', 'NA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(312, 'Visakhapatnam', 'N01361', 'ACTIVE', 'D - RM', 'RM', 'ADAPA SUSHMA', 'N00137', 'H Manohara Rao', 'N7A015', 'JAKKAMPUDI VEERA BRAHMACHARI', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(313, 'Ranchi', 'N0A549', 'ACTIVE', 'D - SFE', 'RM', 'DEBANJAN MUKHERJEE', 'N01231', 'Jaspreet Kaur Chawla', 'N70086', 'Abhishek Kumar', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(314, 'Bangalore', 'N0A797', 'ACTIVE', 'D - DFE', 'RM', 'VANISHREE N', 'Dummy', 'Dummy', 'N7A027', 'CHANDRASHEKAR CHANDRASHEKAR', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(315, 'Vadodara', 'N00915', 'ACTIVE', 'D - AFE', 'RM', 'Parin S Patel', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(316, 'Kolkata', 'N00023', 'ACTIVE', 'D - CBM', 'BM', 'Vikash Jaiswal', 'N00023', 'Vikash Jaiswal', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(317, 'Cochin', 'N00329', 'ACTIVE', 'D - SBM', 'BM', 'Anish Kumar A Krishnan', 'N00329', 'Anish Kumar A Krishnan', 'N70071', 'Abhilash K K', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(318, 'Delhi', 'N90015', 'ACTIVE', 'D - RH', 'RH', 'Jatin Gupta', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(319, 'Ahmedabad', 'N00369', 'ACTIVE', 'D - SBM', 'BM', 'Shah Chintan Dilipbhai', 'N00369', 'Shah Chintan Dilipbhai', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(320, 'Kolkata', 'N0A556', 'ACTIVE', 'D - RM', 'RM', 'GAYATRI UPADHYAY', 'N00023', 'Vikash Jaiswal', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(321, 'Delhi', 'N00229', 'ACTIVE', 'D - FE', 'RM', 'Sajeev L S', 'Dummy', 'Dummy', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(322, 'Surat', 'N0A665', 'ACTIVE', 'D - BDM', 'RM', 'ANKUR KUMAR PATHAK', 'N00960', 'Shailendrasingh Gautam', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(323, 'Pune', 'N0A748', 'ACTIVE', 'D - SFE', 'RM', 'RAHUL PREM PARGAT', 'N0A656', 'DILIP SURYAKANT PATIL', 'N7A025', 'VIVEK NARAYANRAO SHINDE', 'Dummy', 'Dummy', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(324, 'Bangalore', 'N0A796', 'ACTIVE', 'D - SFE', 'RM', 'LIKHITH R', 'Dummy', 'Dummy', 'N7A027', 'CHANDRASHEKAR CHANDRASHEKAR', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(325, 'Chandigarh', 'N01559', 'ACTIVE', 'D - SBDM', 'RM', 'TARLOCHAN SINGH', 'N01365', 'VIJAY KUMAR RANA', 'N70024', 'Vishwa Mohan Yadav', 'Dummy', 'Dummy', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(326, 'Kolkata', 'N90003', 'ACTIVE', 'D - RH', 'RH', 'Madanmohan Upadhyay', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(327, 'Hyderabad', 'N0A692', 'ACTIVE', 'D - BM', 'BM', 'DAASARLA JAGADEESHWAR', 'N0A692', 'DAASARLA JAGADEESHWAR', 'N7A006', 'IRENE SOMA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(328, 'Mumbai', 'N7A009', 'ACTIVE', 'D - ASH', 'ASH', 'PRASHANT VASHISHTH MUNDHE', 'NA', 'NA', 'NA', 'NA', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(329, 'Cochin', 'N70071', 'ACTIVE', 'D - ASH', 'ASH', 'Abhilash K K', 'NA', 'NA', 'NA', 'NA', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(330, 'Lucknow', 'N0A721', 'ACTIVE', 'D - BDM', 'RM', 'MOHAMMAD SHARIQ SIDDIQUI', 'Dummy', 'Dummy', 'N7A023', 'TANVIKA BEG', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(331, 'Chennai', 'N7A026', 'ACTIVE', 'D - ASH', 'ASH', 'SENTHIL KUMAR S', 'NA', 'NA', 'NA', 'NA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(332, 'Lucknow', 'N0A619', 'ACTIVE', 'D - BM', 'BM', 'ANOOP HARMAN DAS', 'N0A619', 'ANOOP HARMAN DAS', 'N7A023', 'TANVIKA BEG', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(333, 'Vadodara', 'N0A784', 'ACTIVE', 'D - DFE', 'RM', 'PURVI PANKAJKUMAR VYAS', 'N01081', 'Harshit Pathak', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(334, 'Chennai', 'N0A655', 'ACTIVE', 'D - DFE', 'RM', 'S ARAVINDAN', 'N0A623', 'CHINNA KUMAR', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(335, 'Chennai', 'N0A712', 'ACTIVE', 'D - AFE', 'RM', 'PARVEZ BASHA A', 'N0A623', 'CHINNA KUMAR', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(336, 'Ranchi', 'N0A603', 'ACTIVE', 'D - DFE', 'RM', 'SATYAJEET BANERJEE', 'N01231', 'Jaspreet Kaur Chawla', 'N70086', 'Abhishek Kumar', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(337, 'Cuttack', 'N0A551', 'ACTIVE', 'D - AFE', 'RM', 'ABINASH MOHANTY', 'N00019', 'Bibhutibhushan Satapathy', 'N7A024', 'ALOK RANJAN JENA', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(338, 'Mumbai', 'N0A645', 'ACTIVE', 'D - SFE', 'RM', 'SHRIKANT KAILASH PANIGRAHI', 'Dummy', 'Dummy', 'N7A018', 'DIPESH KUMAR', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(339, 'Delhi', 'N0A786', 'ACTIVE', 'D - AFE', 'RM', 'ABDUL RAZZAQ', 'Dummy', 'Dummy', 'N7A004', 'NITIN KUMAR', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(340, 'Bhubaneshwar', 'N01139', 'ACTIVE', 'D - SFE', 'RM', 'Saroj Kumar Mallick', 'N0A751', 'SHYAMA SUNDAR JENA', 'N7A024', 'ALOK RANJAN JENA', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(341, 'Cochin', 'N01534', 'ACTIVE', 'D - SBM', 'BM', 'SAHEED ABOOBACKER P A', 'N01534', 'SAHEED ABOOBACKER P A', 'N70071', 'Abhilash K K', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(342, 'Ranchi', 'N0A644', 'ACTIVE', 'D - AFE', 'RM', 'NIHAL NISHANT', 'Dummy', 'Dummy', 'N70086', 'Abhishek Kumar', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(343, 'Bhopal', 'N0A688', 'ACTIVE', 'D - BM', 'BM', 'MANISH ACHARYA', 'N0A688', 'MANISH ACHARYA', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(344, 'Delhi', 'N01151', 'ACTIVE', 'D - BM', 'BM', 'Lalit Sharma', 'N01151', 'Lalit Sharma', 'N70031', 'Manohar Singh', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(345, 'Bangalore', 'N0A464', 'ACTIVE', 'D - SBM', 'BM', 'RAJASHEKAR T', 'N0A464', 'RAJASHEKAR T', 'N7A008', 'KUMARA D B', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(346, 'Pune', 'N0A729', 'ACTIVE', 'D - FE', 'RM', 'RATNESHWAR DATTARAO CHAUDHARI', 'N0A656', 'DILIP SURYAKANT PATIL', 'N7A025', 'VIVEK NARAYANRAO SHINDE', 'Dummy', 'Dummy', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(347, 'Delhi', 'N70102', 'ACTIVE', 'D - ASH', 'ASH', 'GAUTAM BANERJEE', 'NA', 'NA', 'NA', 'NA', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(348, 'Indore', 'N70104', 'ACTIVE', 'D - ASH', 'ASH', 'AKHILESH KUMAR GUPTA', 'NA', 'NA', 'NA', 'NA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(349, 'Delhi', 'N00392', 'ACTIVE', 'D - CBM', 'BM', 'Sumit Kumar Singh', 'N00392', 'Sumit Kumar Singh', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(350, 'Ahmedabad', 'N01375', 'ACTIVE', 'D - BM', 'BM', 'HARSH KIRANBHAI PANCHAL', 'N01375', 'HARSH KIRANBHAI PANCHAL', 'N70084', 'Ravi Kamleshbhai Thakkar', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(351, 'Chennai', 'N0A708', 'ACTIVE', 'D - DFE', 'RM', 'KAMALI KARUNAKARAN', 'N0A623', 'CHINNA KUMAR', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(352, 'Kolkata', 'N0A624', 'ACTIVE', 'D - AFE', 'RM', 'SANCHITA SINGHA', 'N00197', 'Sreemanta Ghosh', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(353, 'Bhopal', 'N0A698', 'ACTIVE', 'D - BDM', 'RM', 'DAWOOD HUSAIN', 'Dummy', 'Dummy', 'N70014', 'Mukesh Pateriya', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(354, 'Hyderabad', 'N01787', 'ACTIVE', 'D - SCRM', 'RM', 'KAMBHAM RANJITHA', 'N01885', 'GEETEY KASHINATH', 'N70101', 'BALKI SHIVA RAJ', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(355, 'Vadodara', 'N0A589', 'ACTIVE', 'D - DFE', 'RM', 'MINALBEN MISTRY', 'N01081', 'Harshit Pathak', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(356, 'Hyderabad', 'N7A022', 'ACTIVE', 'D - ASH', 'ASH', 'GIRIBABU NEELA', 'NA', 'NA', 'NA', 'NA', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(357, 'Pune', 'N0A355', 'ACTIVE', 'D - BDM', 'RM', 'NITIN VILAS KOSHTI', 'Dummy', 'Dummy', 'N7A025', 'VIVEK NARAYANRAO SHINDE', 'Dummy', 'Dummy', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(358, 'Bangalore', 'N0A308', 'ACTIVE', 'D - BM', 'BM', 'CHANDRASHEKHER PATIL', 'N0A308', 'CHANDRASHEKHER PATIL', 'N7A008', 'KUMARA D B', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(359, 'Bhubaneshwar', 'N01331', 'ACTIVE', 'D - DFE', 'RM', 'SWETASWINI MAHAPATRA', 'N00019', 'Bibhutibhushan Satapathy', 'N7A024', 'ALOK RANJAN JENA', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(360, 'Vadodara', 'N01081', 'ACTIVE', 'D - BM', 'BM', 'Harshit Pathak', 'N01081', 'Harshit Pathak', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(361, 'Chennai', 'N0A628', 'ACTIVE', 'D - BDM', 'RM', 'RAVICHANDRAN K', 'N00173', 'Mohan K', 'Dummy', 'Dummy', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(362, 'Cochin', 'N0A413', 'ACTIVE', 'D - BM', 'BM', 'RAJI DILEEP', 'N0A413', 'RAJI DILEEP', 'N7A005', 'JUSTIN ABRAHAM', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(363, 'Chandigarh', 'N0A643', 'ACTIVE', 'D - BDM', 'RM', 'AJAY KUMAR', 'N01365', 'VIJAY KUMAR RANA', 'N70024', 'Vishwa Mohan Yadav', 'Dummy', 'Dummy', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(364, 'Ahmedabad', 'N0A121', 'ACTIVE', 'D - FE', 'RM', 'VIJAY BASAPATI', 'N00369', 'Shah Chintan Dilipbhai', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(365, 'Ahmedabad', 'N0A758', 'ACTIVE', 'D - AFE', 'RM', 'SHIKHA YADAV', 'N0A625', 'ARPITA CHAND', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(366, 'Kolkata', 'N01258', 'ACTIVE', 'D - DRM', 'RM', 'Sankalan Das', 'N00035', 'Soumitra Nath', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(367, 'Aurangabad', 'N0A296', 'ACTIVE', 'D - BM', 'BM', 'ASHUTOSH KUMAR SINGH', 'N0A296', 'ASHUTOSH KUMAR SINGH', 'N70043', 'Lalita D Kolte', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(368, 'Hyderabad', 'N0A218', 'ACTIVE', 'D - FE', 'RM', 'MADDELA SUNITHA KUMARI', 'N00006', 'Anjaneyulu Minukuri', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(369, 'Hyderabad', 'N7A006', 'ACTIVE', 'D - ASH', 'ASH', 'IRENE SOMA', 'NA', 'NA', 'NA', 'NA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(370, 'Chennai', 'N80018', 'ACTIVE', 'D - CBH', 'CBH', 'Devananth K', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(371, 'Hyderabad', 'N0A434', 'ACTIVE', 'D - AFE', 'RM', 'GEETHEY AMULYA RANI', 'N01885', 'GEETEY KASHINATH', 'N70101', 'BALKI SHIVA RAJ', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(372, 'Surat', 'N0A588', 'ACTIVE', 'D - AFE', 'RM', 'BHAVESH ARVINDBHAI MAISURIYA', 'N00960', 'Shailendrasingh Gautam', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(373, 'Pune', 'N01928', 'ACTIVE', 'D - BM', 'BM', 'DNYANESHWAR VAMAN NARAYANKAR', 'N01928', 'DNYANESHWAR VAMAN NARAYANKAR', 'N7A025', 'VIVEK NARAYANRAO SHINDE', 'Dummy', 'Dummy', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(374, 'Bangalore', 'N70018', 'ACTIVE', 'D - ASH', 'ASH', 'Anil Sharma', 'NA', 'NA', 'NA', 'NA', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(375, 'Lucknow', 'N01069', 'ACTIVE', 'D - SFE', 'RM', 'Upasana Verma', 'Dummy', 'Dummy', 'N7A023', 'TANVIKA BEG', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(376, 'Ranchi', 'N01231', 'ACTIVE', 'D - BM', 'BM', 'Jaspreet Kaur Chawla', 'N01231', 'Jaspreet Kaur Chawla', 'N70086', 'Abhishek Kumar', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(377, 'Chennai', 'N01157', 'ACTIVE', 'D - SFE', 'RM', 'Vivek V', 'N00173', 'Mohan K', 'Dummy', 'Dummy', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(378, 'Bangalore', 'N0A776', 'ACTIVE', 'D - DFE', 'RM', 'BHARATH KUMAR R', 'Dummy', 'Dummy', 'N7A027', 'CHANDRASHEKAR CHANDRASHEKAR', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(379, 'Vadodara', 'N00437', 'ACTIVE', 'D - BDM', 'RM', 'Anilbhai Bhailalbhai Raj', 'N01535', 'JAYENDRAKUMAR KIRITBHAI PATEL', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(380, 'Kolkata', 'N00701', 'ACTIVE', 'D - RM', 'RM', 'Satya Kumari Mishra', 'N00023', 'Vikash Jaiswal', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(381, 'Vadodara', 'N0A803', 'ACTIVE', 'D - BDM', 'RM', 'DHAVALKUMAR JAYANTIBHAI TIRGAR', 'N0A736', 'PUJAN ASHOKKUMAR MANIYAR', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(382, 'Chennai', 'N70098', 'ACTIVE', 'D - ASH', 'ASH', 'ANAND RAJA', 'NA', 'NA', 'NA', 'NA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(383, 'Ahmedabad', 'N0A630', 'ACTIVE', 'D - DFE', 'RM', 'BHAVIK RASHMIKANT DAVE', 'N00370', 'Suresh Fulchandbhai Nagora', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(384, 'Kanpur', 'N0A807', 'ACTIVE', 'D - BDM', 'RM', 'ABHISHEK AWASTHI', 'Dummy', 'Dummy', 'N7A016', 'JAI PRAKASH', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(385, 'Vadodara', 'N00364', 'ACTIVE', 'D - BM', 'BM', 'Himanshu M Thakkar', 'N00364', 'Himanshu M Thakkar', 'N7A021', 'SANDIPKUMAR SHAH', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(386, 'Surat', 'N0A800', 'ACTIVE', 'D - FE', 'RM', 'DAXESH NATVARBHAI OZA', 'N0A773', 'SUMIT TANWAR', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(387, 'Vadodara', 'N0A736', 'ACTIVE', 'D - BM', 'BM', 'PUJAN ASHOKKUMAR MANIYAR', 'N0A736', 'PUJAN ASHOKKUMAR MANIYAR', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(388, 'Hyderabad', 'N00125', 'ACTIVE', 'D - FE', 'RM', 'Chintala Reddy', 'N00006', 'Anjaneyulu Minukuri', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(389, 'Ahmedabad', 'N70084', 'ACTIVE', 'D - ASH', 'ASH', 'Ravi Kamleshbhai Thakkar', 'NA', 'NA', 'NA', 'NA', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(390, 'Kolkata', 'N0A622', 'ACTIVE', 'D - SFE', 'RM', 'PRATAP KUMAR', 'N00197', 'Sreemanta Ghosh', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(391, 'Chandigarh', 'N0A279', 'ACTIVE', 'D - BDM', 'RM', 'ADNAN KHAN', 'Dummy', 'Dummy', 'N70024', 'Vishwa Mohan Yadav', 'Dummy', 'Dummy', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(392, 'Hyderabad', 'N70029', 'ACTIVE', 'D - ASH', 'ASH', 'Ravi Kumar Guthupala', 'NA', 'NA', 'NA', 'NA', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(393, 'Kolkata', 'N01435', 'ACTIVE', 'D - SBDM', 'RM', 'BAPILAL DAS', 'N00022', 'Bablu Joardar', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(394, 'Bangalore', 'N7A027', 'ACTIVE', 'D - ASH', 'ASH', 'CHANDRASHEKAR CHANDRASHEKAR', 'NA', 'NA', 'NA', 'NA', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(395, 'Bangalore', 'N00216', 'ACTIVE', 'D - BM', 'BM', 'Amit Kumar', 'N00216', 'Amit Kumar', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(396, 'Delhi', 'N01113', 'ACTIVE', 'D - BM', 'BM', 'Amit Kumar Kaushik', 'N01113', 'Amit Kumar Kaushik', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(397, 'Kolkata', 'N00197', 'ACTIVE', 'D - CBM', 'BM', 'Sreemanta Ghosh', 'N00197', 'Sreemanta Ghosh', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(398, 'Bangalore', 'N0A798', 'ACTIVE', 'D - SFE', 'RM', 'NIKHIL N', 'Dummy', 'Dummy', 'N7A027', 'CHANDRASHEKAR CHANDRASHEKAR', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(399, 'Ahmedabad', 'N0A713', 'ACTIVE', 'D - AFE', 'RM', 'PRIYANSHI SINGHAL', 'N0A625', 'ARPITA CHAND', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(400, 'Indore', 'N0A728', 'ACTIVE', 'D - BM', 'BM', 'SUNIL GURJAR', 'N0A728', 'SUNIL GURJAR', 'N70104', 'AKHILESH KUMAR GUPTA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(401, 'Bangalore', 'N0A512', 'ACTIVE', 'D - RM', 'RM', 'ABHISHEK DIXIT', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(402, 'Hyderabad', 'N0A718', 'ACTIVE', 'D - SFE', 'RM', 'CEKOTI DURGA PRASAD', 'N0A692', 'DAASARLA JAGADEESHWAR', 'N7A006', 'IRENE SOMA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(403, 'Chennai', 'N00181', 'ACTIVE', 'D - CBM', 'BM', 'Sakthivel R S', 'N00181', 'Sakthivel R S', 'N70098', 'ANAND RAJA', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(404, 'Hyderabad', 'N0A742', 'ACTIVE', 'D - BDM', 'RM', 'MUDIGONDA PRAVEEN KUMAR', 'N0A509', 'SOOROJU RANJITH KUMAR', 'N70101', 'BALKI SHIVA RAJ', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(405, 'Vadodara', 'N0A542', 'ACTIVE', 'D - FE', 'RM', 'JANKI MAHESHKUMAR DESAI', 'N01081', 'Harshit Pathak', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(406, 'Bareilly', 'N7A023', 'ACTIVE', 'D - ASH', 'ASH', 'TANVIKA BEG', 'NA', 'NA', 'NA', 'NA', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(407, 'Kolkata', 'N00035', 'ACTIVE', 'D - BM', 'BM', 'Soumitra Nath', 'N00035', 'Soumitra Nath', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(408, 'Kanpur', 'N01643', 'ACTIVE', 'D - BM', 'BM', 'PRAFULL CHOWDHARY', 'N01643', 'PRAFULL CHOWDHARY', 'N7A016', 'JAI PRAKASH', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(409, 'Vadodara', 'N0A313', 'ACTIVE', 'D - FE', 'RM', 'AMRISH MAHENDRAKUMAR THAKKAR', 'N01535', 'JAYENDRAKUMAR KIRITBHAI PATEL', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(410, 'Delhi', 'N00469', 'ACTIVE', 'D - SBM', 'BM', 'Ankit Kumar', 'N00469', 'Ankit Kumar', 'N70102', 'GAUTAM BANERJEE', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(411, 'Kolkata', 'N0A752', 'ACTIVE', 'D - RM', 'RM', 'ROSHAN KUMAR', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(412, 'Chennai', 'N0A618', 'ACTIVE', 'D - FE', 'RM', 'BALAJI DEENADAYALAN', 'N0A623', 'CHINNA KUMAR', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(413, 'Hyderabad', 'N01026', 'ACTIVE', 'D - BM', 'BM', 'T Arun Kumar', 'N01026', 'T Arun Kumar', 'N70012', 'Konkala Laxmi Naidu', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(414, 'Nashik', 'N0A633', 'ACTIVE', 'D - BM', 'BM', 'SANDESH PRABHAKAR GAVANDE', 'N0A633', 'SANDESH PRABHAKAR GAVANDE', 'N7A019', 'VISHAL SURESH BHAMARE', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(415, 'Mumbai', 'N00376', 'ACTIVE', 'D - SBM', 'BM', 'Vinayak Manoher More', 'N00376', 'Vinayak Manoher More', 'N70065', 'Atul Chandraprakash Tiwari', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(416, 'Delhi', 'N70031', 'ACTIVE', 'D - ASH', 'ASH', 'Manohar Singh', 'NA', 'NA', 'NA', 'NA', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(417, 'Kolkata', 'N0A789', 'ACTIVE', 'D - DFE', 'RM', 'SANTANU BOSE', 'N00400', 'Atanu Samadder', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(418, 'Hyderabad', 'N0A597', 'ACTIVE', 'D - FE', 'RM', 'P SURESH', 'N01147', 'M Rajkumar Goud', 'N7A006', 'IRENE SOMA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(419, 'Bhopal', 'N70014', 'ACTIVE', 'D - ASH', 'ASH', 'Mukesh Pateriya', 'NA', 'NA', 'NA', 'NA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S');
INSERT INTO `hms` (`id`, `city`, `agent_code`, `status`, `designation_code`, `derived_designation`, `agent_name`, `bm_code`, `bm_name`, `ash_code`, `ash_name`, `cbh_code`, `cbh_name`, `rh_code`, `rh_name`, `zh_code`, `zh_name`, `nh_code`, `nh_name`) VALUES
(420, 'Vadodara', 'N00372', 'ACTIVE', 'D - SBM', 'BM', 'Sabbirhusen Yunusbhai Rana', 'N00372', 'Sabbirhusen Yunusbhai Rana', 'N7A021', 'SANDIPKUMAR SHAH', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(421, 'Kolkata', 'N01155', 'ACTIVE', 'D - BDM', 'RM', 'Amit Barman', 'N00023', 'Vikash Jaiswal', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(422, 'Hyderabad', 'N0A634', 'ACTIVE', 'D - FE', 'RM', 'JOGULA VIKRAM KUMAR', 'N01147', 'M Rajkumar Goud', 'N7A006', 'IRENE SOMA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(423, 'Delhi', 'N0A791', 'ACTIVE', 'D - SFE', 'RM', 'SATISH KUMAR SHARMA', 'Dummy', 'Dummy', 'N7A004', 'NITIN KUMAR', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(424, 'Mumbai', 'N0A205', 'ACTIVE', 'D - BM', 'BM', 'PRAMOD PAWAR', 'N0A205', 'PRAMOD PAWAR', 'N70067', 'Ganesh Gaonkar', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(425, 'Kolkata', 'N80009', 'ACTIVE', 'D - CBH', 'CBH', 'Sahana Basu', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(426, 'Chennai', 'N0A678', 'ACTIVE', 'D - BDM', 'RM', 'VENKATESAN M', 'N00173', 'Mohan K', 'Dummy', 'Dummy', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(427, 'Mumbai', 'N80012', 'ACTIVE', 'D - CBH', 'CBH', 'Nikhil Ji Shukla', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(428, 'Ahmedabad', 'N0A430', 'ACTIVE', 'D - SRM', 'RM', 'TEJENDRASINH CHAUHAN', 'N00369', 'Shah Chintan Dilipbhai', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(429, 'Visakhapatnam', 'N0A781', 'ACTIVE', 'D - FE', 'RM', 'DUMPALA RAMESH NAIDU', 'N00137', 'H Manohara Rao', 'N7A015', 'JAKKAMPUDI VEERA BRAHMACHARI', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(430, 'Hyderabad', 'N00198', 'ACTIVE', 'D - SBM', 'BM', 'Ajay Maddipatla', 'N00198', 'Ajay Maddipatla', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(431, 'Ahmedabad', 'N0A679', 'ACTIVE', 'D - AFE', 'RM', 'VIKASH RAVINDRA BHAI NAYAK', 'N0A625', 'ARPITA CHAND', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(432, 'Chandigarh', 'N0A458', 'ACTIVE', 'D - FE', 'RM', 'PANKAJ SHARMA', 'N01365', 'VIJAY KUMAR RANA', 'N70024', 'Vishwa Mohan Yadav', 'Dummy', 'Dummy', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(433, 'Bangalore', 'N90005', 'ACTIVE', 'D - RH', 'RH', 'Santanu Ghosh', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(434, 'Kanpur', 'N01627', 'ACTIVE', 'D - BM', 'BM', 'BRIJENDRA SHARMA', 'N01627', 'BRIJENDRA SHARMA', 'N7A016', 'JAI PRAKASH', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(435, 'Hyderabad', 'N70101', 'ACTIVE', 'D - ASH', 'ASH', 'BALKI SHIVA RAJ', 'NA', 'NA', 'NA', 'NA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(436, 'Bangalore', 'N70010', 'ACTIVE', 'D - ASH', 'ASH', 'Ratikant Raajbanshi', 'NA', 'NA', 'NA', 'NA', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(437, 'Mumbai', 'N01927', 'ACTIVE', 'D - BM', 'BM', 'KALPESH DATTARAM JOIL', 'N01927', 'KALPESH DATTARAM JOIL', 'N7A009', 'PRASHANT VASHISHTH MUNDHE', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(438, 'Mumbai', 'N0A689', 'ACTIVE', 'D - BM', 'BM', 'ARNAV SUNIL NAIR', 'N0A689', 'ARNAV SUNIL NAIR', 'Dummy', 'Dummy', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(439, 'Hyderabad', 'N01885', 'ACTIVE', 'D - SBM', 'BM', 'GEETEY KASHINATH', 'N01885', 'GEETEY KASHINATH', 'N70101', 'BALKI SHIVA RAJ', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(440, 'Bangalore', 'N0A631', 'ACTIVE', 'D - BM', 'BM', 'JITENDRA KUMAR', 'N0A631', 'JITENDRA KUMAR', 'N70080', 'Deepak Kumar', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(441, 'Gurugram', 'N0A478', 'ACTIVE', 'D - BM', 'BM', 'DHIRAJ NARULA', 'N0A478', 'DHIRAJ NARULA', 'N7A004', 'NITIN KUMAR', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(442, 'Hyderabad', 'N0A802', 'ACTIVE', 'D - AFE', 'RM', 'N KEERTHI KUMAR', 'N01147', 'M Rajkumar Goud', 'N7A006', 'IRENE SOMA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(443, 'Bangalore', 'N00046', 'ACTIVE', 'D - BM', 'BM', 'Tanneru Vijay Kumar', 'N00046', 'Tanneru Vijay Kumar', 'N70080', 'Deepak Kumar', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(444, 'Bangalore', 'N0A768', 'ACTIVE', 'D - DRM', 'RM', 'CHIRANJEEVI K', 'Dummy', 'Dummy', 'N70010', 'Ratikant Raajbanshi', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(445, 'Vadodara', 'N01535', 'ACTIVE', 'D - CBM', 'BM', 'JAYENDRAKUMAR KIRITBHAI PATEL', 'N01535', 'JAYENDRAKUMAR KIRITBHAI PATEL', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(446, 'Mumbai', 'N7A011', 'ACTIVE', 'D - ASH', 'ASH', 'RAMKRISHNA BAJRANG KHATAVKAR', 'NA', 'NA', 'NA', 'NA', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(447, 'Hyderabad', 'N01922', 'ACTIVE', 'D - BDM', 'RM', 'MADHAR BASHA', 'N01885', 'GEETEY KASHINATH', 'N70101', 'BALKI SHIVA RAJ', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(448, 'Vadodara', 'N0A804', 'ACTIVE', 'D - AFE', 'RM', 'DEVANG RAKESHBHAI SONI', 'N0A736', 'PUJAN ASHOKKUMAR MANIYAR', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(449, 'Raipur', 'N0A723', 'ACTIVE', 'D - SFE', 'RM', 'RAJENDRA ROUT', 'N0A143', 'RIYA KHURANA', 'N70086', 'Abhishek Kumar', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(450, 'Bangalore', 'N80036', 'ACTIVE', 'D - CBH', 'CBH', 'K HARAPRASAD PATRO', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(451, 'Hyderabad', 'N0A555', 'ACTIVE', 'D - BDM', 'RM', 'BOBBILI CHANDRA PRAKASH', 'N00006', 'Anjaneyulu Minukuri', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(452, 'Mumbai', 'N8A001', 'ACTIVE', 'D - CBH', 'CBH', 'RAJKUMAR OMPRAKASH GHANIRA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(453, 'Indore', 'N0A691', 'ACTIVE', 'D - BM', 'BM', 'TEJENDRA DADORE', 'N0A691', 'TEJENDRA DADORE', 'N70104', 'AKHILESH KUMAR GUPTA', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(454, 'Bangalore', 'N0A415', 'ACTIVE', 'D - BDM', 'RM', 'PRATEEK KUMAR PRADHAN', 'Dummy', 'Dummy', 'N70010', 'Ratikant Raajbanshi', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(455, 'Vijaywada', 'N0A553', 'ACTIVE', 'D - SFE', 'RM', 'IKKURTI LAKSHMI SRAVANTHI', 'Dummy', 'Dummy', 'N7A015', 'JAKKAMPUDI VEERA BRAHMACHARI', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(456, 'Vijaywada', 'N0A707', 'ACTIVE', 'D - SFE', 'RM', 'JAGADEESH PILLI', 'Dummy', 'Dummy', 'N7A015', 'JAKKAMPUDI VEERA BRAHMACHARI', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(457, 'Chandigarh', 'N00304', 'ACTIVE', 'D - SBDM', 'RM', 'Paramveer Singh', 'Dummy', 'Dummy', 'N70024', 'Vishwa Mohan Yadav', 'Dummy', 'Dummy', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(458, 'Kolkata', 'N0A526', 'ACTIVE', 'D - SFE', 'RM', 'ABHIMENDRA KR TIWARI', 'N00400', 'Atanu Samadder', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(459, 'Ahmedabad', 'N00321', 'ACTIVE', 'D - SBM', 'BM', 'Akhil Goyal', 'N00321', 'Akhil Goyal', 'N70084', 'Ravi Kamleshbhai Thakkar', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(460, 'Kolkata', 'N01074', 'ACTIVE', 'D - SFE', 'RM', 'Prasanta Halder', 'N00023', 'Vikash Jaiswal', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(461, 'Mumbai', 'N00453', 'ACTIVE', 'D - DCRM', 'RM', 'Jaiswal Ravindra Suryanarayan', 'Dummy', 'Dummy', 'N70065', 'Atul Chandraprakash Tiwari', 'N80012', 'Nikhil Ji Shukla', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(462, 'Pune', 'N0A656', 'ACTIVE', 'D - BM', 'BM', 'DILIP SURYAKANT PATIL', 'N0A656', 'DILIP SURYAKANT PATIL', 'N7A025', 'VIVEK NARAYANRAO SHINDE', 'Dummy', 'Dummy', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(463, 'Pune', 'N0A349', 'ACTIVE', 'D - DFE', 'RM', 'ONKAR R JOSHI', 'Dummy', 'Dummy', 'N7A025', 'VIVEK NARAYANRAO SHINDE', 'Dummy', 'Dummy', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(464, 'Vadodara', 'N0A700', 'ACTIVE', 'D - AFE', 'RM', 'BHAVINKUMAR RAVAL', 'N01081', 'Harshit Pathak', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(465, 'Hyderabad', 'N90004', 'ACTIVE', 'D - RH', 'RH', 'Rama Vijaya Murali N', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(466, 'Raipur', 'N0A041', 'ACTIVE', 'D - AFE', 'RM', 'SHREYA TIWARI', 'N0A143', 'RIYA KHURANA', 'N70086', 'Abhishek Kumar', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(467, 'Hyderabad', 'N0A654', 'ACTIVE', 'D - DFE', 'RM', 'PALNATI SATHI RAJU', 'N0A509', 'SOOROJU RANJITH KUMAR', 'N70101', 'BALKI SHIVA RAJ', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(468, 'Kolkata', 'N0A320', 'ACTIVE', 'D - FE', 'RM', 'BISWAJIT SARKAR', 'N00022', 'Bablu Joardar', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(469, 'Bangalore', 'N00082', 'ACTIVE', 'D - CBM', 'BM', 'Sumantkumar .', 'N00082', 'Sumantkumar .', 'N70018', 'Anil Sharma', 'N80005', 'Dummy_cbh_ 3', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(470, 'Pune', 'N90011', 'ACTIVE', 'D - RH', 'RH', 'Mahesh Wahatule', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(471, 'Kolkata', 'N0A667', 'ACTIVE', 'D - FE', 'RM', 'SUBHASH CHANDRA GHOSH', 'N00197', 'Sreemanta Ghosh', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(472, 'Bhubaneshwar', 'N0A740', 'ACTIVE', 'D - DFE', 'RM', 'SIMANCHALA MISHRA', 'N0A751', 'SHYAMA SUNDAR JENA', 'N7A024', 'ALOK RANJAN JENA', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(473, 'Vadodara', 'N01591', 'ACTIVE', 'D - SRM', 'RM', 'HIRENKUMAR JAYANTIBHAI PAREKH', 'N01535', 'JAYENDRAKUMAR KIRITBHAI PATEL', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(474, 'Chandigarh', 'N0A388', 'ACTIVE', 'D - SFE', 'RM', 'SAROJ BAGGA', 'N0A141', 'ANAND NIGAM', 'N70024', 'Vishwa Mohan Yadav', 'Dummy', 'Dummy', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(475, 'Chandigarh', 'N01359', 'ACTIVE', 'D - SBDM', 'RM', 'MANJU MANJU', 'Dummy', 'Dummy', 'N70024', 'Vishwa Mohan Yadav', 'Dummy', 'Dummy', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(476, 'Kolkata', 'N00032', 'ACTIVE', 'D - SBDM', 'RM', 'Anirban Mahapatra', 'N00023', 'Vikash Jaiswal', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(477, 'Vadodara', 'N0A541', 'ACTIVE', 'D - DFE', 'RM', 'RAHULKUMAR NAGINBHAI PATEL', 'N01081', 'Harshit Pathak', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(478, 'Raipur', 'N0A756', 'ACTIVE', 'D - BDM', 'RM', 'CHANDAN PANDEY', 'N0A143', 'RIYA KHURANA', 'N70086', 'Abhishek Kumar', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(479, 'Cochin', 'N80014', 'ACTIVE', 'D - CBH', 'CBH', 'V Nagarajan', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(480, 'Mumbai', 'N0A298', 'ACTIVE', 'D - BM', 'BM', 'BHARAT RAMESH SHIVHARE', 'N0A298', 'BHARAT RAMESH SHIVHARE', 'N7A009', 'PRASHANT VASHISHTH MUNDHE', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(481, 'Vadodara', 'N00777', 'ACTIVE', 'D - BDM', 'RM', 'Nitin Wagh', 'N01535', 'JAYENDRAKUMAR KIRITBHAI PATEL', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(482, 'Surat', 'N0A009', 'ACTIVE', 'D - FE', 'RM', 'NAZIMUDDIN NURUDDIN SHAIKH', 'N00960', 'Shailendrasingh Gautam', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(483, 'Vadodara', 'N7A021', 'ACTIVE', 'D - ASH', 'ASH', 'SANDIPKUMAR SHAH', 'NA', 'NA', 'NA', 'NA', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(484, 'Ranchi', 'N70086', 'ACTIVE', 'D - ASH', 'ASH', 'Abhishek Kumar', 'NA', 'NA', 'NA', 'NA', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(485, 'Bhubaneshwar', 'N0A739', 'ACTIVE', 'D - SFE', 'RM', 'UTTAM KUMAR SENAPATI', 'N0A751', 'SHYAMA SUNDAR JENA', 'N7A024', 'ALOK RANJAN JENA', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(486, 'Ahmedabad', 'N0A787', 'ACTIVE', 'D - AFE', 'RM', 'VISHAL SARDHARA', 'N00370', 'Suresh Fulchandbhai Nagora', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(487, 'Bangalore', 'N0A544', 'ACTIVE', 'D - DFE', 'RM', 'SEEMA KISHAN', 'Dummy', 'Dummy', 'N70010', 'Ratikant Raajbanshi', 'N8A002', 'DUMMY CBH KARNATAKA', 'N9A001', 'DUMMY RH SOUTH', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(488, 'Kolkata', 'N0A672', 'ACTIVE', 'D - BDM', 'RM', 'VIKASH GOENKA', 'N00022', 'Bablu Joardar', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(489, 'Lucknow', 'N01803', 'ACTIVE', 'D - SBM', 'BM', 'DEEPESH SINGH', 'N01803', 'DEEPESH SINGH', 'N70049', 'Neeraj Shukla', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(490, 'Delhi', 'N0A360', 'ACTIVE', 'D - RM', 'RM', 'TUSHAR SAMSON', 'Dummy', 'Dummy', 'N70031', 'Manohar Singh', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(491, 'Mumbai', 'N0A608', 'ACTIVE', 'D - BM', 'BM', 'DARSHAN BHOLA GUPTA', 'N0A608', 'DARSHAN BHOLA GUPTA', 'N7A018', 'DIPESH KUMAR', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(492, 'Bhubaneshwar', 'N0A735', 'ACTIVE', 'D - SFE', 'RM', 'RUDHI SUNDAR JETHI', 'Dummy', 'Dummy', 'N7A024', 'ALOK RANJAN JENA', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(493, 'Hyderabad', 'N0A737', 'ACTIVE', 'D - BDM', 'RM', 'VALLEPOGU SEKHAR', 'Dummy', 'Dummy', 'N7A022', 'GIRIBABU NEELA', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(494, 'Ranchi', 'N0A702', 'ACTIVE', 'D - DFE', 'RM', 'SUMAN PRIYA', 'N01231', 'Jaspreet Kaur Chawla', 'N70086', 'Abhishek Kumar', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(495, 'Vadodara', 'N90009', 'ACTIVE', 'D - RH', 'RH', 'Keyur Anilbhai Patel', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(496, 'Kolkata', 'N0A653', 'ACTIVE', 'D - DFE', 'RM', 'SUMAN PAL', 'N00022', 'Bablu Joardar', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(497, 'Surat', 'N0A744', 'ACTIVE', 'D - DFE', 'RM', 'AJAY JAYSUKHBHAI THUMMAR', 'N00960', 'Shailendrasingh Gautam', 'Dummy', 'Dummy', 'N80029', 'Nehal J Sharma', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(498, 'Bangalore', 'N70080', 'ACTIVE', 'D - ASH', 'ASH', 'Deepak Kumar', 'NA', 'NA', 'NA', 'NA', 'N80036', 'K HARAPRASAD PATRO', 'N90005', 'Santanu Ghosh', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(499, 'Ahmedabad', 'N0A627', 'ACTIVE', 'D - DFE', 'RM', 'RAMESHVARKUMAR VIJAYKUMAR JANI', 'N00369', 'Shah Chintan Dilipbhai', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(500, 'Hyderabad', 'N0A753', 'ACTIVE', 'D - DFE', 'RM', 'MOHD KHAJA PASHA', 'N01885', 'GEETEY KASHINATH', 'N70101', 'BALKI SHIVA RAJ', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(501, 'Vijaywada', 'N0A364', 'ACTIVE', 'D - DFE', 'RM', 'MEDISETTI SOWJANYA', 'Dummy', 'Dummy', 'N7A015', 'JAKKAMPUDI VEERA BRAHMACHARI', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(502, 'Chandigarh', 'N00306', 'ACTIVE', 'D - DFE', 'RM', 'Jaswinder Singh', 'N01365', 'VIJAY KUMAR RANA', 'N70024', 'Vishwa Mohan Yadav', 'Dummy', 'Dummy', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(503, 'Aurangabad', 'N70043', 'ACTIVE', 'D - ASH', 'ASH', 'Lalita D Kolte', 'NA', 'NA', 'NA', 'NA', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(504, 'Lucknow', 'N0A765', 'ACTIVE', 'D - DFE', 'RM', 'SONAKSHI BHARTIYA', 'Dummy', 'Dummy', 'N7A023', 'TANVIKA BEG', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(505, 'Nagpur', 'N0A763', 'ACTIVE', 'D - BM', 'BM', 'VARIJ SANJEEV DUBEY', 'N0A763', 'VARIJ SANJEEV DUBEY', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(506, 'Kolkata', 'N00400', 'ACTIVE', 'D - BM', 'BM', 'Atanu Samadder', 'N00400', 'Atanu Samadder', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(507, 'Hyderabad', 'N00055', 'ACTIVE', 'D - RM', 'RM', 'Bolagani Kumar', 'N00006', 'Anjaneyulu Minukuri', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(508, 'Aurangabad', 'N00423', 'ACTIVE', 'D - SBM', 'BM', 'Atul V Athawale', 'N00423', 'Atul V Athawale', 'N70043', 'Lalita D Kolte', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(509, 'Hyderabad', 'N0A547', 'ACTIVE', 'D - BDM', 'RM', 'MERCY RAMDASS', 'N01147', 'M Rajkumar Goud', 'N7A006', 'IRENE SOMA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(510, 'Ahmedabad', 'N0A625', 'ACTIVE', 'D - BM', 'BM', 'ARPITA CHAND', 'N0A625', 'ARPITA CHAND', 'Dummy', 'Dummy', 'N80027', 'Biren Mukeshbhai Shah', 'N90009', 'Keyur Anilbhai Patel', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(511, 'Pune', 'N7A025', 'ACTIVE', 'D - ASH', 'ASH', 'VIVEK NARAYANRAO SHINDE', 'NA', 'NA', 'NA', 'NA', 'Dummy', 'Dummy', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(512, 'Kolkata', 'N00022', 'ACTIVE', 'D - CBM', 'BM', 'Bablu Joardar', 'N00022', 'Bablu Joardar', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(513, 'Bareilly', 'N01746', 'ACTIVE', 'D - SBM', 'BM', 'RAVI KUMAR SAXENA', 'N01746', 'RAVI KUMAR SAXENA', 'N70049', 'Neeraj Shukla', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(514, 'Chennai', 'N0A568', 'ACTIVE', 'D - BDM', 'RM', 'JOHNSATHISHKUMAR JOHNPHILIP', 'N00173', 'Mohan K', 'Dummy', 'Dummy', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(515, 'Raipur', 'N0A143', 'ACTIVE', 'D - BM', 'BM', 'RIYA KHURANA', 'N0A143', 'RIYA KHURANA', 'N70086', 'Abhishek Kumar', 'Dummy', 'Dummy', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(516, 'Mumbai', 'N0A566', 'ACTIVE', 'D - BDM', 'RM', 'ALI ASGER MURTUZA NADIR', 'Dummy', 'Dummy', 'N7A018', 'DIPESH KUMAR', 'N8A001', 'RAJKUMAR OMPRAKASH GHANIRA', 'N90013', 'Chandraprakash M Jain', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(517, 'Hyderabad', 'N0A530', 'ACTIVE', 'D - FE', 'RM', 'MARRI SURENDER YADAV', 'N0A509', 'SOOROJU RANJITH KUMAR', 'N70101', 'BALKI SHIVA RAJ', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(518, 'Chandigarh', 'N70024', 'ACTIVE', 'D - ASH', 'ASH', 'Vishwa Mohan Yadav', 'NA', 'NA', 'NA', 'NA', 'Dummy', 'Dummy', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(519, 'Visakhapatnam', 'N0A795', 'ACTIVE', 'D - AFE', 'RM', 'SHAIK ANWAR PASHA', 'N00137', 'H Manohara Rao', 'N7A015', 'JAKKAMPUDI VEERA BRAHMACHARI', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(520, 'Kolkata', 'N0A319', 'ACTIVE', 'D - DFE', 'RM', 'ESHANI SAHA', 'N00022', 'Bablu Joardar', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(521, 'Kolkata', 'N0A569', 'ACTIVE', 'D - SBDM', 'RM', 'AMAL KUMAR DAS', 'N00035', 'Soumitra Nath', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(522, 'Chennai', 'N0A770', 'ACTIVE', 'D - BDM', 'RM', 'SATHISH C', 'N00173', 'Mohan K', 'Dummy', 'Dummy', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(523, 'Hyderabad', 'N01229', 'ACTIVE', 'D - CBM', 'BM', 'Bujunuru Kranthi', 'N01229', 'Bujunuru Kranthi', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(524, 'Kolkata', 'N01457', 'ACTIVE', 'D - SFE', 'RM', 'RAJ KUMAR PRAJAPATI', 'N00022', 'Bablu Joardar', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(525, 'Delhi', 'N0A782', 'ACTIVE', 'D - BM', 'BM', 'AMIT GUPTA', 'N0A782', 'AMIT GUPTA', 'N7A004', 'NITIN KUMAR', 'N80025', 'Abir Dutta', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(526, 'Lucknow', 'N70049', 'ACTIVE', 'D - ASH', 'ASH', 'Neeraj Shukla', 'NA', 'NA', 'NA', 'NA', 'N80013', 'Anuj Dixit', 'N90015', 'Jatin Gupta', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(527, 'Thiruvananthapuram', 'N0A074', 'ACTIVE', 'D - DCRM', 'RM', 'ADHARSA RANI P', 'Dummy', 'Dummy', 'N7A005', 'JUSTIN ABRAHAM', 'N80014', 'V Nagarajan', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(528, 'Hyderabad', 'N01147', 'ACTIVE', 'D - SBM', 'BM', 'M Rajkumar Goud', 'N01147', 'M Rajkumar Goud', 'N7A006', 'IRENE SOMA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(529, 'Kolkata', 'N01722', 'ACTIVE', 'D - BDM', 'RM', 'SUBRA DEY', 'N00197', 'Sreemanta Ghosh', 'Dummy', 'Dummy', 'N80009', 'Sahana Basu', 'N90003', 'Madanmohan Upadhyay', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(530, 'Chennai', 'N0A725', 'ACTIVE', 'D - DFE', 'RM', 'BALAJI KANNAN', 'N00173', 'Mohan K', 'Dummy', 'Dummy', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(531, 'Hyderabad', 'N0A457', 'ACTIVE', 'D - DFE', 'RM', 'VALLURU SRI RAJYALAKSHMI', 'N01885', 'GEETEY KASHINATH', 'N70101', 'BALKI SHIVA RAJ', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(532, 'Nagpur', 'N0A669', 'ACTIVE', 'D - BM', 'BM', 'ASHOK SHRI KRUSHNA GABHANE', 'N0A669', 'ASHOK SHRI KRUSHNA GABHANE', 'N7A020', 'JITENDRA KUMAR BANSOD', 'N80008', 'Ashish Kumar Kapdi', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(533, 'Chennai', 'N00173', 'ACTIVE', 'D - CBM', 'BM', 'Mohan K', 'N00173', 'Mohan K', 'Dummy', 'Dummy', 'N80018', 'Devananth K', 'N90010', 'Sathick Ahmed U', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(534, 'Hyderabad', 'N0A098', 'ACTIVE', 'D - SRM', 'RM', 'G KRISHNA CHAITANYA', 'N01147', 'M Rajkumar Goud', 'N7A006', 'IRENE SOMA', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(535, 'Nashik', 'N00986', 'ACTIVE', 'D - BM', 'BM', 'Shailesh Ramdas Mahajan', 'N00986', 'Shailesh Ramdas Mahajan', 'N70043', 'Lalita D Kolte', 'N80026', 'Dummy_cbh_ 11', 'N90011', 'Mahesh Wahatule', 'N99007', 'Karun Soni', 'N99994', 'Sudharman S'),
(536, 'Hyderabad', 'N00006', 'ACTIVE', 'D - CBM', 'BM', 'Anjaneyulu Minukuri', 'N00006', 'Anjaneyulu Minukuri', 'Dummy', 'Dummy', 'Dummy', 'Dummy', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S'),
(537, 'Hyderabad', 'N0A361', 'ACTIVE', 'D - BM', 'BM', 'VIDYASAGAR BODDI REDDY', 'N0A361', 'VIDYASAGAR BODDI REDDY', 'N70029', 'Ravi Kumar Guthupala', 'N80010', 'Madhu Madire', 'N90004', 'Rama Vijaya Murali N', 'N99004', 'ZH Direct South East', 'N99994', 'Sudharman S');

-- --------------------------------------------------------

--
-- Table structure for table `lead`
--

CREATE TABLE `lead` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `mobile` varchar(100) NOT NULL DEFAULT '',
  `city` varchar(100) NOT NULL DEFAULT '',
  `income` varchar(100) NOT NULL DEFAULT '',
  `dob` varchar(30) NOT NULL DEFAULT '',
  `gender` varchar(20) NOT NULL DEFAULT '',
  `status` varchar(100) NOT NULL DEFAULT 'new',
  `source` varchar(100) NOT NULL DEFAULT '',
  `ip` varchar(100) NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead`
--

INSERT INTO `lead` (`id`, `name`, `email`, `mobile`, `city`, `income`, `dob`, `gender`, `status`, `source`, `ip`, `timestamp`) VALUES
(1, 'krishna sharma', 'admin@gmail.com', '9716435404', 'Gurgaon', '', '2020-12-02', 'Male', 'new', '', '', '2020-12-29 13:15:34'),
(2, 'Testing', 'krishna22@gmail.com', '9988877774', 'Delhi', '', '2018-10-30', 'Male', 'new', 'admin', '', '2020-12-30 10:24:59'),
(3, 'Vishal', 'vishal.5228@gmail.com', '7570080001', 'Delhi', '', '1999-03-04', 'Male', 'new', 'admin', '', '2021-01-09 05:26:14'),
(4, 'Vishal Sharma', 'vishal.5228@gmail.com', '9811290839', 'New Delhi', '', '1991-12-01', 'Male', 'new', 'admin', '', '2021-01-09 05:59:22'),
(5, 'ajay sharma', 'balkrishn2sharma@gmail.com', '9716435404', 'Gurgaon', '', '11992-01-01', 'Male', 'new', 'FI001', '150.242.67.222', '2021-01-10 06:08:49'),
(6, 'Abhishek Singh', 'aseem.singh@relianceada.com', '9616165990', 'Varanasi', '', '1986-01-10', 'Male', 'new', 'FI001', '123.136.251.124', '2021-01-10 11:23:44'),
(7, 'krishna sharma', 'admin@gmail.com', '9716435404', 'Gurgaon', '100000', '1998-02-11', '', 'new', 'FI001', '182.64.19.57', '2021-01-11 07:15:43'),
(8, 'Sangita Singh', 'abc@gmail.com', '8882955130', 'Delhi', '', '1983-12-03', 'Female', 'new', 'admin', '', '2021-01-11 11:46:11'),
(9, 'Saiyad Aslam', 'abc@gmail.com', '9880241954', 'banglore', '', '1991-12-09', 'Male', 'new', 'admin', '', '2021-01-11 12:06:22'),
(10, 'Himanshu', 'abc@gmail.com', '9451901268', 'mumbai', '', '1991-12-01', 'Male', 'new', 'admin', '', '2021-01-11 12:10:19'),
(11, 'Lokesh', 'abc@gmail.com', '9880307455', 'KA001', '', '1990-01-24', 'Male', 'new', 'admin', '', '2021-01-11 12:16:52'),
(12, 'Noor Alam', 'abc@gmail.com', '9163721313', 'WB001', '', '1986-07-25', 'Male', 'new', 'admin', '', '2021-01-11 12:28:24'),
(13, 'Korla Jayaram', '', '9290906042', 'AP004', '', '1978-07-24', 'Male', 'new', 'admin', '', '2021-01-12 05:27:12'),
(14, 'Prakash Narayan Tiwari', '', '9082375869', 'MH001', '', '1974-07-23', 'Male', 'new', 'admin', '', '2021-01-12 05:45:37'),
(15, 'Sachin Pratap Pawar', '', '9987933123', 'MH001', '', '1978-02-28', 'Male', 'new', 'admin', '', '2021-01-12 05:50:33'),
(16, 'Floyd McAtee', '', '7003172488', 'WB001', '', '1984-08-28', 'Male', 'new', 'admin', '', '2021-01-12 05:55:21'),
(17, 'Jitender Pratap Singh', '', '9913479246', 'GJ001', '', '1968-12-02', 'Male', 'new', 'admin', '', '2021-01-12 05:59:07'),
(18, 'Philips Bonta', '', '9920151252', 'MH001', '', '1975-09-22', 'Male', 'new', 'admin', '', '2021-01-12 06:04:30'),
(19, 'Raghunath Choudhary', '', '9922330694', 'MH004', '', '1982-05-17', 'Male', 'new', 'admin', '', '2021-01-12 09:59:05'),
(20, 'Prakash Sharma', '', '9703368804', 'DL001', '', '1986-12-02', 'Male', 'new', 'admin', '', '2021-01-13 08:07:12');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` varchar(7) NOT NULL DEFAULT '',
  `state_name` varchar(14) DEFAULT NULL,
  `zone` varchar(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `state_name`, `zone`) VALUES
('4', 'CHATTISGARH', 'E'),
('12', 'JHARKHAND', 'E'),
('26', 'ODISHA', 'E'),
('33', 'WEST BENGAL', 'E'),
('28', 'CHANDIGARH', 'N'),
('5', 'DELHI', 'N'),
('9', 'HARYANA', 'N'),
('27', 'PUNJAB', 'N'),
('31', 'UTTAR PRADESH', 'N'),
('32', 'UTTARAKHAND', 'N'),
('1', 'ANDHRA PRADESH', 'S'),
('13', 'KARNATAKA', 'S'),
('14', 'KERALA', 'S'),
('30', 'TAMIL NADU', 'S'),
('6', 'GUJARAT', 'W'),
('17', 'MADHYA PRADESH', 'W'),
('18', 'MAHARASHTRA', 'W');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `digital_card`
--
ALTER TABLE `digital_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `final_lead`
--
ALTER TABLE `final_lead`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hms`
--
ALTER TABLE `hms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead`
--
ALTER TABLE `lead`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `digital_card`
--
ALTER TABLE `digital_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `final_lead`
--
ALTER TABLE `final_lead`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `lead`
--
ALTER TABLE `lead`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
