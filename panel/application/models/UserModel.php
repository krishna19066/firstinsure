<?php

class UserModel extends CI_Model
{

    function __construct()
    {
        $this->proTable = 'products';
        $this->custTable = 'users';
        $this->ordTable = 'orders';
        $this->ordItemsTable = 'order_items';
    }

    function CheckUser($email, $password)
    {
        //$where = "select * from admin where username= '$username' AND password= '$password'";
        $this->db->select("*");
        $this->db->from('admin');
        $this->db->where(array('email' => $email, 'password' => $password));
        //$this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result();
    }

    function CheckAdmin($email, $password)
    {
        //$where = "select * from admin where username= '$username' AND password= '$password'";
        $this->db->select("*");
        $this->db->from('admin');
        $this->db->where(array('email' => $email, 'password' => $password));
        //$this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result();
    }

    function CheckGuest($email, $password)
    {
        $this->db->select("*");
        $this->db->from('users');
        $this->db->where(array('email' => $email, 'password' => $password));
        //$this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result();
    }

    function getPropertyListing($agent_id)
    {
        $where = "agent_id = $agent_id";
        $this->db->select("*");
        $this->db->from('property');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
    function getAgentInfo($agent_id)
    {
        $where = "id = $agent_id";
        $this->db->select("*");
        $this->db->from('agent');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
    function getPropertyListingWithURL($url)
    {
        $where = "propertyURL = '$url'";
        $this->db->select("*");
        $this->db->from('property');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
    function getAllProductsWithCategory($category){
        $where = "property_type = '$category' AND status='Approved' AND agent_status !='Inactive' ";
        $this->db->select("*");
        $this->db->from('property');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
    function getAllProductsWithCategoryAndLocation($category,$location){
        $where = "country = '$location' and property_type = '$category' AND status='Approved' AND agent_status !='Inactive' ";
        $this->db->select("*");
        $this->db->from('property');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function getPropertyImg($propID)
    {
        $where = "propertyID = $propID";
        $this->db->select("*");
        $this->db->from('images');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        //$this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
     function getPropertyImgF($propID)
    {
        $where = "propertyID = $propID and propertyF ='Y'";
        $this->db->select("*");
        $this->db->from('images');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        //$this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function getAllPropertyImg($propID)
    {
        $where = "propertyID = $propID";
        $this->db->select("*");
        $this->db->from('images');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        //$this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function getPropertyIMGALL($property_id)
    {
        $where = "propertyID = $property_id and type ='property'";
        $this->db->select("*");
        $this->db->from('images');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
    function getRoomIMG($room_id)
    {
        $where = "roomID = $room_id and type ='room'";
        $this->db->select("*");
        $this->db->from('images');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function getPropertyAllImages($agent_id,$property_id)
    {
        $where = " propertyID = $property_id and agent_id = $agent_id and type ='property'";
        $this->db->select("*");
        $this->db->from('images');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function getRoomAllImages($property_id, $agent_id,$room_id)
    {
        $where = "propertyID = $property_id and agent_id = $agent_id and roomID = $room_id and type ='room'";
        $this->db->select("*");
        $this->db->from('images');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
    function getRoomImagesWithID($room_id)
    {
        $where = "roomID = $room_id and type ='room'";
        $this->db->select("*");
        $this->db->from('images');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function getAllRoomData($agent_id)
    {
        $where = "agent_id = $agent_id";
        $this->db->select("*");
        $this->db->from('rooms');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function getAllProducts($category)
    {
        $this->db->select("*");
        $this->db->from('products');
        $this->db->where('category', $category);
        //$this->db->limit(10, 0);	
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function getproductWithURL($url_slug)
    {
        $this->db->select("*");
        $this->db->from('products');
        $this->db->where('url', $url_slug);
        // $this->db->limit(10, 0);	
        //	$this->db->order_by("id", "desc");		
        $query = $this->db->get();
        return $query->result();
    }

    function getCategoryWithURL($url_slug)
    {
        $this->db->select("*");
        $this->db->from('category');
        $this->db->where(array('url' => $url_slug));
        // $this->db->limit(10, 0);	
        //	$this->db->order_by("id", "desc");		
        $query = $this->db->get();
        return $query->result();
    }

    function getAllData($table)
    {
        $this->db->select("*");
        $this->db->from($table);
        //$this->db->where('category', $category);
        //$this->db->limit(10, 0);	
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
    function getAllDataWithID($table, $id)
    {
        $this->db->select("*");
        $this->db->from($table);
        $this->db->where('id', $id);
        //$this->db->limit(10, 0);	
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
    function getUserReview($table, $user_id)
    {
        $this->db->select("*");
        $this->db->from($table);
        $this->db->where('user_id', $user_id);
        //$this->db->limit(10, 0);	
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    //login API
    function getAllListing($location, $type)
    {
        $where = "city='$location' AND property_type='$type' AND status='Approved' AND agent_status !='Inactive'";
        $this->db->select('*');
        $this->db->where($where);
        $q = $this->db->get("property");
        //    if($q->num_rows() > 0) {
        //        return $q->result();
        //    }
        return $q->result();
    }

    function getPropertyCurrentLocation($current_country){
        $where = "country='$current_country' AND status='Approved' AND agent_status !='Inactive'";
        $this->db->select('*');
        $this->db->where($where);
        $q = $this->db->get("property");
        //    if($q->num_rows() > 0) {
        //        return $q->result();
        //    }
        return $q->result();
    }

    function getpropertyWithID($property_id)
    {
        $where = "id = $property_id";
        $this->db->select("*");
        $this->db->from('property');
        //$this->db->limit(10, 0);	
        //$this->db->order_by("id", "desc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    function getRoomDataWithProID($propertyID)
    {
        $where = "propertyID = $propertyID";
        $this->db->select("*");
        $this->db->from('rooms');
        //$this->db->limit(10, 0);	
        //$this->db->order_by("id", "desc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    function getAllRoomDataWithPropertyID($agent_id, $property_id)
    {
        $where = "propertyID = $property_id and agent_id= $agent_id";
        $this->db->select("*");
        $this->db->from('rooms');
        //$this->db->limit(10, 0);	
        //$this->db->order_by("id", "desc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    function getRoomDataPrice($property_id)
    {
        $where = "propertyID = $property_id";
        $this->db->select("*");
        $this->db->from('rooms');
        //$this->db->limit(10, 0);	
        $this->db->order_by("roomPrice", "desc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    function updateFcmToken($token, $user_id)
    {
        //Make an array to update to the db : FCM token
        $updateToken = array(
            "push_token" => $token
        );
        $where = "user_token = $user_id";
        $this->db->where($where);
        $this->db->update("users", $updateToken);
    }

    function getblogWithURL($url_slug)
    {
        $where = "url_slug = '$url_slug'";
        $this->db->select("*");
        $this->db->from('blog');
        //$this->db->limit(10, 0);	
        //$this->db->order_by("id", "desc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    function getUserWithID($user_id)
    {
        $where = "id = $user_id";
        $this->db->select("*");
        $this->db->from("users");
        //$this->db->group_by("bookings");
        $this->db->where($where);
        // $this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result();
    }

    function getAllBookingData($agent_id)
    {
        $where = "agent_id = $agent_id";
        $this->db->select("*");
        $this->db->from("bookings");
        //$this->db->group_by("bookings");
        $this->db->where($where);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result();
    }

    function getAllBookingDataForUsser($user_id)
    {
        $where = "user_id = $user_id";
        $this->db->select("*");
        $this->db->from("bookings");
        //$this->db->group_by("bookings");
        $this->db->where($where);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result();
    }

    function getRoomDataWithRoomID($roomId)
    {
        $where = "id = $roomId and status='Y'";
        $this->db->select("*");
        $this->db->from('rooms');
        //$this->db->limit(10, 0);	
        //$this->db->order_by("id", "desc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    function checkUserEmail($guest_email)
    {
        $where = "email = '$guest_email'";
        $this->db->select("*");
        $this->db->from('users');
        //$this->db->limit(10, 0);	
        //$this->db->order_by("id", "desc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    function getBookingData($booking_id)
    {
        $where = "id = $booking_id";
        $this->db->select("*");
        $this->db->from('bookings');
        //$this->db->limit(10, 0);	
        //$this->db->order_by("id", "desc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    function getAllListingWithPropertyType($type)
    {
        $where = "property_type='$type' AND status='Approved' and agent_status !='Inactive'";
        $this->db->select('*');
        $this->db->where($where);
        $q = $this->db->get("property");
        //    if($q->num_rows() > 0) {
        //        return $q->result();
        //    }
        return $q->result();
    }
    function getAllActiveListing()
    {
        $where = "status='Approved' and agent_status !='Inactive'";
        $this->db->select('*');
        $this->db->where($where);
        $q = $this->db->get("property");
        //    if($q->num_rows() > 0) {
        //        return $q->result();
        //    }
        return $q->result();
    }

    function getRoomDataWithOnlyActive($propertyID)
    {
        $where = "propertyID = $propertyID and status = 'Y'";
        $this->db->select("*");
        $this->db->from('rooms');
        //$this->db->limit(10, 0);	
        //$this->db->order_by("id", "desc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    function getAllListingWithActive($agent_id)
    {
        $where = "agent_status ='Active' and agent_id = $agent_id";
        $this->db->select('*');
        $this->db->where($where);
        $q = $this->db->get("property");
        //    if($q->num_rows() > 0) {
        //        return $q->result();
        //    }
        return $q->result();
    }

    function getAllListingWithInactive($agent_id)
    {
        $where = "agent_status ='Inactive' and agent_id = $agent_id";
        $this->db->select('*');
        $this->db->where($where);
        $q = $this->db->get("property");
        //    if($q->num_rows() > 0) {
        //        return $q->result();
        //    }
        return $q->result();
    }
    
     function CheckAgentWithEmailID($email)
    {
        $where = "email = '$email'";
        $this->db->select('*');
        $this->db->where($where);
        $q = $this->db->get("agent");
        //    if($q->num_rows() > 0) {
        //        return $q->result();
        //    }
        return $q->result();
    }
     function CheckUserWithEmailID($email)
    {
        $where = "email = '$email'";
        $this->db->select('*');
        $this->db->where($where);
        $q = $this->db->get("users");
        //    if($q->num_rows() > 0) {
        //        return $q->result();
        //    }
        return $q->result();
    }
}
