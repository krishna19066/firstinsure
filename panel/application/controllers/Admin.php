<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
        // sets up the session
        $this->load->library('form_validation');            // Loading form validation library
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->library('pagination');
        //header('Access-Control-Allow-Origin : http://localhost:3000');   
    }

    function index() {
        $this->load->view('admin_login');
    }

    function AdminLogin() {
        $this->form_validation->set_rules("email", "email", "trim|required");
        $this->form_validation->set_rules("password", "password", "trim|required");
        if ($this->form_validation->run() == false) {
            redirect('admin');
        } else {

            $email = $this->input->post("email");
            $password = $this->input->post("password");
            $getUser = $this->UserModel->CheckAdmin($email, $password);
            //print_r($getUser);
            // redirect('user_login/dashboard');
            if (!$getUser) {
                // login failed errror;
                $this->session->set_flashdata('login_error', TRUE);
                $data = array(
                    'error_message' => 'Invalid Email or Password'
                );
                $this->load->view('admin_login', $data);
                //redirect('Login');
            } else {
                // login ;
                $this->session->set_userdata(array(
                    'logged_in' => TRUE,
                    'password' => $password,
                    'email' => $email));
                $u_rec_id = $this->session->userdata('username');
                redirect('dashboard/admindashboard');
            }
        }
    }

}
