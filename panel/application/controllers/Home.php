<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        // sets up the session
        $this->load->library('form_validation');            // Loading form validation library
        $this->load->helper(array('form', 'url'));
        $this->load->library('email');
        $this->load->library('cart');
        $this->load->library('session');
        //header('Access-Control-Allow-Origin : http://localhost:3000'); 
        if ($this->session->userdata('agent_id')) {
            $this->session->userdata('agent_id');
        }
    }

    public function index()
    {
       /* $table = "home_page";
        $table2 = "property";
        $table3 = "testimonial";
        $data['home_page_data'] = $this->UserModel->getAllData($table);
        $data['property_data'] = $this->UserModel->getAllActiveListing();
        $data['testimonial_data'] = $this->UserModel->getAllData($table3);
        $type_pg = 'Paying Guest';
        $type_hostel = 'Hostel';
        $pg_count1 = $this->UserModel->getAllListingWithPropertyType($type_pg);
        $hostel_count1 = $this->UserModel->getAllListingWithPropertyType($type_hostel);
        $data["pg_count"] = count($pg_count1);
        $data["hostel_count"] = count($hostel_count1);
        //print_r($data);
        //die;
        //session_destroy();
        */
         $this->load->view('property_deler_login');
    }

    function PropertyListing()
    {
        $table2 = "property";
        $data['property_data'] = $this->UserModel->getAllData($table2);
        $this->load->view('listing_page', $data);
    }
    function listing_details($url){
        $data['property_data'] = $this->UserModel->getPropertyListingWithURL($url);
         $this->load->view('listing_details',$data);
    }
    
    function listing_page(){
         $this->load->view('listing_page');
    }

    function AgentRegister()
    {
        $this->load->view('agent_register_page');
    }

    function AgentInsertData()
    {
        $submit = $this->input->post('submit');
        if (isset($submit)) {
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $mobile = $this->input->post('mobile');
            $password = $this->input->post('password');
            $country = $this->input->post('country');
            $data = array(
                'name' => $name,
                'email' => $email,
                'mobile' => $mobile,
                'password' => $password,
                'country' => $country,
            );
            //print_r($data);
            $this->db->insert('agent', $data);
            redirect('home/success');
        }
    }
    
    function UserRegister(){
         $this->load->view('user_register_page');
    }
    
    function UserDataInsert(){
        $submit = $this->input->post('submit');
        if (isset($submit)) {
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $mobile = $this->input->post('mobile');
            $password = $this->input->post('password');
            $data = array(
                'name' => $name,
                'email' => $email,
                'mobile' => $mobile,
                'password' => $password,
            );
           // print_r($data);
           // die;
            $this->db->insert('users', $data);
            $user_id = $this->db->insert_id();
            $this->session->set_userdata(array(
                    'logged_in' => TRUE,
                    'username' => $name,
                    'user_id' => $user_id,
                    'email' => $email));
                $u_rec_id = $this->session->userdata('username');
                redirect('Users');
        }
    }

    function success()
    {
        $this->load->view('success');
    }

    function AgentLogin()
    {
        $this->load->view('property_deler_login');
    }

    function UserLogin()
    {
        $this->load->view('user_login');
    }

    function AboutUs()
    {
        $this->load->view('aboutus_page');
    }

    function ContactUs()
    {
        $this->load->view('contactus_page');
    }
    function franchise(){
        $this->load->view('franchise_page'); 
    }

    function SubmitInquiry()
    {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('phone');
        $message = $this->input->post('message');
        $this->SendMail($name, $email, $mobile, $message);
        $data = array(
            'name' => $name,
            'email' => $email,
            'mobile' => $mobile,
            'message' => $message,
        );
        // print_r($data);
        $this->db->insert('inquiry', $data);
        echo '<script type="text/javascript">
               alert("Thank You! Your Request has been successfuly submited");              
            </script>';
        redirect('home');
    }

    function check_availability()
    {
        $propertyLink = $this->input->post('propertyLink');
        echo '<script type="text/javascript" language="Javascript">window.location.href="' . $propertyLink . '";</script>';
        //redirect($propertyLink);
    }

    function SearchByCategory(){
        $category = $this->input->post('category');
        $data['get_property_data'] =   $this->UserModel->getAllProductsWithCategory($category);
        //print_r($data);
        $this->load->view('ajax_category_search', $data);
    }

    function Search()
    {
         $location = $this->input->get('location');
         $category = $this->input->get('category');
         $keyword = $this->input->get('keyword');
         $data['location'] = $location;
         $data['category'] = $category;
         $data['keyword'] = $keyword;
         $data['get_property_data'] = $this->UserModel->getAllProductsWithCategoryAndLocation($category,$location);
        //print_r( $data['results']);
        //die;
        $this->load->view('listing_page', $data);
    }

    function PG_list()
    {
        $location = 'all';
        $type = 'Paying Guest';
        $search_data = array(
            'city' => $location,
            'property_type' => $type,
            'guest' => 1,
            'search_date' => date('m/d/yy'),
            'months' => '1'
        );
        $this->session->set_userdata($search_data);
        $session_id = $this->session->userdata('__ci_last_regenerate');
        $data['search_data'] = $search_data;
        //print_r($search_data);
        //die;
        $data['listing'] = $this->UserModel->getAllListingWithPropertyType($type);
        //$data['search_data'] = $search_data;
        //$this->load->view('listing_page', $data);
        //print_r( $data['listing']);
        // die;
        redirect('home/search_listing');
    }

    function Hostel_list()
    {
        $location = 'all';
        $type = 'Hostel';
        $search_data = array(
            'city' => $location,
            'property_type' => $type,
            'guest' => 1,
            'search_date' => date('m/d/yy'),
            'checkOut' => date('m/d/yy'),
            'months' => '1'
        );
        $this->session->set_userdata($search_data);
        $session_id = $this->session->userdata('__ci_last_regenerate');
        $data['search_data'] = $search_data;
        //print_r($search_data);
        //die;
        $data['listing'] = $this->UserModel->getAllListingWithPropertyType($type);
        //$data['search_data'] = $search_data;
        //$this->load->view('listing_page', $data);
        //print_r( $data['listing']);
        // die;
        redirect('home/search_listing');
    }

    function listing()
    {
        $location = $this->input->get('location');
        $checkIn = $this->input->get('checkIn');
        $checkOut = $this->input->get('checkOut');
        $months = $this->input->get('months');
        $guest = $this->input->get('guest');
        $type = $this->input->get('type');
        $search_data = array(
            'city' => $location,
            'property_type' => $type,
            'guest' => $guest,
            'search_date' => $checkIn,
            'checkOut' => $checkOut,
            'months' => $months
        );
        $this->session->set_userdata($search_data);
        $session_id = $this->session->userdata('__ci_last_regenerate');
        $data['search_data'] = $search_data;
        //print_r($search_data);
        //die;
        $data['listing'] = $this->UserModel->getAllListing($location, $type);
        //$data['search_data'] = $search_data;
        //$this->load->view('listing_page', $data);
        redirect('home/search_listing');
    }

    function search_listing()
    {
        $search_data = $this->session->all_userdata();
        $location = $search_data['city'];
        $type = $search_data['property_type'];
        if ($location == 'all') {
            $data['listing'] = $this->UserModel->getAllListingWithPropertyType($type);
        } else {
            $data['listing'] = $this->UserModel->getAllListing($location, $type);
        }
        //echo "<pre>";
        // print_r($search_data);
        //echo "</pre>";
        // $data['listing'] = $this->UserModel->getAllListing($location, $type);
        $data["total_listing"] = count($data['listing']);
        //$data['search_data'] = $search_data;
        $this->load->view('listing_page', $data);
    }

    function property_details($property_id)
    {

        $sess_city = $this->session->userdata('city');
        //echo "<pre>";
        //print_r($this->session->all_userdata());
        //echo "</pre>";

        $sess_city = $this->session->userdata('city');
        $sess_type = $this->session->userdata('property_type');
        $location = $this->input->post('location');
        $data['results'] = $this->UserModel->getpropertyWithID($property_id);
        $this->load->view('property_details', $data);
    }

    function GuestBooking()
    {
        $sess_city = $this->session->userdata('city');
        $sess_type = $this->session->userdata('property_type');
        $checkIn = $this->input->post('checkIn');
        $checkOut = $this->input->post('checkOut');
        $months = $this->input->post('months');
        $guest = $this->input->post('guest');
        //$roomID = $this->input->post('room');
        $tot_amount = $this->input->post('tot_amount');
        $paid_amount = $this->input->post('paid_amount');
        $property_id = $this->input->post('property_id');
        $guest_name = $this->input->post('guest_name');
        $no_of_bed = $this->input->post('no_of_bed');
        $guest_email = $this->input->post('guest_email');
        $nationality = $this->input->post('nationality');
        $gender = $this->input->post('gender');
        $select_room = $this->input->post('select_room');
        if ($select_room == '') {
            $this->session->set_flashdata('error', ' Ohh!!! Select atleat one room');
            $data['results'] = $this->UserModel->getpropertyWithID($property_id);
            // $this->load->view('property_details', $data);
            redirect('home/property_details/' . $property_id);
        }
        $roomID = implode(',', $select_room);
        //print_r($select_room);
        $searchata['city'] = $sess_city;
        $searchata['checkIn'] = $checkIn;
        $searchata['checkOut'] = $checkOut;
        $searchata['type'] = $sess_type;
        $searchata['months'] = $months;
        $searchata['guest'] = $guest;
        $searchata['bed'] = $no_of_bed;
        $searchata['tot_amount'] = $tot_amount;
        $searchata['paid_amount'] = $paid_amount;
        $search_data = serialize($searchata);
        //print_r($search_data);

        $search_data_upd = array(
            'city' => $sess_city,
            'property_type' => $sess_type,
            'guest' => $guest,
            'search_date' => $checkIn,
            'checkOut' => $checkOut,
            'months' => $months
        );
        $this->session->set_userdata($search_data_upd);
        $session_id = $this->session->userdata('__ci_last_regenerate');
        if (isset($property_id)) {
            $booking_data = $this->UserModel->getpropertyWithID($property_id);
            foreach ($booking_data as $prop) {
                $agent_id = $prop->agent_id;
            }
            $data['results'] = $this->UserModel->getpropertyWithID($property_id);
            $user_data = array(
                'name' => $guest_name,
                'email' => $guest_email,
                'nationality' => $nationality,
                'gender' => ''
            );
            $checkUser = $this->UserModel->checkUserEmail($guest_email);
            if ($checkUser) {
                foreach ($checkUser as $dt) {
                    $user_id = $dt->id;
                }
            } else {
                $this->db->insert('users', $user_data);
                $user_id = $this->db->insert_id();
            }

            $booking_data = array(
                'room_id' => $roomID,
                'property_id' => $property_id,
                'agent_id' => $agent_id,
                'user_id' => $user_id,
                'search_data' => $search_data,
                'status' => 'Pending'
            );
            $this->db->insert('bookings', $booking_data);
            $order_id = $this->db->insert_id();
            $data["booking_data"] = $booking_data;
            $data['user_id'] = $user_id;
            $data['order_id'] = $order_id;
            $data['email'] = $guest_email;
            $data['roomIDs'] = $select_room;
            $data['no_of_bed'] = $no_of_bed;
            //print_r($booking_data);
            $this->load->view('booking_details', $data);
        } else {
            redirect('home');
        }
    }

    function ConfirmBooking()
    {
        $sess_city = $this->session->userdata('city');
        $sess_type = $this->session->userdata('property_type');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $password = $this->input->post('password');
        $order_id = $this->input->post('order_id');
        $user_id = $this->input->post('user_id');
        $property_id = $this->input->post('property_id');
        $roomID = $this->input->post('room_id');
        $order_id = $this->input->post('order_id');
        $total_amt = $this->input->post('total_amt');
        $pay_amt = $this->input->post('pay_amt');
        $new_user = $this->input->post('new_user');
        $login = $this->input->post('login');

        if (isset($new_user)) {
            $user_data = array(
                'mobile' => $mobile,
                'email' => $email,
                'password' => $password
            );
            $this->db->where('id', $user_id);
            $this->db->update('users', $user_data);
            $userdata = $this->UserModel->getUserWithID($user_id);
            foreach ($userdata as $user) {
                $username = $user->name;
                $email = $user->email;
            }
            $user_data = array(
                'user_id' => $user_id,
                'email' => $email,
                'username' => $username,
                'property_id' => $property_id,
                'pay_amt' => $pay_amt,
                'total_amt' => $total_amt,
                'order_id' => $order_id
            );
            $this->session->set_userdata($user_data);
            $session_id = $this->session->userdata('__ci_last_regenerate');
            $this->session->set_flashdata('success', 'Successful Booked Property! Thank You');
            //$this->load->view('booking_confirm', $data);
            redirect('home/Payment');
        }
        if (isset($login)) {
            $getUser = $this->UserModel->CheckGuest($email, $password);
            $data['results'] = $this->UserModel->getpropertyWithID($property_id);
            foreach ($getUser as $user) {
                $user_id = $user->id;
                $username = $user->name;
            }
            if (!$getUser) {
                echo "<html><body><script>alert(Invalid Email or Password);</script></body></html>";
                // login failed errror;
                /* $this->session->set_flashdata('login_error', TRUE);
                  $data = array(
                  'error_message' => 'Invalid Email or Password'
                  );
                  $data['user_id'] = $user_id;
                  $data['order_id'] = $order_id;
                  $data['email'] = $email;
                  $data['roomID'] = $roomID;
                  $this->load->view('booking_details', $data);
                  //redirect('Login');
                 * /
                 */
                //$this->load->view('booking_confirm');
            } else {

                $user_data = array(
                    'user_id' => $user_id,
                    'email' => $email,
                    'username' => $username,
                    'property_id' => $property_id,
                    'pay_amt' => $pay_amt,
                    'total_amt' => $total_amt,
                    'order_id' => $order_id
                );
                $this->session->set_userdata($user_data);
                $session_id = $this->session->userdata('__ci_last_regenerate');
                $this->session->set_flashdata('success', 'Successful Booked Property! Thank You');

                //print_r($booking_data);
                redirect('home/Payment');
                //$this->load->view('booking_confirm', $data);
            }
        }
    }

    function Payment()
    {
        $total_amt = $this->session->userdata('total_amt');
        $pay_amt1 = $this->session->userdata('pay_amt');
        $order_id = $this->session->userdata('order_id');
        $booking_data = array('amount' => $total_amt, 'pay_amt' => $pay_amt1, 'status' => 'Payment Not Done');
        $this->db->where('id', $order_id);
        $this->db->update('bookings', $booking_data);
        $this->load->view('booking_confirm');
    }
    
     function ForgotPassword()
    {
        $this->load->view('forgot_password');
    }
    function sendForgotPassword()
    {
        $email = $this->input->post('email');
        $checkEmail = $this->UserModel->CheckAgentWithEmailID($email);
        if ($checkEmail) {
            foreach($checkEmail as $dt){
                $password = $dt->password;
            }
            $this->session->set_flashdata('success', 'Successful sent password your email id');
           
            $url = 'https://api.sendgrid.com/';
            $user = 'no-reply@payingguestworld.com';
            $pass = 'PGW123456wolrd';
            $msg = 'Your password is:'. $password ;
            $params = array(
                'api_user' => $user,
                'api_key' => $pass,
                'to' => $email,
                'subject' => "Forgot Password - Paying Guest Wolrd",
                'html' => $msg,
                'text' => 'Paying Guest Wolrd',
                'from' => 'no-reply@payingguestworld.com'
                // 'files['.$fileName.']' => '@'.$filePath.'/'.$fileName
                //  'files' => $fileName
            );
            //print_r($params);
    
            $request = $url . 'api/mail.send.json';
            $session = curl_init($request);
            curl_setopt($session, CURLOPT_POST, true);
            curl_setopt($session, CURLOPT_POSTFIELDS, $params);
            curl_setopt($session, CURLOPT_HEADER, false);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            $response1 = curl_exec($session);
            //print_r($response1);
            curl_close($session);
        } else {
            $this->session->set_flashdata('error', 'Email id does not exist');
           
        }
         $this->load->view('forgot_password');
    }
    
     function UserForgotPassword()
    {
        $this->load->view('user_forgot_password');
    }

    function UsersendForgotPassword()
    {
        $email = $this->input->post('email');
        $checkEmail = $this->UserModel->CheckUserWithEmailID($email);
        if ($checkEmail) {
            foreach ($checkEmail as $dt) {
                $password = $dt->password;
            }
            $this->session->set_flashdata('success', 'Successful sent password your email id');
           
            $url = 'https://api.sendgrid.com/';
            $user = 'no-reply@payingguestworld.com';
            $pass = 'PGW123456wolrd';
            $msg = 'Your password is:' . $password;
            $params = array(
                'api_user' => $user,
                'api_key' => $pass,
                'to' => $email,
                'subject' => "Forgot Password - Paying Guest Wolrd",
                'html' => $msg,
                'text' => 'Paying Guest Wolrd',
                'from' => 'no-reply@payingguestworld.com'
                // 'files['.$fileName.']' => '@'.$filePath.'/'.$fileName
                //  'files' => $fileName
            );
            //print_r($params);

            $request = $url . 'api/mail.send.json';
            $session = curl_init($request);
            curl_setopt($session, CURLOPT_POST, true);
            curl_setopt($session, CURLOPT_POSTFIELDS, $params);
            curl_setopt($session, CURLOPT_HEADER, false);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            $response1 = curl_exec($session);
            //print_r($response1);
            curl_close($session);
        } else {
            $this->session->set_flashdata('error', 'Email id does not exist');
           
        }
        $this->load->view('user_forgot_password');
    }

    function MailTest()
    {
        $this->load->view('agent_order_mail');
    }

    function SendMail($name, $email, $mobile, $message)
    {
        $guest_name = $name;
        $property_name = 'Dream stay';
        $page = 'Contact US';
        $guest_mail = $email;
        $phone = $mobile;
        $hotel_name = 'Dream Stay';
        $to_email = 'airbnbofmalik@gmail.com';
        $from_email = 'support@bkpinfo.in';
        $subject = "You Have Recieved New Inquire From" . ' ' . $guest_name;
        $mess = "Regarding :- " . $property_name . " - From " . $page . " <br /><br /> Name :- " . $guest_name . "<br /><br /> Email : " . $guest_mail . "<br /><br /> Mobile : " . $phone . "<br /><br />Message : " . $message . "<br /><br />";
        $to = $to_email;
        $cc = '';
        $from = $from_email;
        $url = 'https://api.sendgrid.com/';
        $user = 'balkrishn2sharma@gmail.com';
        $pass = '0108it101010';
        $json_string = array('to' => array($to), 'category' => 'inquiry');
        $params = array(
            'api_user' => 'balkrishn2sharma@gmail.com',
            'api_key' => '0108it101010',
            'x-smtpapi' => json_encode($json_string),
            'to' => $to,
            'subject' => $subject,
            'html' => $mess,
            'replyto' => $to,
            'cc' => $cc,
            'fromname' => $hotel_name,
            'text' => 'testing body',
            'from' => $from,
        );
        $request = $url . 'api/mail.send.json';
        $session = curl_init($request);
        curl_setopt($session, CURLOPT_POST, true);
        curl_setopt($session, CURLOPT_POSTFIELDS, $params);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($session);
        curl_close($session);
        // print_r($response);
    }

    function InsertInquiry()
    {
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $property_name = $this->input->post('property_name');
        $property_url = $this->input->post('property_url');
        $name = $this->input->post('name');
        $date = $this->input->post('date');
        $time = $this->input->post('time');
        $adult = $this->input->post('quant[1]');
        $children = $this->input->post('quant[2]');
        $booking_date = $date.'-'.$time;
        $data = array(
            'name' => $name,
            'email' => $email,
            'mobile' => $mobile,
            'adult' => $adult,
            'children' => $children,
            'listing_name' => $property_name,
            'booking_date'=>$booking_date
        );
       // print_r($data);
        //die;
        $this->session->set_flashdata('success', 'Successfuly Booked...');
        $this->db->insert('inquiry', $data);
        redirect('home/listing_details/'.$property_url);
    }

    function Payment_confirm()
    {
        $postdata = $_POST;
        $msg = '';
        if (isset($postdata['key'])) {
            $key = $postdata['key'];
            $salt = 'cjK1T61Gbf';
            $txnid = $postdata['txnid'];
            $amount = $postdata['amount'];
            $productInfo = $postdata['productinfo'];
            $firstname = $postdata['firstname'];
            $email = $postdata['email'];
            $udf5 = $postdata['udf5'];
            $mihpayid = $postdata['mihpayid'];
            $status = $postdata['status'];
            $resphash = $postdata['hash'];
            //Calculate response hash to verify	
            $keyString = $key . '|' . $txnid . '|' . $amount . '|' . $productInfo . '|' . $firstname . '|' . $email . '|||||' . $udf5 . '|||||';
            $keyArray = explode("|", $keyString);
            $reverseKeyArray = array_reverse($keyArray);
            $reverseKeyString = implode("|", $reverseKeyArray);
            $CalcHashString = strtolower(hash('sha512', $salt . '|' . $status . '|' . $reverseKeyString));
            $order_id = $udf5;
            $booking_data =  $this->UserModel->getBookingData($order_id);
            foreach ($booking_data as $dt) {
                $property_id = $dt->property_id;
            }
            $property_data =  $this->UserModel->getpropertyWithID($property_id);
            foreach ($property_data as $pt) {
                $to_email_agent = $pt->booking_email;
            }
            if ($status == 'success' && $resphash == $CalcHashString) {

                //$msg = "Transaction Successful and Hash Verified...";
                $this->session->set_flashdata('success', 'Successful Booked Property! Thank You');
                $booking_data = array('status' => 'Payment Confirmed');
                $this->db->where('id', $order_id);
                $this->db->update('bookings', $booking_data);
                $booking_id = $order_id;
                $to_email = $email;
                $property_name = $productInfo;
                $this->sendMailFileForUser($property_name, $to_email, $booking_id);
                $this->sendMailFileForAgent($property_name, $to_email_agent, $booking_id);
                //Do success order processing here...
            } else {
                //tampered or failed
                //$msg = "Payment failed for Hasn not verified...";
                $this->session->set_flashdata('success', 'Payment failed for Hasn not verified...');
                $booking_data = array('status' => 'Payment Failed');
                $this->db->where('id', $order_id);
                $this->db->update('bookings', $booking_data);
            }
        }
        redirect('users/My_booking');
        //$this->load->view('payment_response');
    }

    function about_us()
    {
        $this->load->view('about_us');
    }

    function contact_us()
    {
        $this->load->view('contactus_page');
    }

    function faq()
    {
        $this->load->view('faq');
    }

    function terms_and_service()
    {
        $this->load->view('terms_and_service');
    }

    function booking_condition()
    {
        $this->load->view('booking_condition');
    }

    function data_protection_policy()
    {
        $this->load->view('data_protection_policy');
    }

    function cancellation_and_refund()
    {
        $this->load->view('cancellation_and_refund');
    }

    function agent_policy()
    {
        $this->load->view('agent_policy');
    }

    function sendMailFileForUser($property_name, $to_email, $booking_id)
    {
        $url = 'https://api.sendgrid.com/';
        $user = 'no-reply@payingguestworld.com';
        $pass = 'PGW123456wolrd';
        $email = $to_email;
        $data['booking_id'] = $booking_id;
        $msg = $this->load->view('agent_order_mail', $data, true);
        $params = array(
            'api_user' => $user,
            'api_key' => $pass,
            'to' => $email,
            'subject' => "Resevation confirmed for" . " "  . $property_name,
            'html' => $msg,
            'text' => 'Paying Guest Wolrd',
            'from' => 'no-reply@payingguestworld.com'
            // 'files['.$fileName.']' => '@'.$filePath.'/'.$fileName
            //  'files' => $fileName
        );
        //print_r($params);
        $request = $url . 'api/mail.send.json';
        $session = curl_init($request);
        curl_setopt($session, CURLOPT_POST, true);
        curl_setopt($session, CURLOPT_POSTFIELDS, $params);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $response1 = curl_exec($session);
        //print_r($response1);
        curl_close($session);
    }

    function sendMailFileForAgent($property_name, $to_email_agent, $booking_id)
    {
        $url = 'https://api.sendgrid.com/';
        $user = 'no-reply@payingguestworld.com';
        $pass = 'PGW123456wolrd';
        $email = $to_email_agent;
        $data['booking_id'] = $booking_id;
        $msg = $this->load->view('agent_order_mail', $data, true);
        $params = array(
            'api_user' => $user,
            'api_key' => $pass,
            'to' => $email,
            'subject' => "Resevation confirmed for" . " "  . $property_name,
            'html' => $msg,
            'text' => 'Paying Guest Wolrd',
            'from' => 'no-reply@payingguestworld.com'
            // 'files['.$fileName.']' => '@'.$filePath.'/'.$fileName
            //  'files' => $fileName
        );
        //print_r($params);

        $request = $url . 'api/mail.send.json';
        $session = curl_init($request);
        curl_setopt($session, CURLOPT_POST, true);
        curl_setopt($session, CURLOPT_POSTFIELDS, $params);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $response1 = curl_exec($session);
        //print_r($response1);
        curl_close($session);
    }
}
