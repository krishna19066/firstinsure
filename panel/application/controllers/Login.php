<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
        // sets up the session
        $this->load->library('form_validation');            // Loading form validation library
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->library('pagination');
        //$this->load->model('AdminModel');
        //header('Access-Control-Allow-Origin : http://localhost:3000');   
    }

    public function index() {

        //$this->load->view('login_page');
        redirect('home/AgentLogin');
    }

    public function AgentLogin() {
        $this->form_validation->set_rules("email", "email", "trim|required");
        $this->form_validation->set_rules("password", "password", "trim|required");
        if ($this->form_validation->run() == false) {

            redirect('home/AgentLogin');
        } else {

            $email = $this->input->post("email");
            $password = $this->input->post("password");
            $getUser = $this->UserModel->CheckAdmin($email, $password);
            //print_r($getUser);
            // redirect('user_login/dashboard');
            if (!$getUser) {
                // login failed errror;

                $this->session->set_flashdata('login_error', TRUE);
                $data = array(
                    'error_message' => 'Invalid Email or Password'
                );
                $this->load->view('property_deler_login', $data);
                //redirect('Login');
            } else {
                foreach ($getUser as $res) {
                    $username = $res->name;
                    $agent_id = $res->id;
                }
                // login ;
                $this->session->set_userdata(array(
                    'logged_in' => TRUE,
                    'username' => $username,
                    'agent_id' => $agent_id,
                    'email' => $email));
                $u_rec_id = $this->session->userdata('username');
                redirect('dashboard');
            }
        }
    }

    function UserLogin() {
        $this->form_validation->set_rules("email", "email", "trim|required");
        $this->form_validation->set_rules("password", "password", "trim|required");
        if ($this->form_validation->run() == false) {
            redirect('home/UserLogin');
        } else {
            $email = $this->input->post("email");
            $password = $this->input->post("password");
            $getUser = $this->UserModel->CheckGuest($email, $password);
            //print_r($getUser);
            // redirect('user_login/dashboard');
            if (!$getUser) {
                // login failed errror;
                $this->session->set_flashdata('login_error', TRUE);
                $data = array(
                    'error_message' => 'Invalid Email or Password'
                );
                $this->load->view('user_login', $data);
                //redirect('Login');
            } else {
                foreach ($getUser as $res) {
                    $username = $res->name;
                    $user_id = $res->id;
                }
                // login ;
                $this->session->set_userdata(array(
                    'logged_in' => TRUE,
                    'username' => $username,
                    'user_id' => $user_id,
                    'email' => $email));
                $u_rec_id = $this->session->userdata('username');
                redirect('Users');
            }
        }
    }

    function Logout() {
        $this->session->sess_destroy();
        redirect('Home');
    }

}
