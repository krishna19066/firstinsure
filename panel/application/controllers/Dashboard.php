<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        // sets up the session
        $this->load->library('form_validation');            // Loading form validation library
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->library('pagination');
       // $this->load->library('Cloudinarylib');
        //header('Access-Control-Allow-Origin : http://localhost:3000'); 
        if (!$this->session->userdata('agent_id')) {
            redirect('login');
        }
    }

    function index()
    {
        $this->load->view('agent_dashboard');
    }

    function agentdashboard()
    {
        $this->load->view('property_deler_dashboard');
    }

    function add_listing()
    {
        $this->load->view('add_property_1');
    }
    
    function Viewdetails($lead_id){
         $table = 'lead';
         $data['lead_data'] = $this->UserModel->getAllDataWithID($table,$lead_id);
         $this->load->view('lead_details',$data);
    }
    
    function ViewAssignLead($lead_id){
         $lead_id;
         $table = 'final_lead';
         $data['lead_data'] = $this->UserModel->getAllDataWithID($table,$lead_id);
         $this->load->view('final_lead_details',$data);
    }
    
    function SameConfirmLead(){
        $current_date = date('Y-m-d h:i:s');
        $email = $this->input->post('email');
        $lead_id = $this->input->post('id');
        $fname = $this->input->post('name');
        $lname = $this->input->post('last_name');
        $salutation = $this->input->post('salutation');
        $mobile = $this->input->post('mobile');
        $dob = $this->input->post('dob');
        $gender = $this->input->post('gender');
        $state = $this->input->post('state');
        $city_code = $this->input->post('city');
        $state_id = $this->input->post('state_id');
        $pincode = $this->input->post('pincode');
        $appointment_date = $this->input->post('appointment_date');
        $appointment_time = $this->input->post('appointment_time');
        $appointment = $appointment_date . 'T'. $appointment_time;
        //$allocate_to = $this->input->post('allocated_to');
        $allocate_to = '';
        $allocate_at_date = $this->input->post('allocated_at');
        $allocate_at_time = $this->input->post('allocated_at_time');
        $allocate_at = $allocate_at_date .''.$allocate_at_time;
        $address_1 = $this->input->post('address_1');
        $address_2 = $this->input->post('address_2');
        $address_3 = $this->input->post('address_3');
        $income = $this->input->post('income');
        $cc_remarks = $this->input->post('cc_remarks');
        $follow_up_time = $this->input->post('follow_up_time');
        $status = $this->input->post('feedback');
        $data = array(
            'salutation'=>$salutation,
            'first_name'=>$fname,
            'last_name'=>$lname,
            'email'=>$email,
            'mobile'=>$mobile,
            'dob'=>$dob,
            'gender'=>$gender,
            'city_code'=>$city_code,
            'state_id'=>$state_id,
            'pincode'=>$pincode,
            'appointment_date'=>$appointment,
            'allocated_to'=>$allocate_to,
            'allocated_at'=>$current_date,
            'address_1'=>$address_1,
            'address_2'=>$address_2,
            'income'=>$income,
            'cc_remarks'=>$cc_remarks,
            'follow_up_time'=>$follow_up_time,
            'status'=>$status
            );
        $this->session->set_flashdata('success', 'Successfuly save lead data');
        $this->db->insert('final_lead', $data);
       // $property_id = $this->db->insert_id();
         redirect('Dashboard/lead_listing/');
    }
    
    function AssignLead(){
        $current_date = date('Y-m-d h:i:s');
        $email = $this->input->post('email');
        $lead_id = $this->input->post('id');
        $fname = $this->input->post('name');
        $lname = $this->input->post('last_name');
        $salutation = $this->input->post('salutation');
        $mobile = $this->input->post('mobile');
        $dob = $this->input->post('dob');
        $gender = $this->input->post('gender');
        $state = $this->input->post('state');
        $city_code = $this->input->post('city');
        $state_id = $this->input->post('state_id');
        $pincode = $this->input->post('pincode');
        $appointment_date = $this->input->post('appointment_date');
        //$appointment_time = $this->input->post('appointment_time');
        //$appointment = $appointment_date . 'T'. $appointment_time;
         $allocate_to = $this->input->post('allocated_to');
        //$allocate_to = '';
        $allocate_at_date = $this->input->post('allocated_at');
        $allocate_at_time = $this->input->post('allocated_at_time');
        //$allocate_at = $allocate_at_date .''.$allocate_at_time;
         $allocate_at = $current_date;
        $address_1 = $this->input->post('address_1');
        $address_2 = $this->input->post('address_2');
        $address_3 = $this->input->post('address_3');
        $income = $this->input->post('income');
        $cc_remarks = $this->input->post('cc_remarks');
        $follow_up_time = $this->input->post('follow_up_time');
        $status = $this->input->post('feedback');
       $data = array(
            'salutation'=>$salutation,
            'first_name'=>$fname,
            'last_name'=>$lname,
            'email'=>$email,
            'mobile'=>$mobile,
            'dob'=>$dob,
            'gender'=>$gender,
            'city_code'=>$city_code,
            'state_id'=>$state_id,
            'pincode'=>$pincode,
            'appointment_date'=>$appointment_date,
            'allocated_to'=>$allocate_to,
            'allocated_at'=>$current_date,
            'address_1'=>$address_1,
            'address_2'=>$address_2,
            'income'=>$income,
            'cc_remarks'=>$cc_remarks,
            'follow_up_time'=>$follow_up_time,
            'status'=>'Assigned'
            );
       // die;
         $url = 'https://sams.edelweisstokio.in/Wrapper/api/lead/WebAggregateSaveAppointment';  // for test mode
         $ch = curl_init($url);
         $payload = json_encode(array("childDetailsList" => array(["ChildName" => "", "ChildAge" => "","ChildGender"=>""]),
                                    "customerVehiclesList" => array(["VehicleType" => "", "VehicleMfrId" => "0","VehModelId"=>"0","VehRegNum"=>""]),
		                            "AffiliateCode" => "A080", "Salutation" => "$salutation","FirstName"=>"$fname","MiddleName"=>"","LastName"=>"$lname","Telephone"=>"","PrimaryMobile"=>"$mobile",
		                            "SecondaryMobile" => "", "ContactAddressLine1" => "$address_1","ContactAddressLine2"=>"$address_2","ContactAddressLine3"=>"$address_2","Landmark"=>"","ContactState"=>"$state_id","ContactCity"=>"$city_code",
		                            "ContactPinCode" => "$pincode", "EmailId" => "$email","DateofBirth"=>"$dob","Age"=>"","Income"=>"$income","Education"=>"7","Gender"=>"M","ProfessionType"=>"","Profession"=>"",
		                            "CurrProdName" => "", "MaritalStatus" => "","NoOfChildren"=>"","TotalLifeCover"=>"0","AnnualPremForLifeIns"=>"","HasTermIns"=>false,"HouseholdNOP"=>"0","HouseholdPremium"=>"","HouseholdIncome"=>"",
		                            "IsSpouseWorking" => false, "Remark" => "","HasHealthInsurance"=>false,"TotHealthCover"=>"0","AnnualPremHI"=>"","HasMutualFund"=>false,"MFValue"=>"0","HasHomeLoan"=>false,"BankingRelationship"=>"",
		                            "CustomerVintage" => "", "HasVehicle" => false,"AppointmentDateTime"=>"$appointment_date","AllocatedTo"=>"$allocate_to","AllocatedAt"=>"$allocate_at","PlanInterestedIn"=>"Investment Plan","SubCampaignCode"=>"C004","CampaignCode"=>"CM01","CallAndGo"=>true,
		                            "ExpectedPremium" => "0", "ExpectedClosureDate" => "","IsJointCall"=>false,"ReadyAppointmentFlag"=>true,"CreatedAtCC"=>"$current_date","ProfessionName"=>"","GoogleMapLink"=>"","CCRemarks"=>"$cc_remarks","ApplicationPlatform"=>3,
		                            "UserId" => "", "UserCode" => "","ReqId"=>"A001#9999999999#291220143349","LocalAppDBVersion"=>null,"userdetails"=>null,"lstUserdetails"=>null,"lstCBMBMUsers"=>null,"ImageUpload"=>null,"dbPlatformInfo"=>null
									 ));
        // print_r($payload);
         //die;
		// Attach encoded JSON string to the POST fields
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		// Set the content type to application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Accept:application/json'));
		// Return response instead of outputting
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_ENCODING, "gzip");
		// Execute the POST request
		$result = curl_exec($ch);
		$res1 = json_decode($result,true);
		$lead_data = json_decode($result,true);
	//	print_r($lead_data);
	//	die;
		$code = $lead_data['Code'];
		$msg = $lead_data['Message'];
		 $this->session->set_flashdata('success', $msg);
         $this->db->where(array('id' => $lead_id));
         $this->db->update('final_lead', $data);
        //redirect('Dashboard/property_listing');
        redirect('Dashboard/ViewAssignLead/'.$lead_id);
        
    }
    function conf_lead_listing(){
        $table = 'final_lead';
        $data['listing'] = $this->UserModel->getAllData($table);
        $this->load->view('final_lead_listing', $data);
    }

    function format_uri($string, $separator = '-')
    {
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = array('&' => 'and', "'" => '');
        $string = mb_strtolower(trim($string), 'UTF-8');
        $string = str_replace(array_keys($special_cases), array_values($special_cases), $string);
        $string = preg_replace($accents_regex, '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'));
        $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
        $string = preg_replace("/[$separator]+/u", "$separator", $string);
        return $string;
    }

    function BusinessData()
    {
        $agent_id = $this->session->userdata('agent_id');
        $agent_email = $this->session->userdata('email');
        $business_name = $this->input->post('business_name');
        $slug_url =  $this->format_uri($business_name);
        $property_type = $this->input->post('category');
        $address = $this->input->post('address');
        $landmark = $this->input->post('landmark');
        $country = $this->input->post('country');
        $state = $this->input->post('state');
        $city1 = $this->input->post('city');
        $city =  ucfirst($city1);
        $pincode = $this->input->post('pincode');
        $price = $this->input->post('price');
        $checkin = $this->input->post('opening_time');
        $checkout = $this->input->post('closing_time');
        $booking_no = $this->input->post('booking_no');
        $booking_email = $this->input->post('booking_email');
        $video_link = $this->input->post('video_link');
        $map_url = $this->input->post('map_url');
        $property_desc = $this->input->post('business_desc');
        $policy = $this->input->post('policy');
        $amenity1 = $this->input->post('amenity');
        $near_by = $this->input->post('near_by');
        $locality = $this->input->post('locality');
        $amenity = implode(', ', $amenity1);
        $data = array(
            'property_name' => $business_name,
            'property_type' => $property_type,
            'propertyURL' => $slug_url,
            'agent_id' => $agent_id,
            'address' => $address,
            'landmark' => $landmark,
            'country' => $country,
            'state' => $state,
            'city' => $city,
            'pincode' => $pincode,
            'price' => $price,
            'checkIN' => $checkin,
            'checkOut' => $checkout,
            'booking_no' => $booking_no,
            'booking_email' => $booking_email,
            'video_link' => $video_link,
            'map_url' => $map_url,
            'propertyInfo' => $property_desc,
            'policy' => $policy,
            'amenities' => $amenity,
            'near_by' => $near_by,
            'locality' => $locality,
            'status' => 'Pending',
            'agent_status' => 'Active'
        );
        //print_r($data);
        // die;
        //$this->sendMailFileForCommon($property_name, $to_email);
        $this->session->set_flashdata('success', 'Successfuly Insert data');
        $this->db->insert('property', $data);
        $property_id = $this->db->insert_id();
        //redirect('Dashboard/property_listing');
        redirect('Dashboard/property_listing');
    }

    function property_listing()
    {
        $agent_id = $this->session->userdata('agent_id');
        $data['pro_listing'] = $this->UserModel->getPropertyListing($agent_id);
        $this->load->view('property_listing', $data);
    }
    function Editlisting($id)
    {
        $data['property_data'] = $this->UserModel->getpropertyWithID($id);
        $this->load->view('edit_property', $data);
    }
    function UpdatePropertyData()
    {
        $property_id = $this->input->post('property_id');
        $property_name = $this->input->post('property_name');
        //$property_type = $this->input->post('property_type');
        $propertyURL = $this->input->post('propertyURL');
        $address = $this->input->post('address');
        $price = $this->input->post('price');
        $landmark = $this->input->post('landmark');
        //$country = $this->input->post('country');
        //$state = $this->input->post('state');
        // $city = $this->input->post('city');
        //$pincode = $this->input->post('pincode');
        $checkin = $this->input->post('checkin');
        $checkout = $this->input->post('checkout');
        $booking_no = $this->input->post('booking_no');
        $booking_email = $this->input->post('booking_email');
        $video_link = $this->input->post('video_link');
        $map_url = $this->input->post('map_url');
        $property_desc = $this->input->post('property_desc');
        $policy = $this->input->post('policy');
        // $amenity1 = $this->input->post('amenity');
        $near_by = $this->input->post('near_by');
        $locality = $this->input->post('locality');
        // $amenity = implode(', ', $amenity1);
        $data = array(
            'property_name' => $property_name,
            // 'property_type' => $property_type,
            'propertyURL' => $propertyURL,
            // 'agent_id' => $agent_id,
            'address' => $address,
            'landmark' => $landmark,
            'price' => $price,
            //'country' => $country,
            //  'state' => $state,
            //  'city' => $city,
            //  'pincode' => $pincode,
            'checkIN' => $checkin,
            'checkOut' => $checkout,
            'booking_no' => $booking_no,
            'booking_email' => $booking_email,
            'video_link' => $video_link,
            'map_url' => $map_url,
            'propertyInfo' => $property_desc,
            'policy' => $policy,
            //  'amenities' => $amenity,
            'near_by' => $near_by,
            'locality' => $locality
        );
        $this->session->set_flashdata('success', 'Your listing has been Updated!');
        $this->db->where(array('id' => $property_id));
        $this->db->update('property', $data);
        redirect('Dashboard/property_listing');
    }

    function UploadPropertyImg($property_id)
    {
        $data['property_id'] = $property_id;
        $agent_id = $this->session->userdata('agent_id');
        $data['results'] = $this->UserModel->getPropertyAllImages($agent_id, $property_id);
        $this->load->view('pro_upload_img', $data);
    }

    function manage_room($property_id)
    {
        $data['property_id'] = $property_id;
        $data['property_data'] = $this->UserModel->getpropertyWithID($property_id);
        $agent_id = $this->session->userdata('agent_id');
        $data['results'] = $this->UserModel->getAllRoomDataWithPropertyID($agent_id, $property_id);
        $this->load->view('manage_room_page', $data);
    }

    function InsertRoomData()
    {
        $agent_id = $this->session->userdata('agent_id');
        $property_id = $this->input->post('property_id');
        $room_name = $this->input->post('room_name');
        $room_price = $this->input->post('room_price');
        $occupancy = $this->input->post('occupancy');
        $bed = $this->input->post('bed');
        $no_of_room = $this->input->post('no_of_room');
        $room_size = $this->input->post('room_size');
        $after_room_price = $this->input->post('after_discount_price');
        $room_category = $this->input->post('room_category');
        $gender = $this->input->post('gender');
        $data = array(
            'roomName' => $room_name,
            'agent_id' => $agent_id,
            'propertyID' => $property_id,
            'roomPrice' => $room_price,
            'occupancy' => $occupancy,
            'bed' => $bed,
            'no_of_room' => $no_of_room,
            'size' => $room_size,
            'after_discount_price' => $after_room_price,
            'room_category' => $room_category,
            'gender' => $gender,
            'status' => 'Y'
        );

        //print_r($data);
        //die;
        //$this->db->where('id', $product_id);
        $this->session->set_flashdata('success', 'Added Room, Now you can upload property and room images');
        $this->db->insert('rooms', $data);
        //redirect('Dashboard/manage_room/' . $property_id);
        redirect('Dashboard/property_listing');
    }

    function InsertPropertyImg()
    {
        $agent_id = $this->session->userdata('agent_id');
        $table = 'cloudnary';
        $cloudnary = $this->UserModel->getAllData($table);
        //print_r($cloudnary);

        foreach ($cloudnary as $res) {
            $cloud_cdnName = $res->cloud_name;
            $cloud_cdnKey = $res->api_key;
            $cloud_cdnSecret = $res->api_secret;
        }
        Cloudinary::config(array(
            "cloud_name" => $cloud_cdnName,
            "api_key" => $cloud_cdnKey,
            "api_secret" => $cloud_cdnSecret
        ));

        $property_id = $this->input->post('property_id');
        $img = $_FILES['image_org']['name'];

        $timdat = date('Y-m-d');
        $timtim = date('H-i-s');
        $timestmp = $timdat . "_" . $timtim;
        $status = \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "msme/business"));
        $data = array(
            'img' => $timestmp,
            'agent_id' => $agent_id,
            'propertyID' => $property_id,
            'type' => 'property',
            'propertyF' => 'N',
        );

        //print_r($data);
        //die;
        //$this->db->where('id', $product_id);
        $this->db->insert('images', $data);
        redirect('Dashboard/UploadPropertyImg/' . $property_id);
    }

    function upload_room_img($property_id, $room_id)
    {
        $data['property_id'] = $property_id;
        $data['room_id'] = $room_id;
        $agent_id = $this->session->userdata('agent_id');
        $data['results'] = $this->UserModel->getRoomAllImages($property_id, $agent_id, $room_id);
        $this->load->view('room_upload_img', $data);
    }

    function InsertRoomImg()
    {
        $agent_id = $this->session->userdata('agent_id');
        $table = 'cloudnary';
        $cloudnary = $this->UserModel->getAllData($table);
        //print_r($cloudnary);
        foreach ($cloudnary as $res) {
            $cloud_cdnName = $res->cloud_name;
            $cloud_cdnKey = $res->api_key;
            $cloud_cdnSecret = $res->api_secret;
        }
        Cloudinary::config(array(
            "cloud_name" => $cloud_cdnName,
            "api_key" => $cloud_cdnKey,
            "api_secret" => $cloud_cdnSecret
        ));

        $property_id = $this->input->post('property_id');
        $room_id = $this->input->post('room_id');

        $img = $_FILES['image_org']['name'];

        $timdat = date('Y-m-d');
        $timtim = date('H-i-s');
        $timestmp = $timdat . "_" . $timtim;
        $status = \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "pgw/room"));
        $data = array(
            'img' => $timestmp,
            'agent_id' => $agent_id,
            'propertyID' => $property_id,
            'roomID' => $room_id,
            'type' => 'room',
            'roomF' => 'N',
        );

        //print_r($data);
        //die;
        //$this->db->where('id', $product_id);
        $this->db->insert('images', $data);
        redirect('Dashboard/upload_room_img/' . $property_id . '/' . $agent_id);
    }
    function add_review($id){
        $data['property_id'] = $id;
        $this->load->view('add_review', $data);
    }
    function Insert_review(){
        $property_id = $this->input->post('property_id');
        $name = $this->input->post('name');
        $rating = $this->input->post('rating');
        $review = $this->input->post('review');
        $data = array(
            'property_id' =>$property_id,
            'name'=>$name,
            'rating'=>$rating,
            'content'=>$review
        );
        $this->session->set_flashdata('success', ' Success Added Review');
        $this->db->insert('testimonial', $data);
        //redirect('Dashboard/manage_room/' . $property_id);
        redirect('Dashboard/add_review/'.$property_id);
    }

    function add_lead()
    {
       
        $this->load->view('add_lead');
    }
    function SubmitLead(){
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $city = $this->input->post('city');
        $mobile = $this->input->post('mobile');
        $dob = $this->input->post('dob');
        $gender = $this->input->post('gender');
        $lead_data = array(
            'name' => $name,
            'mobile' => $mobile,
            'email'=>$email,
            'city'=>$city,
            'dob'=>$dob,
            'gender'=>$gender,
            'source'=>'admin',
            );
        //$this->db->where('id', $user_id);
        $this->db->insert('lead', $lead_data);
        $this->session->set_flashdata('success', 'Successfuly added lead...');
        redirect('Dashboard/lead_listing');
    }
    function lead_listing(){
        $table = 'lead';
        $data['listing'] = $this->UserModel->getAllData($table);
        $this->load->view('lead_listing', $data);
    }
    
    function hms_listing(){
        $table = 'hms';
        $data['listing'] = $this->UserModel->getAllData($table);
        $this->load->view('hms_listing', $data); 
    }

    function MyAccount()
    {
        $agent_id = $this->session->userdata('agent_id');
        $table = 'admin';
        $data['account'] = $this->UserModel->getAllDataWithID($table, $agent_id);
        $this->load->view('agent_account', $data);
    }

    function UpdateAccount()
    {
        //$name = $this->input->post('name');
        //$email = $this->input->post('email');
        $user_id = $this->input->post('id');
        $mobile = $this->input->post('mobile');
        $password = $this->input->post('password');
        $user_data = array('password' => $password, 'mobile' => $mobile);
        $this->db->where('id', $user_id);
        $this->db->update('agent', $user_data);
        $this->session->set_flashdata('success', 'Success Update info...');
        redirect('Dashboard/MyAccount');
    }

    function ViewVoucher($booking_id)
    {
        $user_id = $this->session->userdata('user_id');
        $data['booking_data'] = $this->UserModel->getBookingData($booking_id);
        //print_r($data['review']);
        $this->load->view('agent-voucher-page', $data);
    }

    function ActiveProperty($property_id)
    {
        $agent_id = $this->session->userdata('agent_id');
        $status = 'Active';
        $data = array(
            'agent_status' => $status
        );
        $this->session->set_flashdata('success', 'Your listing is Active now!');
        $this->db->where(array('id' => $property_id, 'agent_id' => $agent_id));
        $this->db->update('property', $data);
        redirect('Dashboard/property_listing');
    }
    function InactiveProperty($property_id)
    {
        $agent_id = $this->session->userdata('agent_id');
        $status = 'Inactive';
        $data = array(
            'agent_status' => $status
        );
        $this->session->set_flashdata('success', 'Your listing is Inactive now!');
        $this->db->where(array('id' => $property_id, 'agent_id' => $agent_id));
        $this->db->update('property', $data);
        redirect('Dashboard/property_listing');
    }

    function ActiveRoom($room_id, $property_id)
    {
        $agent_id = $this->session->userdata('agent_id');
        $status = 'Y';
        $data = array(
            'status' => $status
        );
        $this->session->set_flashdata('success', 'Your Room has been Active!');
        $this->db->where(array('id' => $room_id, 'agent_id' => $agent_id));
        $this->db->update('rooms', $data);
        redirect('Dashboard/manage_room/' . $property_id);
    }
    function InactiveRoom($room_id, $property_id)
    {
        $agent_id = $this->session->userdata('agent_id');
        $status = 'N';
        $data = array(
            'status' => $status
        );
        $this->session->set_flashdata('success', 'Your Room has been Inactive!');
        $this->db->where(array('id' => $room_id, 'agent_id' => $agent_id));
        $this->db->update('rooms', $data);
        redirect('Dashboard/manage_room/' . $property_id);
    }
    function DeleteImage($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('images');
        redirect('dashboard/property_listing');
    }
    function SetFeatureImg($id)
    {
        $data = array('propertyF' => 'Y');
        $this->db->where('id', $id);
        $this->db->update('images', $data);
        redirect('dashboard/property_listing');
    }

    function sendMailFileForCommon($property_name, $to_email)
    {
        $url = 'https://api.sendgrid.com/';
        $user = 'no-reply@payingguestworld.com';
        $pass = 'PGW123456wolrd';
        $email = $to_email;
        $data['property_name'] = $property_name;
        $msg = $this->load->view('common_mail', $data,  true);
        $params = array(
            'api_user' => $user,
            'api_key' => $pass,
            'to' => $email,
            'subject' => "Thank you for Property Listing with PayingGuestWorld",
            'html' => $msg,
            'text' => 'Paying Guest Wolrd',
            'from' => 'no-reply@payingguestworld.com'
            // 'files['.$fileName.']' => '@'.$filePath.'/'.$fileName
            //  'files' => $fileName
        );
        //print_r($params);

        $request = $url . 'api/mail.send.json';
        $session = curl_init($request);
        curl_setopt($session, CURLOPT_POST, true);
        curl_setopt($session, CURLOPT_POSTFIELDS, $params);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $response1 = curl_exec($session);
        //print_r($response1);
        curl_close($session);
    }
}
