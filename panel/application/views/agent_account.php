<!DOCTYPE html>
<html>
    <?php include 'header.php'; ?>
    <?php
    foreach ($account as $res) {
        
    }
    ?>

    <body>
        <?php include 'agent_header.php'; ?>
        <!-- container open -->
        <div class="container-fluid dashboard-ac form_start mt-4">
            <div class="container-fluid mb-4">
                <div class="row">
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 pr-0">
                        <?php include 'agent_menu.php'; ?>
                    </div>
                    <!--column-->
                    <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 col-12 pl-0">
                        <form action="<?= base_url() ?>Dashboard/UpdateAccount" method="POST">
                            <div class="form_bg">
                                <div class="row dash-bg">
                                    <div class="col-md-12">
                                        <h4><b>My Info</b></h4>
                                        <?php if ($this->session->flashdata('success')) { ?>
                                            <div class="alert alert-success">
                                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                                            </div>
                                        <?php } ?>
                                        <hr>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="required">Full Name</label>
                                        <input type="text" name="name" class="form-control" value="<?php echo $res->name; ?>" readonly="">
                                        <input type="hidden" name="id" class="form-control" value="<?php echo $res->id; ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="required">Email</label>
                                        <input type="text" name="email" class="form-control" value="<?php echo $res->email; ?>" readonly="">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="required">Password</label>
                                        <input type="text" name="password" class="form-control" value="<?php echo $res->password; ?>">
                                    </div>

                                    <div class="col-md-12 text-right mt-3">
                                        <input type="submit" class="btn payingguest-btn no2" value="Update Information">
                                    </div>
                                </div>
                                <!--row-->

                            </div>
                        </form>
                    </div>
                    <!--column-->

                </div>
                <!--column-->
            </div>
            <!--row-->
        </div>
        <!-- container close -->
    </div>
    <?php include 'footer.php'; ?>
</body>
</html>