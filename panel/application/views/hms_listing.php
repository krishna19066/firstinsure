<!DOCTYPE html>
<html>
<?php include 'header.php'; ?>
<style>
    #example_wrapper{
        width:100%;
    }
</style>

<body>
    <?php include 'agent_header.php'; ?>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/table/jquery.dataTables.min.css">
        <script type="text/javascript" src="<?= base_url() ?>assets/table/jquery-3.5.1.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/table/jquery.dataTables.min.js"></script>
    <!-- container open -->
    <div class="container-fluid dashboard-ac form_start mt-4">
        <div class="container-fluid mb-4">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 pr-0">
                    <?php include 'agent_menu.php'; ?>
                </div>
                <!--column-->
                <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 col-12 pl-0">
                    <div class="form_bg">
                        <div class="row dash-bg">
                            <div class="col-md-12">
                                <h4><b>All HMS Listing</b></h4>
                                <hr>
                                 <?php if ($this->session->flashdata('success')) { ?>
                                        <div class="alert alert-success">
                                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                                        </div>
                                    <?php } ?>
                            </div>
                            <!--column-->

                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Agent Name</th>
                                        <th>BM Name</th>
                                        <th>ASH Name</th>
                                        <th>City</th>
                                        <th>ZH Name</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($listing as $list) {
                                        
                                    ?>
                                        <tr>
                                            <td>
                                                <?php echo $list->agent_name; ?></br>
                                                <small style="color: #9C27B0;"><?php echo $list->agent_code; ?></small>
                                            </td>
                                            <td>
                                                <?php echo $list->bm_name; ?></br>
                                                <small style="color: #9C27B0;"><?php echo $list->bm_code; ?></small>
                                            </td>
                                            <td>
                                                <?php echo $list->ash_name; ?></br>
                                                <small style="color: #FF5722;"> Status:<?php echo $list->ash_code; ?></small>
                                            </td>
                                            <td><?php echo $list->city; ?></td>
                                            <td>
                                                <?php echo $list->zh_name; ?></br>
                                                <small style="color: #FF5722;"> Status:<?php echo $list->zh_code; ?></small>
                                            </td>
                                            <td><?php echo $list->status; ?></td>
                                        </tr>
                                    <?php } ?>

                                </tbody>

                            </table>


                        </div>
                        <!--row-->
                    </div>
                    <!--column-->

                </div>
                <!--column-->
            </div>
            <!--row-->
        </div>
        <!-- container close -->
    </div>
    <?php include 'footer.php'; ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
</body>

</html>