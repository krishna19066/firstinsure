<!DOCTYPE html>
<html>
    <?php include 'header.php'; ?>
    <body>
        <div class="property_header">
            <div class="container-fluid">
                <?php //include 'static-page-menu.php'; ?>
            </div><!--container--fluid-->
        </div><!--property_header-->
        <div class="container-fluid customer_form" id="usercontent">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3 col-sm-offset-2 col-sm-8">
                        <form class="property_form" action="<?= base_url() ?>index.php/Login/AgentLogin" method="POST">
                            <div class="heading">Login in</div>
                            <?php
                            echo "<div class='text-center' style='color:#ff9800;'>";
                            if (isset($error_message)) {
                                echo $error_message;
                            }
                            echo validation_errors();
                            echo "</div>";
                            ?>
                            <div class="form-group">
                                <i class="fa fa-envelope"></i>
                                <input class="form-control" name="email" type="email" placeholder="Email Address" required>
                            </div>
                            <div class="form-group">
                                <i class="fa fa-lock"></i>
                                <input class="form-control" name="password" type="password" placeholder="Password" required>
                            </div>
                            <div class="form-terms">
                                <input type="checkbox">
                                <span>Keep me Signed in</span>
                            </div>
                            <div class="form-group text-center mt-4">
                                <input type="submit" class="btn btn-default" value="Sign In">
                            </div>
                            <div class="form-group text-center">
                                <span class="form-login">Forgot your password?<a href="<?= base_url() ?>home/ForgotPassword"> Click</a></span>

                            </div>
                           
                        </form>
                    </div>
                </div>
            </div>
        </div><!--container-->
    </div><!--container--fluid-->
    <div class="container-fluid footer-corp">
        <p class="m-0">2020 FirstInsure. All rights reserved </p>
    </div>


    <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/custom.js"></script>
    <script type="text/javascript">
        function myFunction(x) {
            if (x.matches) {
                $(".mobile_remove").remove();
            }
        }

        var x = window.matchMedia("(min-width: 320px) and (max-width: 767px)")
        myFunction(x)
        x.addListener(myFunction)

        //show//
        function myreFunction(e) {
            if (e.matches) {
                $(".desktop_off").show();
            }
        }

        var e = window.matchMedia("(min-width: 320px) and (max-width: 767px)")
        myFunction(x)
        e.addListener(myreFunction)
    </script>
    <script type="text/javascript">
        function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
        }

    </script>
    <script src="js/language.js"></script> 

</body>
</html>