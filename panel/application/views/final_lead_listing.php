<!DOCTYPE html>
<html>
<?php include 'header.php'; ?>
<style>
    #example_wrapper{
        width:100%;
    }
</style>

<body>
    <?php include 'agent_header.php'; ?>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/table/jquery.dataTables.min.css">
        <script type="text/javascript" src="<?= base_url() ?>assets/table/jquery-3.5.1.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/table/jquery.dataTables.min.js"></script>
    <!-- container open -->
    <div class="container-fluid dashboard-ac form_start mt-4">
        <div class="container-fluid mb-4">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 pr-0">
                    <?php include 'agent_menu.php'; ?>
                </div>
                <!--column-->
                <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 col-12 pl-0">
                    <div class="form_bg">
                        <div class="row dash-bg">
                            <div class="col-md-12">
                                <h4><b>All Lead Listing</b></h4>
                                <hr>
                                 <?php if ($this->session->flashdata('success')) { ?>
                                        <div class="alert alert-success">
                                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                                        </div>
                                    <?php } ?>
                            </div>
                            <!--column-->

                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>City</th>
                                        <th>DOB</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($listing as $list) {
                                        
                                    ?>
                                        <tr>
                                            <td>
                                                <?php echo $list->first_name; ?> <?php echo $list->last_name; ?></br>
                                                <small style="color: #9C27B0;"><?php echo $list->gender; ?></small>
                                                </td>
                                            <td><?php echo $list->email; ?></td>
                                            <td>
                                                <?php echo $list->mobile; ?></br>
                                                <small style="color: #FF5722;"> Status:<?php echo $list->status; ?></small>
                                            </td>

                                            <td><?php echo $list->city_code; ?></td>
                                            <td><?php echo $list->dob; ?></td>
                                            <td><?php echo $list->timestamp; ?></td>
                                            <td>
                                                <a href="<?= base_url() ?>index.php/dashboard/ViewAssignLead/<?php echo $list->id; ?>"> <button class="btn payingguest-btn">Assign Lead</button> </a></td>
                                        </tr>
                                    <?php } ?>

                                </tbody>

                            </table>


                        </div>
                        <!--row-->
                    </div>
                    <!--column-->

                </div>
                <!--column-->
            </div>
            <!--row-->
        </div>
        <!-- container close -->
    </div>
    <?php include 'footer.php'; ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
</body>

</html>