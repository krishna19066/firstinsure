<nav class="sidebar">
    <ul class="nav">
        <li class="nav-profile">
            <div class="image">
                <a href="javascript:;"><i class="fa fa-user avatar"></i></a>
            </div>
            <div class="info">
                <?php echo $username = $this->session->userdata('username'); ?>
                <p> <?php echo $username = $this->session->userdata('email'); ?></p>
                 <?php  $agent_id = $this->session->userdata('agent_id'); ?>
            </div>
        </li>
    </ul>
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link " href="<?= base_url() ?>index.php/Dashboard"><i class="fa fa-book"></i>Dashboard</a>
        </li>
        <?php if($agent_id == '1' || $agent_id == '3'){ ?>
        <li class="nav-item ">
            <a class="nav-link " href="<?= base_url() ?>index.php/dashboard/add_lead"><i class="fa fa-list"></i>Add Lead</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="<?= base_url() ?>index.php/dashboard/lead_listing"><i class="fa fa-list-alt"></i>Lead Listing</a>
        </li>
       <?php } ?>
        <?php if($agent_id == '1' || $agent_id == '2'){ ?>
         <li class="nav-item ">
            <a class="nav-link " href="<?= base_url() ?>index.php/dashboard/conf_lead_listing"><i class="fa fa-list-alt"></i> Confirm Lead </a>
        </li>
         <li class="nav-item ">
            <a class="nav-link " href="<?= base_url() ?>index.php/dashboard/hms_listing"><i class="fa fa-list-alt"></i>HMS Hierarchy</a>
        </li>
        <?php } ?>
        <li class="nav-item">
            <a href="<?= base_url() ?>index.php/dashboard/Myaccount" class="nav-link"><i class="fa fa-cogs"></i>My Account</a>
        </li>
         <li class="nav-item">
            <a href="<?= base_url() ?>index.php/Login/Logout" class="nav-link"><i class="fa fa-cogs"></i>Logout</a>
        </li>
    </ul>
</nav>