<!DOCTYPE html>
<html>
    <?php include 'header.php'; ?>
    <body>
        <?php include 'agent_header.php'; ?>
        <!-- container open -->
        <div class="container-fluid dashboard-ac form_start mt-4">
            <div class="container-fluid mb-4">
                <div class="row">
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 pr-0">
                        <?php include 'agent_menu.php'; ?>
                    </div>
                    <!--column-->
                    <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 col-12 pl-0">
                        <form action="<?= base_url() ?>index.php/dashboard/SubmitLead" method="POST">
                            <div class="form_bg">
                                <div class="row dash-bg">
                                    <div class="col-md-12">
                                        <h4><b>Add Lead</b></h4>
                                        <?php if ($this->session->flashdata('success')) { ?>
                                            <div class="alert alert-success">
                                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                                            </div>
                                        <?php } ?>
                                        <hr>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="required">Full Name</label>
                                        <input type="text" name="name" class="form-control" value="" required="">
                                        <input type="hidden" name="id" class="form-control" value="">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="required">Email</label>
                                        <input type="text" name="email" class="form-control" value="" >
                                    </div>
                                     <div class="col-md-6">
                                        <label class="required">Mobile</label>
                                        <input type="text" name="mobile" class="form-control" value="" required>
                                    </div>

                                    <div class="col-md-6">
                                        <label class="required">Gender</label>
                                         <select class="form-control" name="gender">
                                            <option value="" style="color: black;">Select Gender</option>
                                            <option value="Male" style="color: black;">Male</option>
                                            <option value="Female" style="color: black;">Female</option>
                               
                                            </select>
                                    </div>
                                      <div class="col-sm-6">
                                            <label class="required">Date of Birth</label>
                                            <input class="form-control form-text" type="date" name="dob" value="" placeholder="DOB" required />
                                        </div>
                                         <div class="col-sm-6">
                                              <label class="required">City Code</label>
                                              <select class="form-control" name="city" required>
                                           <option value="" style="color: black;">Select City</option>
                                            <?php
                                            $table2 = "cities";
                                            $city_data  = $this->UserModel->getAllData($table2);
                                            foreach($city_data as $ct){
                                            ?>
                                            <option value="<?php echo $ct->city_code; ?>" style="color: black;"><?php echo $ct->city_name; ?></option>
                                        <?php } ?>
                                            </select>
                                        </div>

                                    <div class="col-md-12 text-right mt-3">
                                        <input type="submit" class="btn payingguest-btn no2" value="Submit Information">
                                    </div>
                                </div>
                                <!--row-->

                            </div>
                        </form>
                    </div>
                    <!--column-->

                </div>
                <!--column-->
            </div>
            <!--row-->
        </div>
        <!-- container close -->
    </div>
    <?php include 'footer.php'; ?>
</body>
</html>