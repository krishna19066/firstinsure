<footer>
    <div class="container-fluid footer_bg">
        <div class="container">
            <div class="row">
              
                <!--column-->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-6 col-6 mt-5 copyright mobile_off text-center">
                    <p>2020 FirstInsure. All rights reserved</p>
                </div>
                <!--column-->
               
            </div>
            <!--row-->
        </div>
        <!--container-->
    </div>
    <!--container-fluid-->
</footer>
<!--<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.js"></script>-->
<script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.nice-select.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/js/gijgo.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/custom.js"></script>
<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
    }

</script>
<script>
$(function($) {
          let url = window.location.href;
          $('.sidebar li.nav-item a').each(function() {
            if (this.href === url) {
              $(this).closest('li').addClass('active');
            }
          });
        });
</script>
<script src="<?= base_url() ?>assets/js/language.js"></script> 