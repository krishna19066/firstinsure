<!DOCTYPE html>
<html>
    <?php include 'header.php'; ?>

    <?php
    $agent_id = $this->session->userdata('agent_id');
    $table = "lead";
     $lead_data = $this->UserModel->getAllData($table);
    $total_lead = count($lead_data);
  //  $inactive_listing = $this->UserModel->getAllListingWithInactive($agent_id);
    $total_inactive = '0';
   // $booking_listing = $this->UserModel->getAllBookingData($agent_id);
    $total_booking = '1';
    
    ?>

    <body>
        <?php include 'agent_header.php'; ?>
        <!-- container open -->
        <div class="container-fluid dashboard-ac form_start mt-4">
            <div class="container-fluid mb-4">
                <div class="row">
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 pr-0">
                        <?php include 'agent_menu.php'; ?>
                    </div>
                    <!--column-->
                    <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 col-12 pl-0">
                        <div class="form_bg">
                            <div class="row dash-bg">
                                <div class="col-md-12">
                                    <h4><b>Dashboard</b></h4>
                                    <hr>
                                </div>
                                <!--column-->
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 booking-details">
                                   
                                    <div class="statistic-container fl-wrap">
                                        <!-- statistic-item-wrap--> 
                                        <div class="statistic-item-wrap">
                                            <div class="statistic-item gradient-bg fl-wrap">
                                                <i class="fa fa-book"></i>
                                                <div class="statistic-item-numder"><?php echo $total_lead; ?></div>
                                                <h5>Total Lead</h5>
                                            </div>
                                        </div>
                                        <!-- statistic-item-wrap end-->                                            
                                        <!-- statistic-item-wrap--> 
                                        <div class="statistic-item-wrap">
                                            <div class="statistic-item gradient-bg fl-wrap">
                                                <i class="fa fa fa-list"></i>
                                                <div class="statistic-item-numder"><?php echo $total_lead; ?></div>
                                                <h5> Total Email </h5>
                                            </div>
                                        </div>
                                        <!-- statistic-item-wrap end-->                                             
                                       
                                    </div>
                                   <!-- <div class="dashboard-list-box fl-wrap activities">
                                        <div class="dashboard-header fl-wrap">
                                            <h3>Recent Activities</h3>
                                        </div>
                                       
                                        <div class="dashboard-list">
                                            <div class="dashboard-message">
                                                <span class="new-dashboard-item"><i class="fa fa-times"></i></span>
                                                <div class="dashboard-message-text">
                                                    <p><i class="fa fa-check"></i> Your listing <a href="#">Luxury Restourant</a> has been approved! </p>
                                                </div>
                                            </div>
                                        </div>
                                         
                                        <div class="dashboard-list">
                                            <div class="dashboard-message">
                                                <span class="new-dashboard-item"><i class="fa fa-times"></i></span>
                                                <div class="dashboard-message-text">
                                                    <p><i class="fa fa-heart"></i>Someone bookmarked your <a href="#">Event In City Mol</a> listing!</p>
                                                </div>
                                            </div>
                                        </div>
                                                                           
                                       
                                    </div>-->
                                </div>
                            </div>
                            <!--row-->
                        </div>
                    </div>
                    <!--column-->

                </div>
                <!--column-->
            </div>
            <!--row-->
        </div>
        <!-- container close -->
    </div>
    <?php include 'footer.php'; ?>
</body>
</html>