 <?php if ($this->session->userdata('agent_id')) { ?>
<?php include 'agent_header.php'; ?>
 <?php } else { ?>
<nav class="navbar navbar-expand-lg second_navbar">
    <div class="container">
        <a class="navbar-brand" href="<?= base_url() ?>home"><img class="img-fluid" style="width:150px;" src="<?= base_url() ?>assets/images/logo-1.png"></a>
        <div class="" id="navbarSupportedContent">
            <div class="mr-auto">
            </div>
           <!-- <form action="" class="search_box">
                <input type="search">
                <i class="fa fa-search"></i>
            </form>-->
            <!--translate-->
            <div id="google_translate_element" class="mobile_remove mobile_off"></div>
            <!--translate-->
            <!--list_property-->
            <div class="list_property mobile_off">
                <?php if ($this->session->userdata('agent_id')) { ?>
                    <a href="<?= base_url() ?>Dashboard">List Property</a>
                <?php } else { ?>
                    <a href="<?= base_url() ?>home/AgentRegister/">List Property</a>
                <?php } ?>
            </div> 
            <!--list_property-->
            <div class="login_box">
                <!-- Button trigger modal -->
                <a type="button" class="user" data-toggle="modal" data-target="#exampleModalLong">
                    <i class="fa fa-user" aria-hidden="true"></i>
                </a>                 
            </div>
          <!--  <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <ul class="mt-2">               
                    <div id="google_translate_element" class="desktop_off on_mobile nav-item mt-2"></div>                  
                    <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>home/about_us">About</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>home/AgentLogin">Sign in / Create Account</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>home/faq">Help</a></li>
                    <h6 class="nav-item mt-4">ACCOMMODATION</h6>
                    <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>home/PG_list">Paying Guest</a></li>          
                    <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>home/Hostel_list">Hostel</a></li>
                    <h6 class="nav-item mt-4">EXPLORE</h6>
                    <li class="nav-item"><a class="nav-link" href="#">Blog</a></li>
                    <h6 class="nav-item mt-4">WORK WITH US</h6>
                    <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>home/AgentLogin" target="_blank">List Property</a></li>
                    <li class="nav-item"><a class="nav-lik" href="<?= base_url() ?>assets/pdf/Payingguestworld-collabs.pdf" target="_blank">Collaboration & Meida</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>home/contact_us">Contact us</a></li>
                </ul>
            </div>-->
           <!-- <span class="menu" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></span>-->
        </div>
    </div>
</nav>
  <?php } ?>