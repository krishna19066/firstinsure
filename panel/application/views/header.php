<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!--=============== basic  ===============-->        
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="robots" content="index, follow"/>
 <link rel=icon href="<?= base_url() ?>assets/images/logo-1.png" sizes=32x32>

<!--=============== css  ===============--> 
<head>
   
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Tammudu+2:wght@500&family=Comic+Neue:wght@700&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/css/gijgo.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/nice-select.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/lightgallery.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/swiper.min.css">
</head>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="container-fluid img_user p-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="mt-5 text-center">
                    <h2>Hello!</h2>
                    <p class="mt-2 text-center">How do you want to Sign In?</p>

                </div>
                <div class="container">
                    <div class="row text-center">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                            <a href="<?= base_url() ?>home/AgentLogin">
                                <div class="user_box">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/images/property.png">
                                    <h4>Property Holder</h4>

                                </div>
                            </a>

                        </div><!--column-->

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                            <a href="<?= base_url() ?>home/UserLogin">
                                <div class="user_box">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/images/customeruser.png">
                                    <h4>Guest</h4>

                                </div>
                            </a>

                        </div><!--column-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--container-fluid-->
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->
<!--modal-end-->

