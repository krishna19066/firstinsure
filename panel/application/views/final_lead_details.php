<!DOCTYPE html>
<html>
    <?php include 'header.php'; ?>
    <?php
    foreach ($lead_data as $res) {
        
    }
    ?>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" />
<style>
    .select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 40px;
    margin:10px;
    -webkit-user-select: none;
}
</style>

    <body>
        <?php include 'agent_header.php'; ?>
        <!-- container open -->
        <div class="container-fluid dashboard-ac form_start mt-4">
            <div class="container-fluid mb-4">
                <div class="row">
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 pr-0">
                        <?php include 'agent_menu.php'; ?>
                    </div>
                    <!--column-->
                    <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 col-12 pl-0">
                        <form action="<?= base_url() ?>index.php/dashboard/AssignLead" method="POST">
                            <div class="form_bg">
                                <div class="row dash-bg">
                                    <div class="col-md-12">
                                        <h4><b> Confirm Lead</b></h4>
                                        <?php if ($this->session->flashdata('success')) { ?>
                                            <div class="alert alert-success">
                                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                                            </div>
                                        <?php } ?>
                                        
                                       <?php if($this->session->flashdata('error')){  ?>
                                            <div class="alert alert-danger">
                                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                            </div>
                                        <?php } ?>
                                        <hr>
                                    </div>
                                     <div class="col-md-2">
                                        <label class="required">Salutation</label>
                                         <select class="form-control" name="salutation" required >
                                            <option value="Mr" style="color: black;"><?php echo $res->salutation; ?></option>
                                            <option value="Mrs" style="color: black;">Mrs</option>
                                            <option value="Miss" style="color: black;">Miss</option>
                                            </select>
                                      
                                    </div>
                                    <div class="col-md-3">
                                        <label class="required">First Name</label>
                                        <input type="text" name="name" class="form-control" value="<?php echo $res->first_name; ?>" >
                                        <input type="hidden" name="id" class="form-control" value="<?php echo $res->id; ?>">
                                    </div>
                                     <div class="col-md-3">
                                        <label class="required">Last Name</label>
                                        <input type="text" name="last_name" class="form-control" value="<?php echo $res->last_name; ?>" >
                                    </div>
                                    <div class="col-md-4">
                                        <label class="required">Email</label>
                                        <input type="text" name="email" class="form-control" value="<?php echo $res->email; ?>" >
                                    </div>
                                    <div class="col-md-6">
                                        <label class="required">Mobile</label>
                                        <input type="text" name="mobile" class="form-control" maxlength='10' value="<?php echo $res->mobile; ?>" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="required">DOB</label>
                                        <input type="text" name="dob" class="form-control" value="<?php echo $res->dob; ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="required">State/ID</label>
                                      <select class="form-control" name="state_id" required>
                                          
                                           <option value="<?php echo $res->state_id; ?>" style="color: black;"><?php echo $res->state_id; ?></option>
                                            <?php
                                            $table1 = "state";
                                            $state_data  = $this->UserModel->getAllData($table1);
                                            foreach($state_data as $st){
                                            ?>
                                            <option value="<?php echo $st->id; ?>" style="color: black;"><?php echo $st->state_name; ?></option>
                                        <?php } ?>
                                            </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="required">City/Code</label>
                                       <select class="form-control" name="city" required>
                                            <option value="<?php echo $res->city_code; ?>" style="color: black;"><?php echo $res->city_code; ?></option>
                                            <?php
                                            $table2 = "cities";
                                            $city_data  = $this->UserModel->getAllData($table2);
                                            foreach($city_data as $ct){
                                            ?>
                                            <option value="<?php echo $ct->city_code; ?>" style="color: black;"><?php echo $ct->city_name; ?></option>
                                        <?php } ?>
                                            </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="required">Pincode</label>
                                        <input type="text" name="pincode" maxlength='6' class="form-control" value="<?php echo $res->pincode; ?>" >
                                    </div>
                                      <div class="col-md-6">
                                        <label class="required">Gender</label>
                                         <select class="form-control" name="gender" required >
                                            <option value="<?php echo $res->gender; ?>" style="color: black;"><?php echo $res->gender; ?></option>
                                            <option value="F" style="color: black;">Female</option>
                                            <option value="M" style="color: black;">Male</option>
                                       
                                            </select>
                                    </div>
                                    <!--  <div class="col-md-6">
                                        <label class="required">IP</label>
                                        <input type="text" name="ip" class="form-control" value="10.74.8.10" >
                                    </div>-->
                                    <div class="col-md-6">
                                        <label class="required">Appointment Date/time </label>
                                        <input type="text" name="appointment_date" class="form-control" value="<?php echo $res->appointment_date; ?>" required>
                                    </div>
                                     
                                   
                                     <div class="col-md-6">
                                        <label class="required">Address 1 </label>
                                        <input type="text" name="address_1" class="form-control" maxlength='50' value="<?php echo $res->address_1; ?>" oninput="this.value = this.value.replace(/[^a-zA-Z.0-9]/g, ' ')" required >
                                    </div>
                                      <div class="col-md-6">
                                        <label class="required">Address 2 </label>
                                        <input type="text" name="address_2" maxlength='50' value="<?php echo $res->address_2; ?>" class="form-control" oninput="this.value = this.value.replace(/[^a-zA-Z.0-9]/g, ' ')" value="" required >
                                    </div>
                                    
                                    <!--  <div class="col-md-6">
                                        <label class="required">AllocatedAt Date/time </label>
                                        <input type="text" name="allocated_at" class="form-control" value="<?php echo $res->allocated_at; ?>">
                                    </div>-->
                                     <div class="col-md-6">
                                        <label class="required">CCRemarks </label>
                                        <input type="text" name="cc_remarks" class="form-control" value="<?php echo $res->cc_remarks; ?>">
                                    </div>
                                     <div class="col-md-6">
                                        <label class="required">Income </label>
                                        <input type="number" name="income" class="form-control" value="<?php echo $res->income; ?>">
                                    </div>
                                   <!--  <div class="col-md-6">
                                        <label class="required">Feedback</label>
                                       <select class="form-control" name="feedback" required>
                                            <option value="Follow Up" style="color: black;">Follow Up</option>
                                            <option value="Call Back" style="color: black;">Call Back</option>
                                            <option value="Confirm" style="color: black;">Confirm</option>
                                            <option value="Not Reachable" style="color: black;">Not Reachable</option>
                                            <option value="Switch Off" style="color: black;">Switch Off</option>
                                            </select>
                                    </div>
                                     <div class="col-md-6">
                                        <label class="required">Follow Up Date Time</label>
                                        <input type="datetime-local" name="follow_up_time" class="form-control" value="" >
                                    </div>-->
                                      <div class="col-md-12">
                                        <label class="required">AllocatedTo </label><br>
                                      
                                         <select class="form-control" name="allocated_to" id="position" style="width: 50% height:50px;" required>
                                            <option value="" style="color: black;">Select Manager</option>
                                            <?php
                                            $table = "hms";
                                            $hms_data  = $this->UserModel->getAllData($table);
                                            foreach($hms_data as $hms){
                                            ?>
                                            <option value="<?php echo $hms->agent_code; ?>" style="color: black;"><?php echo $hms->agent_name; ?> - <?php echo $hms->agent_code; ?> -<?php echo $hms->city; ?></option>
                                        <?php } ?>
                                           <script>
                                             $("#position").select2({
                                             allowClear:true,
                                             placeholder: 'Select Manager',
                                             class: 'form-control'
                                             });
                                             </script>
                                 
                                     
                                    <div class="col-md-12 text-right mt-3">
                                        <input type="submit" class="btn payingguest-btn no2" value="Save Lead">
                                    </div>
                                </div>
                                <!--row-->

                            </div>
                        </form>
                    </div>
                    <!--column-->

                </div>
                <!--column-->
            </div>
            <!--row-->
        </div>
        <!-- container close -->
    </div>
    <?php include 'footer.php'; ?>
</body>
</html>