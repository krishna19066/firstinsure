function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  // document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  // document.body.style.backgroundColor = "white";
}


  var nc_select = $('.nc_select');
  if(nc_select.length){
    nc_select.niceSelect();
  }
  
  $('#datepicker_1').datepicker({
  minDate: new Date()

  });
  $('#datepicker_5').datepicker({
  minDate: new Date()

  });

  $('#datepicker_2').datepicker();  
  $('#datepicker_3').datepicker();
  $('#datepicker_4').datepicker();  
  $('#datepicker_6').datepicker();


$(document).ready(function(){
$("#hotel-slider").owlCarousel({
 
        loop:false,
        dots:false,
        autoplay:false,
        autoplayHoverPause: true,
        responsive:{
        0:{
            items:1,
            autoplay:true
        },
        600:{
            items:2
        },
        1000:{
            items:3,
            nav:true
        }
    }
 
  });
});

//language//
//remove//
function myFunction(x) {
  if (x.matches) {
    $(".mobile_remove").remove();
  } 
}

var x = window.matchMedia("(min-width: 320px) and (max-width: 767px)")
myFunction(x)
x.addListener(myFunction)

//show//
function myreFunction(e) {
  if (e.matches) { 
    $(".desktop_off").show();
  } 
}

var e = window.matchMedia("(min-width: 320px) and (max-width: 767px)")
myFunction(x)
e.addListener(myreFunction)

//read more content//
$('.more-conetnt-show').click(function() {
  $('.more-content-off').slideToggle();
  if ($('.more-conetnt-show').text() == "show more") {
    $(this).text("show less")
  } else {
    $(this).text("show more")
  }
});



//caret//
$(document).on('click', '.caret-icon', function() {
   $(this).toggleClass('fa-caret-up fa-caret-down');
})

//text-show more//
var text = $('#show-more-content'),
     btn = $('#show-more-btn'),
       h = text[0].scrollHeight; 

if(h > 125) {
  btn.addClass('less');
  btn.css('display', 'block');
}

btn.click(function(e) 
{
  e.stopPropagation();

  if (btn.hasClass('less')) {
      btn.removeClass('less');
      btn.addClass('more');
      btn.text('Show less');

      text.animate({'height': h});
  } else {
      btn.addClass('less');
      btn.removeClass('more');
      btn.text('Show more');
      text.animate({'height': '125px'});
  }  
});

//menu active//
$(document).ready(function () {
    var str = location.href.toLowerCase();
    $("#mySidenav li a").each(function () {
        if (str.indexOf(this.href.toLowerCase()) > -1) {

            $("nav li").removeClass("active");

            $(this).parent().addClass("active");

        }

    });
});


//innerpagetoggle//
 // Close menu
// $("#close-menu").click(function(e) {
//   e.preventDefault();
//   $(".booking-confirmation").removeClass("active");
// });
// // Open menu
// $("#select").click(function(e) {
//   e.preventDefault();
//   $(".booking-confirmation").addClass("active");
// });

//fixed my selection//
var navTop = $('#selection-fixed').offset().top;
var navStop = $('#prices').offset().top;
var lastMode = "fixed";

$(window).scroll(function() {
  var mode;
  if ($(this).scrollTop() >= navTop) {
    if ($(this).scrollTop() > navStop + $(window).height() - 50)
      mode = 'absolute';
    else
      mode = 'fixed';
  } else {
    mode = 'absolute';
  }

  if (lastMode !== mode) {
    if (mode == 'fixed') {
       $('#selection-fixed').addClass("fixedon");
    } else {
      $('#selection-fixed').removeClass("fixedon");
    }
    lastMode = mode;
  }
});

$(document).scroll(function() {
  var span = $('#selection-fixed'),
    div = $('#prices'),
    spanHeight = span.innerHeight(),
    divHeight = div.height(),
    spanOffset = span.offset().top + spanHeight,
    divOffset = div.offset().top + divHeight;

  if (spanOffset >= divOffset) {
    span.addClass('bottom');
    var windowScroll = $(window).scrollTop() + $(window).height() - 250;
    if (spanOffset > windowScroll) {
      span.removeClass('bottom');
    }
  }
});

